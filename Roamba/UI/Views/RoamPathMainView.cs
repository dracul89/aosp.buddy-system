﻿using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Linq;

namespace Roamba
{
    public class RoamPathMainView : RoamPathView
    {
        private TextView _pointsText;
        private TextView _isLoopingText;
        private TextView _isReversedText;
        private TextView _isLockedText;
        private TextInputView _priorityNames;
        private TextInputView _ignoredNames;
        private TextInputView _fileName;

        public RoamPathMainView(string path, RoamPathWindow window) : base(path, window)
        {

            try
            {
                if (Root.FindChild("AddPoint", out Button addPoint)) { addPoint.Clicked = AddPointClick; }
                if (Root.FindChild("RemovePoint", out Button removePoint)) { removePoint.Clicked = RemovePointClick; }
                if (Root.FindChild("ReversePath", out Button reversePath)) { reversePath.Clicked = ReversePathClick; }
                if (Root.FindChild("RunPath", out Button runPath)) { runPath.Clicked = RunPathClicked; }
                if (Root.FindChild("PickupPoint", out Button pickupPoint)) { pickupPoint.Clicked = PickupPointClick; }
                if (Root.FindChild("PlacePoint", out Button placePoint)) { placePoint.Clicked = PlacePointClick; }
                if (Root.FindChild("SelectPoint", out Button selectPoint)) { selectPoint.Clicked = SelectPointClick; }
                if (Root.FindChild("UnselectPoint", out Button unselectPoint)) { unselectPoint.Clicked = UnselectPointClick; }
                if (Root.FindChild("SplitPath", out Button splitPath)) { splitPath.Clicked = SplitPointClick; }
                if (Root.FindChild("ToggleLock", out Button toggleLock)) { toggleLock.Clicked = ToggleLockClick; }
                if (Root.FindChild("ToggleLoop", out Button toggleLoop)) { toggleLoop.Clicked = ToggleLoopClick; }
                if (Root.FindChild("ClearPath", out Button clearPath)) { clearPath.Clicked = ClearClick; }
                if (Root.FindChild("Points", out _pointsText)) { _pointsText.Text = Roamba.RoamPath.SPath.Waypoints.Count.ToString(); }
                if (Root.FindChild("IsLooping", out _isLoopingText)) { _isLoopingText.Text = Roamba.RoamPath.SPath.IsLooping.ToString(); }
                if (Root.FindChild("IsReversed", out _isReversedText)) { _isReversedText.Text = Roamba.RoamPath.SPath.IsReversed.ToString(); }
                if (Root.FindChild("IsLocked", out _isLockedText)) { _isLockedText.Text = Roamba.RoamPath.SPath.IsLocked.ToString(); }
                if (Root.FindChild("IgnoredNames", out _ignoredNames)) { _ignoredNames.Text = Roamba.RoamPath.Rules.IgnoredNames?.Count == 0 ? "Leetzor\nDraculeet\nNerdleet" : string.Join("\n", Roamba.RoamPath.Rules.IgnoredNames); }
                if (Root.FindChild("PriorityNames", out _priorityNames)) { _priorityNames.Text = Roamba.RoamPath.Rules.PriorityNames?.Count == 0 ? "Pinkleet\nBlueLeet\nRedLeet" : string.Join("\n", Roamba.RoamPath.Rules.PriorityNames); }
                if (Root.FindChild("Save", out Button save)) { save.Clicked += SaveClick; }
                if (Root.FindChild("FileName", out _fileName)) { _fileName.Text = string.IsNullOrEmpty(Roamba.Config.RoamPath) ? "" : System.IO.Path.GetFileNameWithoutExtension(Roamba.Config.RoamPath); }

            }
            catch (Exception ex)
            {
                Roamba.Log.Warning(ex.Message);
            }
        }

        public string GetIgnoredText()
        {
            return _ignoredNames.Text;
        }

        public string GetPriorityText()
        {
            return _priorityNames.Text;
        }

        private void SaveClick(object sender, ButtonBase e)
        {
            if (string.IsNullOrEmpty(_fileName.Text))
            {
                Roamba.Log.Warning("Please provide a file name");
                return;
            }

            var savePath = $"{CommonParameters.PluginDataPath}\\RoamPath\\{_fileName.Text}.json";

            if (!string.IsNullOrEmpty(_ignoredNames.Text))
                Roamba.RoamPath.Rules.IgnoredNames = _ignoredNames.Text.Split('\n').Select(x => x.Trim()).ToList();

            if (!string.IsNullOrEmpty(_priorityNames.Text))
                Roamba.RoamPath.Rules.PriorityNames = _priorityNames.Text.Split('\n').Select(x => x.Trim()).ToList();

            Roamba.RoamPath.SPath.Name = _fileName.Text;
            Roamba.RoamPath.SPath.PlayfieldId = Playfield.ModelIdentity.Instance;
            Roamba.RoamPath.Save(savePath);
            Roamba.Config.RoamPath = savePath;
            Roamba.Config.Save();
        }

        private void AddPointClick(object sender, ButtonBase e)
        {
            if (Roamba.RoamPath.SPath.Waypoints.Count < 2 && Roamba.RoamPath.SPath.IsLooping)
            {
                Roamba.RoamPath.SPath.ToggleLoop();
                _isLoopingText.Text = Roamba.RoamPath.SPath.IsLooping.ToString();
            }

            Roamba.RoamPath.SPath.AddPoint();
            _pointsText.Text = Roamba.RoamPath.SPath.Waypoints.Count.ToString();
        }

        private void RemovePointClick(object sender, ButtonBase e)
        {
            Roamba.RoamPath.SPath.RemovePoint(); 
            _pointsText.Text = Roamba.RoamPath.SPath.Waypoints.Count.ToString();
        }

        private void PickupPointClick(object sender, ButtonBase e)
        {
            Roamba.RoamPath.SPath.PickupPoint();
        }

        private void PlacePointClick(object sender, ButtonBase e)
        {
            Roamba.RoamPath.SPath.PlacePoint();
        }

        private void SelectPointClick(object sender, ButtonBase e)
        {
            Roamba.RoamPath.SPath.SelectPoint();
        }

        private void UnselectPointClick(object sender, ButtonBase e)
        {
            Roamba.RoamPath.SPath.UnselectPoint();
        }

        private void RunPathClicked(object sender, ButtonBase e)
        {
            if (Roamba.RoamPath.SPath == null)
                return;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
                return;
            }

            if (!SMovementController.IsLoaded())
                SMovementController.Set();

            SMovementController.SetPath(Roamba.RoamPath.SPath, true);
        }

        private void SplitPointClick(object sender, ButtonBase e)
        {
            if (!Roamba.RoamPath.SPath.Split())
                return;

            _pointsText.Text = Roamba.RoamPath.SPath.Waypoints.Count.ToString();
        }

        private void ToggleLockClick(object sender, ButtonBase e)
        {
            Roamba.RoamPath.SPath.ToggleLock();
            _isLockedText.Text = Roamba.RoamPath.SPath.IsLocked.ToString();
        }

        private void ToggleLoopClick(object sender, ButtonBase e)
        {
            if (Roamba.RoamPath.SPath.Waypoints.Count < 2)
                return;

            Roamba.RoamPath.SPath.ToggleLoop();
            _isLoopingText.Text = Roamba.RoamPath.SPath.IsLooping.ToString();
        }

        private void ClearClick(object sender, ButtonBase e)
        {
            Roamba.RoamPath.SPath.Clear();
            Roamba.RoamPath.SPath.IsLocked = false;
        }

        private void ReversePathClick(object sender, ButtonBase e)
        {
            Roamba.RoamPath.SPath.Reverse();
            _isReversedText.Text = Roamba.RoamPath.SPath.IsReversed.ToString();
        }
    }
}