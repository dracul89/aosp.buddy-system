﻿using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.IO;

namespace Roamba
{
    public class RoamPathInitView : RoamPathView
    {
        private TextView _pathLabel;

        public RoamPathInitView(string path, RoamPathWindow window) : base(path, window)
        {
            try
            {
                if (Root.FindChild("Path", out _pathLabel)) 
                {
                    var rulesPath = Roamba.Config.RoamPath;

                    if (!string.IsNullOrEmpty(rulesPath))
                        _pathLabel.Text = rulesPath;
                }

                if (Root.FindChild("CreateEditPath", out Button createPath))
                    createPath.Clicked = CreateOrEditClicked;

                if (Root.FindChild("LoadPath", out Button loadPath))
                    loadPath.Clicked = LoadClicked;
            }
            catch (Exception ex)
            {
                Roamba.Log.Warning(ex.Message);
            }
        }

        private void LoadClicked(object sender, ButtonBase e)
        {
            if (!File.Exists(_pathLabel.Text))
            {
                Roamba.Log.Information($"File not found '{_pathLabel.Text}'");
                return;
            }

            if (Roamba.RoamPath.SPath != null)
                Roamba.RoamPath.SPath.Delete();

            Roamba.RoamPath.SPath = RoamPath.Load(_pathLabel.Text).SPath;
            Roamba.Config.RoamPath = _pathLabel.Text;
            Roamba.Config.Save();
            Load();
        }

        private void CreateOrEditClicked(object sender, ButtonBase e)
        {
            if (Roamba.RoamPath.SPath == null)
                Roamba.RoamPath.SPath = SPath.Create();

            Load();
        }

        private void Load()
        {
            Roamba.RoamPath.SPath.IsLocked = Roamba.RoamPath.SPath.Waypoints.Count != 0;
            Dispose();
            Parent.LoadMainView();
        }
    }
}