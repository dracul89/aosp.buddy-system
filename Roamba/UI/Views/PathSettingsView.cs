﻿using AOSharp.Core.UI;
using Roamba;

namespace Buddy.Shared.UI
{
    public class PathSettingsView : CustomView
    {
        private TextInputView _tauntRangeTextView;
        private TextInputView _pathRangeTextView;
        private TextInputView _wanderLimitTextView;
        private TextInputView _fightTimeoutTextView;
        private Checkbox _attackMobsCheckbox;
        private Checkbox _useTauntItemCheckbox;
        private Checkbox _pathToMobsCheckBox;

        public PathSettingsView(string xmlPath) : base(xmlPath)
        {
            if (Root.FindChild("AttackMobs", out _attackMobsCheckbox)) { }
            if (Root.FindChild("TauntItem", out _useTauntItemCheckbox)) { }
            if (Root.FindChild("PathToMobs", out _pathToMobsCheckBox)) { }
            if (Root.FindChild("TauntRange", out _tauntRangeTextView)) { }
            if (Root.FindChild("PathRange", out _pathRangeTextView)) { }
            if (Root.FindChild("WanderLimit", out _wanderLimitTextView)) { }
            if (Root.FindChild("FightTimeout", out _fightTimeoutTextView)) { }
        }

        private int GetValue(TextInputView textView)
        {
            return int.TryParse(textView.Text, out int range) ? range : 0;
        }
        public PathConfig GetData()
        {
            PathConfig pathingConfig = new PathConfig
            {
                AttackMobs = _attackMobsCheckbox.IsChecked,
                UseTauntItem = _useTauntItemCheckbox.IsChecked,
                PathToMobs = _pathToMobsCheckBox.IsChecked,
                TauntRange = GetValue(_tauntRangeTextView),
                PathRange = GetValue(_pathRangeTextView),
                WanderLimit = GetValue(_wanderLimitTextView),
                FightTimeoutPeriod = GetValue(_fightTimeoutTextView),
            };

            return pathingConfig;
        }


        public void SetData(PathConfig config)
        {
            _attackMobsCheckbox.SetValue(config.AttackMobs);
            _useTauntItemCheckbox.SetValue(config.UseTauntItem);
            _pathToMobsCheckBox.SetValue(config.PathToMobs);
            _tauntRangeTextView.Text = config.TauntRange.ToString();
            _pathRangeTextView.Text = config.PathRange.ToString();
            _wanderLimitTextView.Text = config.WanderLimit.ToString();
            _fightTimeoutTextView.Text = config.FightTimeoutPeriod.ToString();
        }
    }
}