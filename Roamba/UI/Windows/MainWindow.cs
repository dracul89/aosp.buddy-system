﻿using AOSharp.Common.GameData.UI;
using AOSharp.Core.UI;
using Buddy.Shared.UI;
using AOSharp.Core;
using AOSharp.Core.Misc;
using Roamba.IPCMessages;
using AOSharp.Pathfinding;
using AOSharp.Common.GameData;
using System;

namespace Roamba
{
    public class MainWindow : BuddyBaseWindow
    {
        //Per plugin adjustable
        public override string CoreViewRootName => "CoreViewRoot";

        //Per plugin adjustable
        public override string InfoText => "Channel - Used to sync Start / Stop across clients\n\n" +
            "Attack Mobs - Attacks any non ignored mob within attack range\n\n" +
            "Path to mobs - Paths to any non ignored mob\n" +
            "Path Range - At which range should we path to attack distance\n\n" +
            "Taunt items - Uses taunt on any non ignored mob\n" +
            "Pull Range - At which range should we use taunt items\n\n" +
            "Wander Limit - At which distance should we ignore mobs\n\n" +
            "Fight Tiemout - How long until I ignore a particular mob\n\n" +
            " - Setup Ignored / Priority names in Path Editor\n" +
            " - Make sure to save your path in the Path Editor to avoid losing data.\n" +
            " - Last saved / loaded path will be loaded on every injection.\n" +
            " - Pressing 'Start' will sync the active chars path across all characters\n\n" +

            "Path Editor - Allows creation of paths (looping / back and forth)\n\n" +
            "Core Operations\n" +
            "   Add - Adds a point at players location\n" +
            "   Remove - Remove closest point (green circle)\n" +
            "   Reverse - Reverses the path\n" +
            "   Test / Stop - Test run / stop navigating\n\n" +
            "Edit Operations\n" +
            "   Pickup - picks up closest point (green circle)\n" +
            "   Place - Places picked up point\n\n" +
            "Split Operations\n" +
            "   Select - Selects or unselects closest circle\n" +
            "   Unselect - Unselects closest circle\n" +
            "   Split - Splits two adjacent selected points\n\n" +
            "Misc Operations\n" +
            "   Toggle Lock - Locks the path in place\n" +
            "   Toggle Loop - Loops the path\n" +
            "   Clear - Clears\n";


        public PathSettingsView PathSettingsView;

        private RoamPathWindow _roamPathWindow;
        private AutoResetInterval _uiUpdateTick;
        private Button _saveConfig;
        private string _roamWindowPath;
        private string _roamInitViewPath;
        private string _roamMainViewPath;
        private string _pathSettingsViewPath;

        public MainWindow(string windowName, string baseWindowPath, string infoWindowPath, string roamWindowPath, string coreViewPath, string pathSettingsViewPath, string roamInitViewPath, string roamMainViewPath, WindowStyle windowStyle = WindowStyle.Default, WindowFlags flags = WindowFlags.AutoScale | WindowFlags.NoFade) : base(windowName, baseWindowPath, infoWindowPath, coreViewPath, windowStyle, flags)
        {
            _uiUpdateTick = new AutoResetInterval(100);
            _roamWindowPath = roamWindowPath;
            _roamInitViewPath = roamInitViewPath;
            _roamMainViewPath = roamMainViewPath;
            _pathSettingsViewPath = pathSettingsViewPath;
        }

        protected override void OnWindowCreating()
        {
            //Make sure to call the base first in order to init all the core views
            base.OnWindowCreating();

            Window.FindView("RangeSettingsRoot", out View rangeSettingsRoot);
            PathSettingsView = new PathSettingsView(_pathSettingsViewPath);
            rangeSettingsRoot.AddChild(PathSettingsView.Root, true);

            //Populate UI with config file data

            PathSettingsView.SetData(Roamba.Config.PathingConfig);
            CoreSettingsView.SetData(Roamba.Config.CoreConfig);

            if (Window.FindView("PathCreator", out Button pathCreator))
                pathCreator.Clicked += PathCreatorClick;

            if (Window.FindView("SaveConfig", out _saveConfig))
                _saveConfig.Clicked = OnSaveConfigClick;

            if (Roamba.Config.WindowCoords.X != 0 && Roamba.Config.WindowCoords.Y != 0)
                Window.MoveTo(Roamba.Config.WindowCoords.X, Roamba.Config.WindowCoords.Y);

            CoreSettingsView.EnabledButton.Clicked += OnEnable;
            Game.OnUpdate += MainWindowUpdate;
        }

        private void OnEnable(object sender, ButtonBase e)
        {
            try
            {
                if (CoreSettingsView.IsButtonEnabled && Roamba.RoamPath?.SPath?.Waypoints.Count == 0)
                {
                    CoreSettingsView.IsButtonEnabled = false;
                    CoreSettingsView.SetButtonState(false);
                    Chat.WriteLine("Path needs to have at least one waypoint");
                    
                }
                else
                {
                    Roamba.Ipc.Broadcast(new EnabledIPCMessage
                    {
                        SetEnabled = CoreSettingsView.IsButtonEnabled,
                        RoamPath = Roamba.Config.RoamPath,
                    });

                    OnEnabledPress(CoreSettingsView.IsButtonEnabled);
                }

            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
            }
        }

        public void OnEnabledPress(bool result)
        {
            CoreSettingsView.SetButtonState(result);

            if (Roamba.Ipc.ChannelId != Roamba.Config.CoreConfig.ChannelId)
                Roamba.Ipc = new IPC(Roamba.Ipc.ChannelId);

            Roamba.StateMachine.SetStatus(result);

            if (SMovementController.IsNavigating())
                SMovementController.Halt();

            if (result)
                SaveConfig();
        }

        private void OnSaveConfigClick(object sender, ButtonBase e)
        {
            SaveConfig();
        }

        private void SaveConfig()
        {
            Roamba.Config.CoreConfig = CoreSettingsView.GetData();
            Roamba.Config.WindowCoords = new Vector2(Window.GetFrame().MinX, Window.GetFrame().MinY);
            Roamba.Config.PathingConfig = PathSettingsView.GetData();
            Roamba.Config.Save();

            Chat.WriteLine("Config saved! (use the Path Editor to save / export your path)",ChatColor.Green);
        }

        private void PathCreatorClick(object sender, ButtonBase e)
        {
            if (_roamPathWindow != null && _roamPathWindow.Window.IsValid)
                return;

            _roamPathWindow = new RoamPathWindow("Roamba Path", _roamWindowPath, _roamInitViewPath, _roamMainViewPath);
            _roamPathWindow.Show();
        }

        private void MainWindowUpdate(object sender, float e)
        {
            if (!_uiUpdateTick.Elapsed)
                return;

            if (_roamPathWindow != null && !_roamPathWindow.Window.IsValid)
            {
                Roamba.RoamPath.SPath.IsLocked = true;
                _roamPathWindow = null;
            }
        }
    }
}