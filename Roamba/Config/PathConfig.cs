﻿
namespace Roamba
{
    public class PathConfig
    {
        public bool AttackMobs;
        public bool UseTauntItem;
        public bool PathToMobs;
        public int TauntRange;
        public int PathRange;
        public int WanderLimit;
        public int FightTimeoutPeriod;

        public PathConfig()
        {
            TauntRange = 40;
            PathRange = 30;
            WanderLimit = 70;
            FightTimeoutPeriod = 30;
            PathToMobs = true;
            AttackMobs = true;
            UseTauntItem = true;
        }
    }
}