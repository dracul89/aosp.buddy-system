﻿using AOSharp.Core;
using AOSharp.Common.GameData;

namespace Roamba
{
    public static class Extensions
    {
        public static Vector3 GetPathPos(this SimpleChar simpleChar)
        {
            return simpleChar.IsPathing && !simpleChar.IsAttacking ? simpleChar.Position + simpleChar.Rotation * Vector3.Forward * 2f : simpleChar.Position;
        }

        public static float GetLowestWeaponAttackRange(this LocalPlayer localPlayer)
        {
            float cachedRange = byte.MaxValue;
            var rangePrc = localPlayer.GetStat(Stat.RangeIncreaserWeapon) / 100f;

            if (localPlayer.Weapons.Count == 0)
                return 2 * (1f + rangePrc);

            foreach (var weapon in localPlayer.Weapons)
            {
                var weaponAttackRange = weapon.Value.AttackRange;

                if (weaponAttackRange > cachedRange)
                    continue;

                cachedRange = weaponAttackRange;
            }

            return cachedRange * (1f + rangePrc);
        }

        public static bool IsInWeaponHitRange(this SimpleChar target, float padding)
        {
            return Vector3.Distance(target.GetPathPos(), DynelManager.LocalPlayer.Position) < DynelManager.LocalPlayer.GetLowestWeaponAttackRange() - padding;
        }
    }
}
