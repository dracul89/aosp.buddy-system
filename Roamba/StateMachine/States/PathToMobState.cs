﻿using AOSharp.Core;
using AOSharp.Pathfinding;
using AOSharp.Core.Inventory;

namespace Roamba
{
    public class PathToMobState : FSMProvider<State, Trigger, RoamContext>, IState
    {
        public PathToMobState(FSM<State, Trigger, RoamContext> stateMachine) : base(stateMachine)
        {
        }

        public void OnStateEnter()
        {
            SMovementController.SetDestination(StateMachine.Context.NextTarget.GetPathPos());
        }

        public void OnStateExit()
        {
            if (SMovementController.IsNavigating())
                SMovementController.Halt();
        }

        public void Tick()
        {
            if (!StateMachine.Context.MobTargeting.TryGetNextTarget(out SimpleChar target, out bool shouldAttack, out Item tauntItem))
            {
                StateMachine.Fire(Trigger.TargetNull);
            }
            else if (target.Identity != StateMachine.Context.NextTarget.Identity)
            {
                StateMachine.Context.NextTarget = target;
                StateMachine.Fire(Trigger.ChangedTarget);
            }
            else if (StateMachine.Context.MobTargeting.IsInWeaponsRange(StateMachine.Context.NextTarget))
            {
                StateMachine.Fire(shouldAttack ? Trigger.TargetIsHittable : Trigger.FollowTarget);
            }
            else if (StateMachine.Context.MobTargeting.IsInPathRange(StateMachine.Context.NextTarget) && !SMovementController.IsNavigating())
            {
                SMovementController.SetDestination(StateMachine.Context.NextTarget.GetPathPos());
            }

            if (tauntItem != null)
            {
                Targeting.SetTarget(target);
                tauntItem.Use();
                SMovementController.Halt();
            }
        }
    }
}