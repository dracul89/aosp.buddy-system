﻿using AOSharp.Core;
using AOSharp.Pathfinding;
namespace Roamba
{
    public class RoamState : FSMProvider<State, Trigger, RoamContext>, IState
    {
        public RoamState(FSM<State, Trigger, RoamContext> stateMachine) : base(stateMachine)
        {
        }

        public void OnStateEnter()
        {
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (StateMachine.Context.MobTargeting.TryGetNextTarget(out SimpleChar target, out _, out _)) { }

            StateMachine.Context.NextTarget = target;

            if (StateMachine.Context.NextTarget == null)
            {
                if (!SMovementController.IsNavigating())
                    SMovementController.SetPath(Roamba.RoamPath.SPath, true);
            }
            else
            {
                StateMachine.Fire(Trigger.TargetFound);
            }
        }
    }
}