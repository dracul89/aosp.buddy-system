﻿using AOSharp.Core.Misc;
using AOSharp.Core;
using AOSharp.Pathfinding;
using System;
using AOSharp.Common.GameData;
using System.Linq;

namespace Roamba
{
    public enum State
    {
        Roam,
        PathToMob,
        Fight
    }

    public enum Trigger
    {
        TargetIsHittable,
        TargetOutRange,
        TargetFound,
        ChangedTarget,
        RetriggerOnEntry,
        TargetNull,
        FollowTarget
    }

    public class RoamContext
    {
        public SimpleChar NextTarget;
        public DateTime LastFightTime;
        public MobTargeting MobTargeting;

        public RoamContext(MobTargeting mobTargeting)
        {
            MobTargeting = mobTargeting;
        }
    }

    public class RoamStateMachine : FSM<State, Trigger, RoamContext>
    {
        private AutoResetInterval _fsmTickRate;
        public const int UpdateRate = 10;
        private bool _enabled = false;

        public RoamStateMachine(MobTargeting mobTargeting, bool enabled = false) : base(State.Roam, new RoamContext(mobTargeting))
        {
            _enabled = enabled;
            _fsmTickRate = new AutoResetInterval(1000 / UpdateRate);
            Game.OnUpdate += OnUpdate;
            SMovementController.DestinationReached += OnDestinationReached;
        }

        public void SetStatus(bool enabled)
        {
            _enabled = enabled;

            if (enabled)
            {
                return;
            }

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();

            Fire(Trigger.TargetNull);
        }

        private void OnDestinationReached(Vector3 vector)
        {
            var sPath = Roamba.RoamPath.SPath;

            if (sPath.Waypoints.Count == 0)
                return;

            var startPos = sPath.Waypoints.FirstOrDefault();
            var endPos = sPath.Waypoints.LastOrDefault();

            if (Vector3.Distance(startPos, vector) < 0.05f || Vector3.Distance(endPos, vector) < 0.05f)
            {
                if (!sPath.IsLooping)
                    sPath.Reverse();

                SMovementController.SetPath(sPath, false);
                return;
            }
        }

        private void OnUpdate(object sender, float e)
        {
            if (!_fsmTickRate.Elapsed)
                return;

            if (!_enabled)
                return;

            Tick();
        }

        protected override void ConfigureStateMachine()
        {
            OnTransitioned(OnTransitioned);

            AddState(State.Roam, typeof(RoamState))
                .Permit(Trigger.TargetFound, State.PathToMob)
                .PermitReentry(Trigger.TargetNull);

            AddState(State.PathToMob, typeof(PathToMobState))
                .Permit(Trigger.TargetNull, State.Roam)
                .Permit(Trigger.TargetIsHittable, State.Fight)
                .PermitReentry(Trigger.TargetFound)
                .PermitReentry(Trigger.ChangedTarget)
                .PermitReentry(Trigger.FollowTarget);

            AddState(State.Fight, typeof(FightState))
                .Permit(Trigger.TargetNull, State.Roam)
                .Permit(Trigger.TargetOutRange, State.PathToMob)
                .PermitReentry(Trigger.TargetFound)
                .PermitReentry(Trigger.ChangedTarget);
        }

        private void OnTransitioned(Transition transition)
        {
            //Roamba.Log.Information($"Transitioned from {transition.Source} to {transition.Destination}. Trigger {transition.Trigger}");
        }
    }
}