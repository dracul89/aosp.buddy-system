﻿using AOSharp.Core;
using AOSharp.Core.IPC;
using Roamba.IPCMessages;

namespace Roamba
{
    public class IPC : IPCChannelBase
    {
        protected override int _localDynelId => DynelManager.LocalPlayer.Identity.Instance;
        internal byte ChannelId;

        public IPC(byte channelId) : base(channelId)
        {
            ChannelId = channelId;
            RegisterCallback((int)IPCOpcode.Enabled, OnEnabledMessage);
        }

        private void OnEnabledMessage(int arg1, IPCMessage message)
        {
            EnabledIPCMessage enabledIpc = (EnabledIPCMessage)message;


            if (!string.IsNullOrEmpty(enabledIpc.RoamPath) && Roamba.RoamPath.SPath != null)
            {
                Roamba.RoamPath.SPath.Delete();
                Roamba.RoamPath = RoamPath.Load(enabledIpc.RoamPath);
                Roamba.Config.Save();
            }

            Roamba.MainWindow.OnEnabledPress(enabledIpc.SetEnabled);
        }
    }
}
