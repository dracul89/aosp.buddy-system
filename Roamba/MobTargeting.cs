﻿using AOSharp.Core;
using System.Linq;
using AOSharp.Common.GameData;
using System.Collections.Generic;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.Messages;
using System.Data;
using AOSharp.Core.Inventory;

namespace Roamba
{
    public class MobTargeting
    {
        private List<Identity> _ignoredIdentities;
        private List<int> _tauntItemIds;
        private RoamPath _roamPath;
        private RoambaConfig _config;

        public MobTargeting(RoamPath roamPath, RoambaConfig config)
        {
            _roamPath = roamPath;
            _config = config;
            _tauntItemIds = new List<int> { 83919, 83920 };
            _ignoredIdentities = new List<Identity>();

            Network.N3MessageReceived += OnN3MessageReceived;
        }

        public bool TryGetNextTarget(out SimpleChar target, out bool shouldAttack, out Item tauntItem)
        {
            shouldAttack = _config.PathingConfig.AttackMobs;
            tauntItem = GetTauntItem();

            var filteredTargets = FilterTargets(GetPossibleTargets());
            filteredTargets = OrderTargets(filteredTargets);
            target = filteredTargets.FirstOrDefault();

            return target != null;
        }

        private IEnumerable<SimpleChar> GetPossibleTargets()
        {
            return DynelManager.NPCs
                .Where(x => !x.IsPet && !x.IsPlayer && x.IsAlive && x.IsInLineOfSight && !_roamPath.Rules.IgnoredNames.Contains(x.Name) && !_ignoredIdentities.Contains(x.Identity));
        }

        private List<SimpleChar> FilterTargets(IEnumerable<SimpleChar> possibleTargets)
        {
            var finalTargets = new List<SimpleChar>();

            if (!_config.PathingConfig.AttackMobs && !_config.PathingConfig.UseTauntItem && !_config.PathingConfig.PathToMobs)
                return finalTargets;

            if (_config.PathingConfig.UseTauntItem)
                finalTargets.AddRange(possibleTargets.Where(x => Vector3.Distance(x.Position, DynelManager.LocalPlayer.Position) <= _config.PathingConfig.TauntRange));

            if (_config.PathingConfig.PathToMobs)
                finalTargets.AddRange(possibleTargets.Where(x => Vector3.Distance(x.Position, DynelManager.LocalPlayer.Position) <= _config.PathingConfig.PathRange));
            else
                finalTargets.AddRange(possibleTargets.Where(x => IsInWeaponsRange(x)));

            finalTargets = finalTargets
              .Distinct()
              .Where(x => _roamPath.SPath.Waypoints.Any(c => Vector3.Distance(c, x.Position) < _config.PathingConfig.WanderLimit))
              .ToList();

            if (Team.IsInTeam && !Team.IsLeader)
            {
                var teamMemberTarget = Team.Members?
                    .Where(x => x.Character?.Identity != DynelManager.LocalPlayer.Identity && x.Character?.FightingTarget != null)
                    .OrderByDescending(x => x.IsLeader)
                    .ThenByDescending(x => x.Level)
                    .FirstOrDefault()?.Character?.FightingTarget;

                if (teamMemberTarget != null && finalTargets.Any(x => x.Identity == teamMemberTarget.Identity))
                {
                    return new List<SimpleChar> { teamMemberTarget };
                }

            }

            return finalTargets;
        }

        private Item GetTauntItem()
        {
            if (!_config.PathingConfig.UseTauntItem || DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology) || Item.HasPendingUse)
                return null;

            return Inventory.Items.FirstOrDefault(x => _tauntItemIds.Contains(x.Id));
        }

        private List<SimpleChar> OrderTargets(List<SimpleChar> finalTargets)
        {
            if (finalTargets.Count == 1)
                return finalTargets;

            return finalTargets
                .OrderBy(x =>
                {
                    int index = _roamPath.Rules.PriorityNames.IndexOf(x.Name);
                    return index != -1 ? index : int.MaxValue;
                })
                .ThenBy(x => x.FightingTarget?.Identity != DynelManager.LocalPlayer.Identity)
                .ThenBy(x => Vector3.Distance(x.Position, DynelManager.LocalPlayer.Position))
                .ToList();
        }

        public bool IsInTauntRange(SimpleChar target)
        {
            return target != null && _config.PathingConfig.UseTauntItem && target.IsInLineOfSight && Vector3.Distance(target.Position, DynelManager.LocalPlayer.Position) <= Roamba.Config.PathingConfig.TauntRange;
        }


        public bool IsInPathRange(SimpleChar target)
        {
            return _config.PathingConfig.PathToMobs && Vector3.Distance(target.Position, DynelManager.LocalPlayer.Position) <= _config.PathingConfig.PathRange;
        }

        public bool IsInWeaponsRange(SimpleChar target)
        {
            return target != null && target.IsInLineOfSight && target.IsInWeaponHitRange(1);
        }

        public void AddToIgnoreList(Identity identity)
        {
            if (!_ignoredIdentities.Contains(identity))
                _ignoredIdentities.Add(identity);
        }

        private void OnN3MessageReceived(object sender, N3Message n3Msg)
        {
            if (n3Msg is AttackMessage attackMsg)
                _ignoredIdentities.Remove(attackMsg.Identity);
        }
    }
}
