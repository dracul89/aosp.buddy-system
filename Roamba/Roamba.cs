﻿using AOSharp.Core;
using System;
using AOSharp.Pathfinding;
using Serilog.Core;
using AOSharp.Core.UI;

namespace Roamba
{
    public class Roamba : AOPluginEntry
    {
        public static RoambaConfig Config;
        public static RoamPath RoamPath;
        public static Logger Log;
        public static RoamStateMachine StateMachine;
        public static IPC Ipc;
        public static MainWindow MainWindow;

        public override void Run()
        {
            try
            {
                SMovementController.Set();

                Logger.Information("Roamba Loaded!");
                Log = Logger;

                CommonParameters.Init("Roamba");   // Init plugin name to create all relative directories
                XmlPath.Set(PluginDirectory);   // Set our xml paths for windows / views

                Config = RoambaConfig.LoadConfig($"{CommonParameters.PlayerSettingsPath}"); // Per character config containing all UI related settings
                RoamPath = RoamPath.Load(Config.RoamPath);  // RoamPath config which contains SPath and Targeting Rules

                OpenMainWindow();

                Chat.RegisterCommand("roamba", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    OpenMainWindow();
                });

                StateMachine = new RoamStateMachine(new MobTargeting(RoamPath, Config), Config.CoreConfig.OnInjectEnable);
                Ipc = new IPC(Config.CoreConfig.ChannelId);
            }
            catch (Exception e)
            {
                Logger.Warning(e.ToString());
            }
        }

        private void OpenMainWindow()
        {
            MainWindow = new MainWindow("Roamba",
                $"{XmlPath.WindowsRootDir}\\MainWindow.xml",
                $"{XmlPath.WindowsRootDir}\\InfoWindow.xml",
                $"{XmlPath.WindowsRootDir}\\RoamPathWindow.xml",
                $"{XmlPath.ViewsRootDir}\\BuddyCoreView.xml",
                $"{XmlPath.ViewsRootDir}\\PathSettingsView.xml",
                $"{XmlPath.ViewsRootDir}\\RoamPathInitView.xml",
                $"{XmlPath.ViewsRootDir}\\RoamPathMainView.xml");

            MainWindow.Show();
        }
    }
}
