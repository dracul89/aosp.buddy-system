﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using System.Linq;

namespace LeBuddy
{
    public static class Extensions
    {
        public static bool IsAtUnicornRecruiter()
        {
            return DynelManager.LocalPlayer.Position.DistanceFrom(Constants._unicornRecruiter) < 5.0f;
        }

        public static bool CanProceed()
        {
            return DynelManager.LocalPlayer.HealthPercent > LeBuddy.KitHealthPercentage
                && DynelManager.LocalPlayer.NanoPercent > LeBuddy.KitNanoPercentage
                && DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1
                && DynelManager.LocalPlayer.MovementState != MovementState.Sit
                && !Spell.HasPendingCast && Spell.List.Any(spell => spell.IsReady);
        }
    }
}
