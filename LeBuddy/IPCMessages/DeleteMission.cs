﻿using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace LeBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.DeleteMission)]
    public class DeleteMissionMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.DeleteMission;
    }
}
