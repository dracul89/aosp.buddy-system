﻿using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace LeBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.Looting)]
    public class LootingStatusMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Looting;

        [AoMember(0)]
        public bool IsLooting { get; set; }
    }
}
