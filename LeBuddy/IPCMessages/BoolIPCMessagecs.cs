﻿using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace LeBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StartStop)]
    public class BoolIPCMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StartStop;

        [AoMember(0)]
        public bool IsStarting { get; set; }

        [AoMember(1)]
		public bool IsLooting { get; set; }

        [AoMember(2)]
		public bool IsBuffing { get; set; }
		[AoMember(3)]

		public bool IsKitting { get; set; }
	}
}
