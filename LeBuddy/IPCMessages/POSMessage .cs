﻿using AOSharp.Common.GameData;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace LeBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.POS)]
    public class POSMessage : IPCMessage
    {
        [AoMember(0)]
        public Vector3 Position { get; set; }

        [AoMember(1)]
        public int Floor { get; set; }

        public override short Opcode => (short)IPCOpcode.POS;
    }
}
