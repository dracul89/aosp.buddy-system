﻿namespace LeBuddy.IPCMessages
{
    public enum IPCOpcode
    {
        StartStop = 1001,
        LeaderInfo = 1002,
        WaitAndReady = 1003,
        Enter = 1004,
        SelectedMemberUpdate = 1005,
        ClearSelectedMember = 1006,
        DeleteMission = 1007,
        Looting = 1008,
        POS = 1009,
    }
}
