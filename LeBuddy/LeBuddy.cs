﻿using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Core;
using AOSharp.Core.IPC;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using LeBuddy.IPCMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SharpNav;

namespace LeBuddy
{
    public class LeBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static SMovementController SMovementController { get; set; }
        public static IPCChannel IPCChannel { get; set; }

        public bool _mesh;
        public static Config Config { get; private set; }

        public static bool Enable = false;
        public static bool Looting = false;
        public static bool Kitting = false;
        public static bool Buffing = false;

        public static double missionTimer;
        private static double readyTimer;

        public static int level;

        public static int KitHealthPercentage = 0;
        public static int KitNanoPercentage = 0;

        public static SimpleChar _leader;
        public static Identity Leader = Identity.None;
        public static Vector3 _leaderPos = Vector3.Zero;
        public static int _leaderFloor;

        public static bool Ready = true;
        public static Dictionary<Identity, bool> teamReadiness = new Dictionary<Identity, bool>();
        public static Dictionary<Vector3, int> _downButtonLocation = new Dictionary<Vector3, int>();
        public static Dictionary<Vector3, int> _upButtonLocations = new Dictionary<Vector3, int>();
        public static Vector3 _exitDoorLocation = Vector3.Zero;
        public static Dictionary<int, Tuple<Vector3, int>> _mobLocations = new Dictionary<int, Tuple<Vector3, int>>();


        private static bool lastSentIsReadyState;

        public static Door _exitDoor;

        public static double _stateTimeOut;

        private static Window infoWindow;
        private static Window kittingWindow;

        public static string PluginDir;

        public static Settings _settings;

        public static string previousErrorMessage = string.Empty;

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("LeBuddy");

                PluginDir = pluginDir;
                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{CommonParameters.BasePath}\\{CommonParameters.AppPath}\\LeBuddy\\{DynelManager.LocalPlayer.Name}\\Config.json");

                SMovementControllerSettings mSettings = new SMovementControllerSettings
                {
                    NavMeshSettings = new SNavMeshSettings { DrawNavMesh = false, DrawDistance = 30 },

                    PathSettings = new SPathSettings
                    {
                        DrawPath = true,
                        MinRotSpeed = 10,
                        MaxRotSpeed = 30,
                        UnstuckUpdate = 5000,
                        UnstuckThreshold = 2f,
                        RotUpdate = 10,
                        MovementUpdate = 200,
                        PathRadius = 0.29f,
                        Extents = new Vector3(1.0f, 0.1f, 1.0f)
                    }
                };

                SMovementController.Set(mSettings);

                SMovementController.AutoLoadNavmeshes($"{PluginDir}\\NavMeshes");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel));
                KitHealthPercentage = Config.CharSettings[DynelManager.LocalPlayer.Name].KitHealthPercentage;
                KitNanoPercentage = Config.CharSettings[DynelManager.LocalPlayer.Name].KitNanoPercentage;

                IPCChannel.RegisterCallback((int)IPCOpcode.StartStop, BoolOnOffMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Enter, EnterMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.LeaderInfo, OnLeaderInfoMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.WaitAndReady, OnWaitAndReadyMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.DeleteMission, DeleteMissionMessage);
                //IPCChannel.RegisterCallback((int)IPCOpcode.Looting, OnLootingStatusMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.SelectedMemberUpdate, HandleSelectedMemberUpdate);
                IPCChannel.RegisterCallback((int)IPCOpcode.ClearSelectedMember, HandleClearSelectedMember);
                IPCChannel.RegisterCallback((int)IPCOpcode.POS, POSRecieved);

                Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[DynelManager.LocalPlayer.Name].KitHealthPercentageChangedEvent += KitHealthPercentage_Changed;
                Config.CharSettings[DynelManager.LocalPlayer.Name].KitNanoPercentageChangedEvent += KitNanoPercentage_Changed;

                SettingsController.RegisterSettingsWindow("LeBuddy", pluginDir + "\\UI\\LeBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                Chat.RegisterCommand("enable", BuddyCommand);

                Chat.RegisterCommand("gen", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    SharpNavGenState.NavGen();
                });

                Chat.RegisterCommand("load", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    if (SNavMeshSerializer.LoadFromFile($"{PluginDir}\\NavMeshes\\{Playfield.ModelIdentity.Instance}.nav", out NavMesh navMesh))
                    {
                        SMovementController.LoadNavmesh(navMesh);
                    }
                });

                Chat.RegisterCommand("mesh", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    SMovementController.ToggleNavMeshDraw();
                });

                Chat.RegisterCommand("print", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    if (DynelManager.LocalPlayer.Identity == Leader)
                    {
                        if (Playfield.IsDungeon)
                        {
                            Chat.WriteLine($"Team is ready: {Ready}");
                            Chat.WriteLine($"Player floor: {DynelManager.LocalPlayer.Room.Floor}");
                            Chat.WriteLine($"Visited rooms: {PathState.visitedRooms.Count()}");
                            Chat.WriteLine($"Total rooms: {PathState.linkedRooms.Count()}");

                            foreach (var upButton in _upButtonLocations)
                            {
                                Chat.WriteLine($"Up button: Position: {upButton.Key}, Floor: {upButton.Value}");
                            }

                            foreach (var downButton in _downButtonLocation)
                            {
                                Chat.WriteLine($"Down button: Position: {downButton.Key}, Floor: {downButton.Value}");
                            }

                            foreach (var mob in _mobLocations)
                            {
                                Chat.WriteLine($"Mob: {mob.Key}, Position: {mob.Value.Item1}, Floor: {mob.Value.Item2}");
                            }
                        }
                    }
                });

                Game.OnUpdate += OnUpdate;
                Game.TeleportEnded += Reset;
                Team.TeamRequest += TeamRequest;
                Team.MemberLeft += MemberLeft;
                //SMovementController.Stuck += OnStuck;
                //SMovementController.AgentStateChange += OffMesh;

                _settings.AddVariable("Enable", false);
                _settings.AddVariable("Looting", false);
                _settings.AddVariable("Stop", false);
                _settings.AddVariable("Kitting", false);
                _settings.AddVariable("Buffing", false);
                _settings.AddVariable("Merge", false);

                _settings.AddVariable("DifficultySelection", (int)DifficultySelection.Hard);

                _settings["Enable"] = false;
                _settings["Stop"] = false;

                if (Game.IsNewEngine)
                {
                    Chat.WriteLine("Does not work on this engine!");
                }
                else
                {
                    Chat.WriteLine("LeBuddy Loaded!");
                    Chat.WriteLine("/buddy for settings. /enable to start and stop.");
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        private void OnStuck(object sender, Vector3 e)
        {
            if (Playfield.IsDungeon)
            {
                if (DynelManager.LocalPlayer.MovementState != MovementState.Sit)
                {
                    Chat.WriteLine("Stuck!!");

                    WarpToClosestDynel();
                }
            }
        }

        private void OffMesh(object sender, NavMeshQuery.AgentNavMeshState e)
        {
            if (Playfield.IsDungeon)
            {
                if (e == NavMeshQuery.AgentNavMeshState.OutNavMesh)
                {
                    Chat.WriteLine("Off mesh!");

                    WarpToClosestDynel();
                }
            }
        }

        private void WarpToClosestDynel()
        {
            TeamMember member = Team.Members.FirstOrDefault(m => m.Character != null);

            Dynel anyDynel = DynelManager.AllDynels.Where(dynel => dynel.Identity != DynelManager.LocalPlayer.Identity).
                OrderBy(name => name.Name.Contains("Button")).
                ThenBy(team => team.Identity == member.Identity)
                .FirstOrDefault();

            if (anyDynel != null)
            {
                if (SMovementController.IsNavigating())
                {
                    SMovementController.Halt();
                }
                else
                {
                    DynelManager.LocalPlayer.Position = anyDynel.Position;
                }
            }
        }

        private void MemberLeft(object sender, Identity e)
        {
            Chat.WriteLine($"Member left msg recieved");
            Chat.WriteLine($"Member identity: {e}");
            SimpleChar name = DynelManager.Characters.Where(n => n.Identity == e).FirstOrDefault();

            if (DynelManager.LocalPlayer.Identity == Leader)
            {
                if (ReformState._teamCache.Contains(e))
                {
                    Chat.WriteLine($"Team member {name.Name} left! Removing from _teamCache.");
                    ReformState._teamCache.Remove(e);
                }
            }
        }

        private void Reset(object sender, EventArgs e)
        {
            var localPlayer = DynelManager.LocalPlayer;

            if (localPlayer.Identity != Leader)
            {
                if (Time.AONormalTime > readyTimer + 4)
                {
                    if (!Spell.HasPendingCast && localPlayer.NanoPercent > KitNanoPercentage
                            && localPlayer.HealthPercent > KitHealthPercentage && Spell.List.Any(spell => spell.IsReady))
                    {
                        IPCChannel.Broadcast(new WaitAndReadyIPCMessage
                        {
                            IsReady = true,
                            PlayerIdentity = localPlayer.Identity,
                        });
                    }
                }

                readyTimer = Time.AONormalTime;
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }
        public static void KitHealthPercentage_Changed(object s, int e)
        {
            Config.CharSettings[DynelManager.LocalPlayer.Name].KitHealthPercentage = e;
            KitHealthPercentage = e;
            Config.Save();
        }
        public static void KitNanoPercentage_Changed(object s, int e)
        {
            Config.CharSettings[DynelManager.LocalPlayer.Name].KitNanoPercentage = e;
            KitNanoPercentage = e;
            Config.Save();
        }

        private void InfoView(object s, ButtonBase button)
        {
            infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\LeBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            infoWindow.Show(true);
        }

        private void KittingView(object s, ButtonBase button)
        {
            kittingWindow = Window.CreateFromXml("Kitting", PluginDir + "\\UI\\LeBuddyKittingView.xml",
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            kittingWindow.FindView("KitHealthPercentageBox", out TextInputView healthBox);
            kittingWindow.FindView("KitNanoPercentageBox", out TextInputView nanoBox);

            if (healthBox != null)
            {
                healthBox.Text = $"{KitHealthPercentage}";
            }

            if (nanoBox != null)
            {
                nanoBox.Text = $"{KitNanoPercentage}";
            }

            kittingWindow.Show(true);
        }

        private void Start()
        {
            Enable = true;

            Chat.WriteLine("LeBuddy enabled.");

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }
        }

        private void Stop()
        {
            Enable = false;

            Chat.WriteLine("LeBuddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        private void LootingEnabled()
        {
            Chat.WriteLine("Looting Enabled.");
            Looting = true;
        }
        private void LootingDisabled()
        {
            Chat.WriteLine("Looting Disabled");
            Looting = false;
        }

        private void KittingEnabled()
        {
            Chat.WriteLine("Kitting Enabled.");
            Kitting = true;
        }
        private void KittingDisabled()
        {
            Chat.WriteLine("Kitting Disabled");
            Kitting = false;
        }

        private void BuffingEnabled()
        {
            Chat.WriteLine("Buffing Enabled.");
            Buffing = true;
        }
        private void BuffingDisabled()
        {
            Chat.WriteLine("Buffing Disabled");
            Buffing = false;
        }

        private void BoolOnOffMessage(int sender, IPCMessage msg)
        {
            if (msg is BoolIPCMessage boolMsg)
            {
                if (boolMsg.IsStarting == true)
                {
                    _settings["Enable"] = true;
                    Start();
                }
                if (boolMsg.IsStarting == false)
                {
                    _settings["Enable"] = false;
                    Stop();
                }
                if (boolMsg.IsLooting == true)
                {
                    _settings["Looting"] = true;
                    LootingEnabled();
                }
                if (boolMsg.IsLooting == false)
                {
                    _settings["Looting"] = false;
                    LootingDisabled();
                }
                if (boolMsg.IsKitting == true)
                {
                    _settings["Kitting"] = true;
                    KittingEnabled();
                }
                if (boolMsg.IsKitting == false)
                {
                    _settings["Kitting"] = false;
                    KittingDisabled();
                }
                if (boolMsg.IsBuffing == true)
                {
                    _settings["Buffing"] = true;
                    BuffingEnabled();
                }
                if (boolMsg.IsBuffing == false)
                {
                    _settings["Buffing"] = false;
                    BuffingDisabled();
                }
            }
        }

        private void EnterMessage(int sender, IPCMessage msg)
        {
            if (Playfield.ModelIdentity.Instance == Constants.UnicornOutpost)
            {
                if (!(_stateMachine.CurrentState is EnterState))
                {
                    _stateMachine.SetState(new EnterState());
                }
            }
        }

        private void DeleteMissionMessage(int sender, IPCMessage msg)
        {
            foreach (Mission mission in Mission.List)
            {
                if (mission != null)
                {
                    if (mission.DisplayName.Contains("Infiltrate the alien ships!"))
                    {
                        Chat.WriteLine("Deleting mission");
                        mission.Delete();
                    }
                }
            }    
        }

        private void HandleSelectedMemberUpdate(int sender, IPCMessage msg)
        {
            SelectedMemberUpdateMessage message = msg as SelectedMemberUpdateMessage;

            if (message != null)
            {
                IdleState.selectedMember = message.SelectedMemberIdentity;
            }
        }
        private void HandleClearSelectedMember(int sender, IPCMessage msg)
        {
            IdleState.selectedMember = Identity.None;
        }

        private void OnLeaderInfoMessage(int sender, IPCMessage msg)
        {
            if (msg is LeaderInfoIPCMessage leaderInfoMessage)
            {
                if (leaderInfoMessage.IsRequest)
                {
                    if (DynelManager.LocalPlayer.Identity == Leader)
                    {
                        IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = Leader, IsRequest = false });
                    }
                }
                else
                {
                    Leader = leaderInfoMessage.LeaderIdentity;
                }
            }
        }
        private void OnWaitAndReadyMessage(int sender, IPCMessage msg)
        {
            if (msg is WaitAndReadyIPCMessage waitAndReadyMessage)
            {
                if (DynelManager.LocalPlayer.Identity == Leader)
                {
                    Identity senderIdentity = waitAndReadyMessage.PlayerIdentity;

                    SimpleChar name = DynelManager.Characters.Where(m => m.Identity == senderIdentity).FirstOrDefault();

                    teamReadiness[senderIdentity] = waitAndReadyMessage.IsReady;

                    bool allReady = true;

                    foreach (var teamMember in Team.Members)
                    {
                        if (teamReadiness.ContainsKey(teamMember.Identity) && !teamReadiness[teamMember.Identity])
                        {
                            allReady = false;
                            break;
                        }
                    }

                    if (Leader == DynelManager.LocalPlayer.Identity)
                    {
                        Ready = allReady;

                        if (Time.AONormalTime > readyTimer + 60)
                        {
                            Ready = true;
                        }
                        readyTimer = Time.AONormalTime;
                    }
                }
            }
        }

        private void POSRecieved(int arg1, IPCMessage message)
        {
            POSMessage msg = message as POSMessage;

            _leaderPos = msg.Position;
            _leaderFloor = msg.Floor;
        }

        private void OnUpdate(object s, float deltaTime)
        {
            try
            {
                if (Game.IsZoning) { return; }

                if (_stateMachine != null)
                {
                    _stateMachine.Tick();
                }
                else
                {
                    Chat.WriteLine("StateMachine is null");
                }

                Shared.Kits kitsInstance = new Shared.Kits();

                kitsInstance.SitAndUseKit(KitNanoPercentage, KitHealthPercentage);

                if (Playfield.IsDungeon)
                {
                    if (DynelManager.LocalPlayer.Room.Floor != level)
                    {
                        Chat.WriteLine($"Player changed floors");
                        level = DynelManager.LocalPlayer.Room.Floor;
                    }
                }

                if (Team.IsInTeam)
                {
                    if (Leader == Identity.None)
                    {
                        if (Team.IsLeader)
                        {
                            Leader = DynelManager.LocalPlayer.Identity;
                        }
                        else
                        {
                            if (_settings["Merge"].AsBool())
                            {
                                SimpleChar teamLeader = Team.Members.FirstOrDefault(member => member.IsLeader)?.Character;

                                Leader = teamLeader?.Identity ?? Identity.None;
                            }
                            else
                            {
                                IPCChannel.Broadcast(new LeaderInfoIPCMessage() { IsRequest = true });
                            }
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Identity != Leader)
                        {
                            var localPlayer = DynelManager.LocalPlayer;
                            bool currentIsReadyState = true;

                            if (_settings["Kitting"].AsBool())
                            {
                                if (!Shared.Kits.InCombat())
                                {
                                    if (localPlayer.NanoPercent < KitNanoPercentage || localPlayer.HealthPercent < KitHealthPercentage)
                                    {
                                        currentIsReadyState = false;
                                    }
                                }
                            }
                            if (_settings["Buffing"].AsBool())
                            {
                                if (!Shared.Kits.InCombat())
                                {
                                    if (Spell.HasPendingCast || !Spell.List.Any(spell => spell.IsReady))
                                    {
                                        currentIsReadyState = false;
                                    }
                                }
                            }
                            if (!Spell.HasPendingCast && Spell.List.Any(spell => spell.IsReady)
                                && localPlayer.NanoPercent > KitNanoPercentage && localPlayer.HealthPercent > KitHealthPercentage)
                            {
                                currentIsReadyState = true;
                            }
                            if (currentIsReadyState != lastSentIsReadyState)
                            {
                                IPCChannel.Broadcast(new WaitAndReadyIPCMessage
                                {
                                    IsReady = currentIsReadyState,
                                    PlayerIdentity = DynelManager.LocalPlayer.Identity,
                                });

                                lastSentIsReadyState = currentIsReadyState;
                            }
                        }
                        else
                        {
                            foreach (TeamMember member in Team.Members)
                            {
                                if (member.Identity == Leader)
                                {
                                    continue;
                                }

                                if (!ReformState._teamCache.Contains(member.Identity))
                                {
                                    ReformState._teamCache.Add(member.Identity);
                                }
                            }
                        }
                    }
                }

                #region UI

                if (kittingWindow != null && kittingWindow.IsValid)
                {
                    kittingWindow.FindView("KitHealthPercentageBox", out TextInputView kitHealthInput);
                    kittingWindow.FindView("KitNanoPercentageBox", out TextInputView kitNanoInput);

                    if (kitHealthInput != null && !string.IsNullOrEmpty(kitHealthInput.Text))
                    {
                        if (int.TryParse(kitHealthInput.Text, out int kitHealthValue))
                        {
                            if (Config.CharSettings[DynelManager.LocalPlayer.Name].KitHealthPercentage != kitHealthValue)
                            {
                                Config.CharSettings[DynelManager.LocalPlayer.Name].KitHealthPercentage = kitHealthValue;
                            }
                        }
                    }

                    if (kitNanoInput != null && !string.IsNullOrEmpty(kitNanoInput.Text))
                    {
                        if (int.TryParse(kitNanoInput.Text, out int kitNanoValue))
                        {
                            if (Config.CharSettings[DynelManager.LocalPlayer.Name].KitNanoPercentage != kitNanoValue)
                            {
                                Config.CharSettings[DynelManager.LocalPlayer.Name].KitNanoPercentage = kitNanoValue;
                            }
                        }
                    }
                }

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);

                    if (channelInput != null)
                    {
                        if (int.TryParse(channelInput.Text, out int channelValue)
                            && Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel != channelValue)
                        {
                            Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel = channelValue;
                        }
                    }

                    if (SettingsController.settingsWindow.FindView("LeBuddyInfoView", out Button infoView))
                    {
                        infoView.Tag = SettingsController.settingsWindow;
                        infoView.Clicked = InfoView;
                    }

                    if (SettingsController.settingsWindow.FindView("LeBuddyKittingView", out Button kittingView))
                    {
                        kittingView.Tag = SettingsController.settingsWindow;
                        kittingView.Clicked = KittingView;
                    }

                    if (!_settings["Enable"].AsBool() && Enable)
                    {
                        IPCChannel.Broadcast(new BoolIPCMessage()
                        {
                            IsStarting = false,
                            IsLooting = Looting,
                            IsBuffing = Buffing,
                            IsKitting = Kitting
                        });
                        Stop();
                    }
                    if (_settings["Enable"].AsBool() && !Enable)
                    {
                        IPCChannel.Broadcast(new BoolIPCMessage()
                        {
                            IsStarting = true,
                            IsLooting = Looting,
                            IsBuffing = Buffing,
                            IsKitting = Kitting
                        });
                        Start();
                    }

                    if (!_settings["Looting"].AsBool() && Looting)
                    {
                        IPCChannel.Broadcast(new BoolIPCMessage
                        {
                            IsStarting = Enable,
                            IsLooting = false,
                            IsBuffing = Buffing,
                            IsKitting = Kitting
                        });
                        LootingDisabled();
                    }
                    if (_settings["Looting"].AsBool() && !Looting)
                    {
                        IPCChannel.Broadcast(new BoolIPCMessage
                        {
                            IsStarting = Enable,
                            IsLooting = true,
                            IsBuffing = Buffing,
                            IsKitting = Kitting
                        });
                        LootingEnabled();
                    }
                    if (!_settings["Kitting"].AsBool() && Kitting)
                    {
                        IPCChannel.Broadcast(new BoolIPCMessage
                        {
                            IsStarting = Enable,
                            IsLooting = Looting,
                            IsBuffing = Buffing,
                            IsKitting = false,
                        });
                        KittingDisabled();
                    }
                    if (_settings["Kitting"].AsBool() && !Kitting)
                    {
                        IPCChannel.Broadcast(new BoolIPCMessage
                        {
                            IsStarting = Enable,
                            IsLooting = Looting,
                            IsBuffing = Buffing,
                            IsKitting = true,
                        });
                        KittingEnabled();
                    }
                    if (!_settings["Buffing"].AsBool() && Buffing)
                    {
                        IPCChannel.Broadcast(new BoolIPCMessage
                        {
                            IsStarting = Enable,
                            IsLooting = Looting,
                            IsBuffing = false,
                            IsKitting = Kitting,
                        });
                        BuffingDisabled();
                    }
                    if (_settings["Buffing"].AsBool() && !Buffing)
                    {
                        IPCChannel.Broadcast(new BoolIPCMessage
                        {
                            IsStarting = Enable,
                            IsLooting = Looting,
                            IsBuffing = true,
                            IsKitting = Kitting,
                        });
                        BuffingEnabled();
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public static bool MissionExist()
        {
            if (Time.AONormalTime > missionTimer + 5)
            {
                if (DynelManager.LocalPlayer != null)
                {
                    if (!Mission.List.Exists(x => x.DisplayName.Contains("Infiltrate the alien ships!")))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void BuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Enable"].AsBool())
                    {
                        _settings["Enable"] = true;

                        IPCChannel.Broadcast(new BoolIPCMessage()
                        {
                            IsStarting = true,
                            IsLooting = _settings["Looting"].AsBool(),
                            IsBuffing = _settings["Buffing"].AsBool(),
                            IsKitting = _settings["Kitting"].AsBool(),
                        });
                        Start();
                    }
                    else
                    {
                        _settings["Enable"] = false;

                        IPCChannel.Broadcast(new BoolIPCMessage()
                        {
                            IsStarting = false,
                            IsLooting = _settings["Looting"].AsBool(),
                            IsBuffing = _settings["Buffing"].AsBool(),
                            IsKitting = _settings["Kitting"].AsBool(),
                        });
                        Stop();
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public enum DifficultySelection
        {
            Easy, Medium, Hard
        }

        private void TeamRequest(object sender, TeamRequestEventArgs e)
        {
            if (_settings["Merge"].AsBool())
            {
                if (Leader == Identity.None)
                {
                    Leader = e.Requester;
                }
                if (e.Requester == Leader)
                {
                    e.Accept();
                }
            }
        }

        public static class RelevantNanos { }

        public static class RelevantItems { }

        public static int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;

            var lineMatch = Regex.Match(ex.StackTrace ?? "", @":line (\d+)$", RegexOptions.Multiline);

            if (lineMatch.Success)
            {
                lineNumber = int.Parse(lineMatch.Groups[1].Value);
            }

            return lineNumber;
        }
    }
}
