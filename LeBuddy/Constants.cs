﻿using AOSharp.Common.GameData;
using System.Collections.Generic;

namespace LeBuddy
{
    public static class Constants
    {
        public const string UnicornRecruiter = "Unicorn Recruiter";

        public static List<string> _ignores = new List<string>
        {
            "Zix",
            "Nanovoider",
        };

        public static Vector3 _unicornRecruiter = new Vector3(151.6924f, 100.905f, 164.38f); //Unicorn Recruiter, Position: (151.6924, 100.905, 164.38)
        public static Vector3 _reclaim = new Vector3(160.0f, 101.0f, 177.1f);
        public static Vector3 _reformArea = new Vector3(64.7f, 100.7f, 187.1f);//64.7, 187.1, 100.7
        public static Vector3 _entrance = new Vector3(64.5f, 101.1f, 181.8f); //Alien Mothership, Position: (64.5, 101.1, 181.8)
        public static Vector3 _entranceStart = new Vector3(65.1f, 100.7f, 184.2f);
        public static Vector3 _entranceEnd = new Vector3(64.5f, 101.1f, 180.6f); 
        public static Vector3 _buttonExit = new Vector3(150.7f, 520.0f, 163.9f);

        public const int UnicornOutpost = 4364;
    }
}
