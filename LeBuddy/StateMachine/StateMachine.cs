﻿using AOSharp.Core.UI;
using System;

namespace LeBuddy
{
    public class StateMachine
    {
        public IState CurrentState { get; private set; } = null;

        public StateMachine(IState defaultState)
        {
            SetState(defaultState);
        }

        public void Tick()
        {
            try
            {
                IState nextState = CurrentState.GetNextState();

                if (nextState != null)
                {
                    SetState(nextState);
                }

                CurrentState.Tick();
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + LeBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != LeBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    LeBuddy.previousErrorMessage = errorMessage;
                }
            }
        }
        public void SetState(IState state, bool triggerEvents = true)
        {
            if (CurrentState != null && triggerEvents)
            {
                CurrentState.OnStateExit();
            }

            CurrentState = state;

            if (triggerEvents)
            {
                state.OnStateEnter();
            }
        }
    }
}
