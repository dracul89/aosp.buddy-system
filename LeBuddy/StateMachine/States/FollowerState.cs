﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LeBuddy
{
    public class FollowerState : IState
    {
        Dynel _upButton;
        Dynel _downButton;

        SimpleChar targetMob;
        Door _exitDoor;

        public static double _buttonTimer;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (!LeBuddy._settings["Enable"].AsBool() || !Playfield.IsDungeon)
            {
                return new IdleState();
            }
            else
            {
                if (DynelManager.LocalPlayer.Room.Floor != LeBuddy.level)
                {
                    return new IdleState();
                }
                else
                {
                    if (!Team.IsInTeam || !LeBuddy.MissionExist())
                    {
                        return new ButtonExitState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Follower");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }
        public void OnStateExit()
        {
            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack();
            }
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }
        public void Tick()
        {
            try
            {
                if (Game.IsZoning || !Team.IsInTeam) { return; }

                _downButton = DynelManager.AllDynels
                 .Where(c => c.Name == "Button (down)")
                 .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                 .FirstOrDefault();

                _upButton = DynelManager.AllDynels
                .Where(c => (c.Name == "Button (up)" || c.Name == "Button (boss)") && c.Room.Floor == LeBuddy.level)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();

                _exitDoor = Playfield.Doors.FirstOrDefault(d =>
                        (d.RoomLink1 != null && d.RoomLink2 == null) || (d.RoomLink1 == null && d.RoomLink2 != null));

                LeBuddy._leader = Team.Members.Where(c => c.Character?.Health > 0
                && c.Character?.IsValid == true && c.Identity == LeBuddy.Leader).FirstOrDefault()?.Character;

                if (LeBuddy._leader != null)
                {
                    LeBuddy._leaderPos = LeBuddy._leader.Position;

                    if (LeBuddy._leader?.FightingTarget != null)
                    {
                        targetMob = DynelManager.NPCs
                           .Where(c => c.Health > 0
                               && c.Identity == (Identity)LeBuddy._leader?.FightingTarget?.Identity)
                           .FirstOrDefault();
                    }
                }

                if (_exitDoor != null && DynelManager.LocalPlayer.Room.Name == "Mothership_entrance")
                {
                    if (LeBuddy._exitDoorLocation == Vector3.Zero)
                    {
                        LeBuddy._exitDoorLocation = _exitDoor.Position;
                    }
                }

                if (_downButton != null)
                {
                    if (!LeBuddy._downButtonLocation.ContainsKey(_downButton.Position))
                    {
                        LeBuddy._downButtonLocation.Add(_downButton.Position, _downButton.Room.Floor);
                    }
                }

                if (_upButton != null)
                {
                    if (!LeBuddy._upButtonLocations.ContainsKey(_upButton.Position))
                    {
                        LeBuddy._upButtonLocations.Add(_upButton.Position, _upButton.Room.Floor);
                    }
                }

                if (LeBuddy._leader != null)
                {
                    if (LeBuddy._leader?.IsAttacking == true)
                    {
                        if (targetMob != null)
                        {
                            PathState.HandleMobs(targetMob);
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Position.Distance2DFrom(LeBuddy._leaderPos) > 2 || !LeBuddy._leader.IsInLineOfSight)
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                SMovementController.SetNavDestination(LeBuddy._leaderPos);
                            }
                        }
                        else
                        {
                            if (SMovementController.IsNavigating())
                            {
                                SMovementController.Halt();
                            }
                        }
                    }
                }
                else
                {
                    if (LeBuddy._leaderFloor == LeBuddy.level)
                    {
                        SMovementController.SetNavDestination(LeBuddy._leaderPos);
                    }
                    else
                    {
                        if (LeBuddy._leaderFloor > LeBuddy.level)
                        {
                            if (_upButton != null)
                            {
                                if (_upButton.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 2f)
                                {
                                    if (SMovementController.IsNavigating())
                                    {
                                        SMovementController.Halt();
                                    }
                                    else
                                    {
                                        var lvlCD = DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Level);

                                        if (!lvlCD)
                                        {
                                            if (Time.AONormalTime > _buttonTimer + 3.0)
                                            {
                                                _buttonTimer = Time.AONormalTime;

                                                _upButton.Use();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    SMovementController.SetNavDestination(_upButton.Position);
                                }
                            }
                        }

                        if (LeBuddy._leaderFloor < LeBuddy.level)
                        {
                            if (_downButton != null)
                            {
                                if (!SMovementController.IsNavigating())
                                {
                                    SMovementController.SetNavDestination(_downButton.Position);
                                }
                            }
                            else
                            {
                                if (LeBuddy._downButtonLocation.Count > 0)
                                {
                                    foreach (var buttonEntry in LeBuddy._downButtonLocation)
                                    {
                                        if (buttonEntry.Value == LeBuddy.level)
                                        {
                                            if (DynelManager.LocalPlayer.Position.DistanceFrom(buttonEntry.Key) > 5)
                                            {
                                                if (!SMovementController.IsNavigating())
                                                {
                                                    SMovementController.SetNavDestination(buttonEntry.Key);
                                                }

                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + LeBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != LeBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    LeBuddy.previousErrorMessage = errorMessage;
                }
            }
        }
    }
}
