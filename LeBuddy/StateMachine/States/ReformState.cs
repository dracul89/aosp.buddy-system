﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace LeBuddy
{
    public class ReformState : IState
    {
        private const float ReformTimeout = 180;
        private const float InviteDelay = 5;

        private double _reformStartedTime;

        public static HashSet<Identity> _teamCache = new HashSet<Identity>();
        public static HashSet<Identity> _invitedList = new HashSet<Identity>();


        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Team.IsInTeam)
            {
                if (_teamCache.Count == (Team.Members.Count - 1)) // Subtract 1 for the leader
                {
                    return new IdleState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Reforming");
            _reformStartedTime = Time.AONormalTime;
        }

        public void OnStateExit()
        {
            _invitedList.Clear();
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            InvitePlayers();
        }

        private void InvitePlayers()
        {
            if (Time.AONormalTime <= _reformStartedTime + InviteDelay)
            {
                return;
            }

            foreach (SimpleChar player in DynelManager.Players.Where(c => c.IsInPlay && _teamCache.Contains(c.Identity)
            && !_invitedList.Contains(c.Identity)))
            {
                _invitedList.Add(player.Identity);
                Team.Invite(player.Identity);
            }
        }
    }
}

