﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Core.Misc;
using AOSharp.Core.Movement;
using LeBuddy.IPCMessages;

namespace LeBuddy
{
    public class PathState : IState
    {
        private SimpleChar OutOfSightMob;
        private SimpleChar InLineOfSightMob;
        private static Corpse _corpse;
        private Dynel _upButton;
        private Dynel _downButton;

        static bool POSMsg;

        public static double _buttonTimer;
        public static AutoResetInterval delay;


        public static Dictionary<string, Tuple<Vector3, int>> linkedRooms = new Dictionary<string, Tuple<Vector3, int>>();
        public static Dictionary<string, Tuple<Vector3, int>> visitedRooms = new Dictionary<string, Tuple<Vector3, int>>();

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (!LeBuddy._settings["Enable"].AsBool() || !Playfield.IsDungeon)
            {
                return new IdleState();
            }
            else
            {
                if (DynelManager.LocalPlayer.Room.Floor != LeBuddy.level)
                {
                    return new IdleState();
                }
                else
                {
                    if (!Team.IsInTeam || !LeBuddy.MissionExist())
                    {
                        return new ButtonExitState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("PathState");

            POSMsg = false;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            Chat.WriteLine("Exit PathState");

            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack();
            }
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }

            linkedRooms.Clear();
            visitedRooms.Clear();
            LeBuddy._mobLocations.Clear();

            SharpNavGenState.Instance = Playfield.ModelIdentity.Instance;
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning || !Team.IsInTeam) { return; }

                Door _exitDoor = Playfield.Doors.FirstOrDefault(d =>
                        (d.RoomLink1 != null && d.RoomLink2 == null) || (d.RoomLink1 == null && d.RoomLink2 != null));

                LockedDoors();

                InLineOfSightMob = DynelManager.NPCs
                .Where(c => c.Health > 0 && !Constants._ignores.Contains(c.Name) && c.IsInLineOfSight)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .ThenBy(c => c.HealthPercent)
                .FirstOrDefault();

                OutOfSightMob = DynelManager.NPCs
                .Where(c => c.Health > 0 && !Constants._ignores.Contains(c.Name) && !c.IsInLineOfSight && c.Room.Floor == LeBuddy.level)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .ThenBy(c => c.HealthPercent)
                .FirstOrDefault();

                _corpse = DynelManager.Corpses.Where(c=>c.Room.Floor == LeBuddy.level)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();

                _downButton = DynelManager.AllDynels
                .Where(c => c.Name == "Button (down)")
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();

                _upButton = DynelManager.AllDynels
                .Where(c => (c.Name == "Button (up)" || c.Name == "Button (boss)") && c.Room.Floor == LeBuddy.level)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();

                if (_exitDoor != null && DynelManager.LocalPlayer.Room.Name == "Mothership_entrance")
                {
                    if (LeBuddy._exitDoorLocation == Vector3.Zero)
                    {
                        LeBuddy._exitDoorLocation = _exitDoor.Position;
                    }
                }

                if (_downButton != null)
                {
                    if (!LeBuddy._downButtonLocation.ContainsKey(_downButton.Position))
                    {
                        LeBuddy._downButtonLocation.Add(_downButton.Position, _downButton.Room.Floor);
                    }
                }

                if (_upButton != null)
                {
                    if (!LeBuddy._upButtonLocations.ContainsKey(_upButton.Position))
                    {
                        LeBuddy._upButtonLocations.Add(_upButton.Position, _upButton.Room.Floor);
                    }
                }

                if ((InLineOfSightMob == null || !InLineOfSightMob.IsInLineOfSight) && DynelManager.LocalPlayer.IsAttacking)
                {
                    DynelManager.LocalPlayer.StopAttack();
                }

                if (InLineOfSightMob == null)
                {
                    foreach (SimpleChar mobs in DynelManager.NPCs)
                    {
                        if (!LeBuddy._mobLocations.ContainsKey(mobs.Identity.Instance) && !Constants._ignores.Contains(mobs.Name)
                            && mobs.Health > 0)
                        {
                            LeBuddy._mobLocations[mobs.Identity.Instance] = new Tuple<Vector3, int>(mobs.Position, mobs.Room.Floor);
                        }
                    }
                }

                if (InLineOfSightMob != null)
                {
                    if (InLineOfSightMob.IsInAttackRange() && InLineOfSightMob.IsInLineOfSight)
                    {
                        if (LeBuddy._mobLocations.ContainsKey(InLineOfSightMob.Identity.Instance))
                        {
                            LeBuddy._mobLocations.Remove(InLineOfSightMob.Identity.Instance);
                        }
                    }
                }

                if (Team.Members.Any(m => m.Character == null) || !LeBuddy.Ready)
                {
                    if (SMovementController.IsNavigating())
                    {
                        SMovementController.Halt();
                    }

                    if (Team.Members.Any(missing => missing.Character == null))
                    {
                        if (!POSMsg)
                        {
                            LeBuddy.IPCChannel.Broadcast(new POSMessage
                            {
                                Position = DynelManager.LocalPlayer.Position,
                                Floor = DynelManager.LocalPlayer.Room.Floor,
                            });

                            POSMsg = true;
                        }
                    }

                    return;
                }

                foreach (Door door in Playfield.Doors)
                {
                    var roomLink1 = door.RoomLink1;
                    var roomLink2 = door.RoomLink2;

                    if (roomLink1 != null && !linkedRooms.ContainsKey(roomLink1.Name))
                    {
                        linkedRooms[roomLink1.Name] = new Tuple<Vector3, int>(roomLink1.Position, roomLink1.Floor);
                    }
                    if (roomLink2 != null && !linkedRooms.ContainsKey(roomLink2.Name))
                    {
                        linkedRooms[roomLink2.Name] = new Tuple<Vector3, int>(roomLink2.Position, roomLink2.Floor);
                    }
                }

                var currentRoom = DynelManager.LocalPlayer.Room;

                if (currentRoom != null && currentRoom.Name != null)
                {
                    if (!visitedRooms.ContainsKey(currentRoom.Name))
                    {
                        visitedRooms[currentRoom.Name] = new Tuple<Vector3, int>(currentRoom.Position, currentRoom.Floor);
                    }
                }

                if (!Team.Members.Any(m => m.Character == null))
                {
                    POSMsg = false;

                    if (InLineOfSightMob != null)
                    {
                        HandleMobs(InLineOfSightMob);
                    }
                    else if (LeBuddy._settings["Looting"].AsBool() && _corpse != null)
                    {
                        HandleCorpses(_corpse);
                    }
                    else if (Extensions.CanProceed())
                    {
                        if (OutOfSightMob != null)
                        {
                            HandleMobs(OutOfSightMob);
                        }
                        else if (LeBuddy._mobLocations.Count >= 1)
                        {
                            HandleMobDictionary();
                        }
                        else if (_upButton != null)
                        {
                            HandleButtons(_upButton);
                        }
                        else if (LeBuddy._upButtonLocations.ContainsValue(LeBuddy.level))
                        {
                            HandleNullButtons();
                        }
                        else
                        {
                            ProcessDoors();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + LeBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != LeBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    LeBuddy.previousErrorMessage = errorMessage;
                }
            }
        }

        public static void HandleMobs(SimpleChar target)
        {
            if (target != null)
            {
                if (!(target.IsInAttackRange(true) && target.IsInLineOfSight))
                {
                    if (!SMovementController.IsNavigating())
                    {
                        Chat.WriteLine($"Pathing to {target.Name}");
                        SMovementController.SetNavDestination(target.Position);
                    }
                }
                else
                {
                    if (SMovementController.IsNavigating())
                    {
                        SMovementController.Halt();
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.FightingTarget == null
                                         && !DynelManager.LocalPlayer.IsAttacking
                                                          && !DynelManager.LocalPlayer.IsAttackPending)
                        {
                            DynelManager.LocalPlayer.Attack(target);
                        }
                    }
                }
            }
        }
        public static void HandleCorpses(Corpse corpse)
        {
            if (corpse.Room.Floor == LeBuddy.level)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(corpse.Position) > 2)
                {
                    if (!SMovementController.IsNavigating())
                    {
                        SMovementController.SetNavDestination(corpse.Position);
                    }
                }
            }
        }

        private void HandleMobDictionary()
        {
            int playerFloor = DynelManager.LocalPlayer.Room.Floor;

            List<int> keysToRemove = new List<int>();

            foreach (var pair in LeBuddy._mobLocations)
            {
                var mobPosition = pair.Value.Item1;
                var mobFloor = pair.Value.Item2;

                if (mobFloor != playerFloor ||
                    (mobPosition.Distance2DFrom(DynelManager.LocalPlayer.Position) < 40f && OutOfSightMob == null))
                {
                    keysToRemove.Add(pair.Key);
                }
            }

            foreach (var key in keysToRemove)
            {
                LeBuddy._mobLocations.Remove(key);
            }

            if (LeBuddy._mobLocations.Count == 0)
            {
                if (SMovementController.IsNavigating())
                {
                    SMovementController.Halt();
                }
                else
                {
                    Chat.WriteLine("Return for _mobLocations.Count == 0");
                    return;
                }
            }

            foreach (var pair in LeBuddy._mobLocations)
            {
                var mobPosition = pair.Value.Item1;

                if (DynelManager.LocalPlayer.Position.DistanceFrom(mobPosition) > 5)
                {
                    if (!SMovementController.IsNavigating())
                    {
                        SMovementController.SetNavDestination(mobPosition);
                    }

                    break;
                }
            }
        }

        private void HandleButtons(Dynel button)
        {
            if (button.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 2f)
            {
                if (SMovementController.IsNavigating())
                {
                    SMovementController.Halt();
                }
                else
                {
                    UseButton(button);
                }
            }
            else
            {
                if (!SMovementController.IsNavigating())
                {
                    SMovementController.SetNavDestination(button.Position);
                }
            }
        }

        public static void UseButton(Dynel button)
        {
            var lvlCD = DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Level);

            if (!lvlCD)
            {
                if (Time.AONormalTime > _buttonTimer)
                {
                    _buttonTimer = Time.AONormalTime + 4;
                    button?.Use();
                }
            }
        }

        private void HandleNullButtons()
        {
            int playerFloor = DynelManager.LocalPlayer.Room.Floor;

            foreach (var buttonEntry in LeBuddy._upButtonLocations)
            {
                if (buttonEntry.Value == playerFloor)
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(buttonEntry.Key) > 2)
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            Chat.WriteLine("Pathing to saved button");
                            SMovementController.SetNavDestination(buttonEntry.Key);
                        }
                        break;
                    }
                }
            }
        }

        public void ProcessDoors()
        {
            var unvisitedRoomEntry = linkedRooms.FirstOrDefault(roomEntry =>
           !visitedRooms.ContainsKey(roomEntry.Key) && roomEntry.Value.Item2 == LeBuddy.level);

            var doorPosition = unvisitedRoomEntry.Value.Item1;

            if (DynelManager.LocalPlayer.Position.DistanceFrom(doorPosition) > 1)
            {
                if (!SMovementController.IsNavigating())
                {
                    Chat.WriteLine($"Lost, pathing to door at {doorPosition}.");
                    SMovementController.SetNavDestination(doorPosition);
                }
            }
            else
            {
                MovementController.Instance.SetMovement(MovementAction.ForwardStart);

                return;
            }
        }

        public static Dynel GetButton(string buttonName1, string buttonName2)
        {
            return DynelManager.AllDynels
                .Where(c => (c.Name == buttonName1 || c.Name == buttonName2) && c.Room.Floor == DynelManager.LocalPlayer.Room.Floor)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();
        }

        public static void LockedDoors()
        {
            var lockPick = Inventory.Items
            .Where(c => c.Name.Contains("Lock Pick"))
            .FirstOrDefault();

            var door = Playfield.Doors.Where(c => c.IsLocked
            && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 5)
            .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
            .FirstOrDefault();

            if (door != null && lockPick != null)
            {
                lockPick.UseOn(door.Identity);
            }
        }
    }
}
