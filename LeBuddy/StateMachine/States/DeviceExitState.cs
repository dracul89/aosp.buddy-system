﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Linq;

namespace LeBuddy
{
    public class DeviceExitState : IState
    {
        private Dynel _exitDevice;
        private Door _exitDoor;

        public IState GetNextState()
        {
            if (!LeBuddy._settings["Enable"].AsBool() || !Playfield.IsDungeon)
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            EnterState.NavGenSuccessful = false;

            Chat.WriteLine("Enter DeviceExitState");
        }

        public void OnStateExit()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }

            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack();
            }

            Chat.WriteLine("Exit DeviceExitState");
        }

        public void Tick()
        {
            try
            {
                _exitDevice = DynelManager.AllDynels.FirstOrDefault(c => c.Name == "Exit Device"
                && c.Room.Floor == DynelManager.LocalPlayer.Room.Floor);

                _exitDoor = Playfield.Doors.FirstOrDefault(d =>
                            (d.RoomLink1 != null && d.RoomLink2 == null) || (d.RoomLink1 == null && d.RoomLink2 != null));


                if (DynelManager.LocalPlayer.Room.Name == "Mothership_bossroom")
                {
                    if (_exitDevice != null)
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(_exitDevice.Position) < 5)
                        {
                            if (SMovementController.IsNavigating())
                            {
                                SMovementController.Halt();
                            }
                            else
                            {
                                if (Time.AONormalTime > PathState._buttonTimer + 3.0)
                                {
                                    PathState._buttonTimer = Time.AONormalTime;

                                    _exitDevice.Use();
                                }
                            }
                        }
                        else
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                SMovementController.SetNavDestination(_exitDevice.Position);
                            }
                        }
                    }
                }
                if (DynelManager.LocalPlayer.Room.Name == "Mothership_entrance")
                {
                    if (DynelManager.LocalPlayer.Identity == LeBuddy.Leader)
                    {
                        Team.Disband();
                    }

                    MoveToExit();
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + LeBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != LeBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    LeBuddy.previousErrorMessage = errorMessage;
                }
            }
        }

        private void MoveToExit()
        {
            if (_exitDoor != null)
            {
                float distanceFromExit = DynelManager.LocalPlayer.Position.DistanceFrom(_exitDoor.Position);

                if (!SMovementController.IsNavigating())
                {
                    if (distanceFromExit > 5)
                    {
                        MovementController.Instance.SetDestination(_exitDoor.Position);
                    }
                    else if (distanceFromExit < 2)
                    {
                        MovementController.Instance.SetMovement(MovementAction.ForwardStart);
                    }
                }
            }
        }
    }
}
