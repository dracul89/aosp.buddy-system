﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using LeBuddy.IPCMessages;
using System;
using System.Linq;

namespace LeBuddy
{
    public class BossRoomState : IState
    {
        //Bosses
        private SimpleChar _masterofBiologicalMetamorphoses;
        private SimpleChar _masterofTimeandSpace;
        private SimpleChar _masterofPsyMod;
        private SimpleChar _masterofSilence;
        private SimpleChar _coccoonAttendant;

        private SimpleChar _alienCoccoon;
        private SimpleChar _chaHeru;
        private SimpleChar _regenerationConduit;
        private SimpleChar _nanovoider;
        private SimpleChar _attackMobs;
        public static SimpleChar _allMobs;

        SimpleChar targetMob;

        private Dynel _exitDevice;

        private static Corpse _corpse;

        private bool teamNull;
        private bool coonAttacked;

        public IState GetNextState()
        {
            _nanovoider = DynelManager.NPCs.FirstOrDefault(c => c.Name == "Nanovoider");
            _exitDevice = DynelManager.AllDynels.FirstOrDefault(c => c.Name == "Exit Device");
            _allMobs = DynelManager.NPCs.Where(c => c.Health > 0 && !c.Name.Contains("Nanovoider") && c.IsInLineOfSight).FirstOrDefault();
            _corpse = DynelManager.Corpses.OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position)).FirstOrDefault();
            _alienCoccoon = DynelManager.NPCs.Where(c => c.Health > 0&& c.Name.Contains("Alien Coccoon")) .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position)).FirstOrDefault();
            
            if (!LeBuddy._settings["Enable"].AsBool() || !Playfield.IsDungeon
                || DynelManager.LocalPlayer.Room.Name != "Mothership_bossroom")
            {
                return new IdleState();
            }
            if (_alienCoccoon == null)
            {
                if (!LeBuddy.MissionExist() || !Team.IsInTeam)
                {
                    if (_masterofTimeandSpace != null)
                    {
                        return new ButtonExitState();
                    }

                    else if (_exitDevice != null && _allMobs == null && !coonAttacked)
                    {
                        if (_nanovoider != null)
                        {
                            if (LeBuddy._settings["Looting"].AsBool() && _corpse == null)
                            {
                                return new ButtonExitState();
                            }
                            if (!LeBuddy._settings["Looting"].AsBool())
                            {
                                return new ButtonExitState();
                            }
                        }
                        else
                        if (LeBuddy._settings["Looting"].AsBool() && _corpse == null)
                        {
                            return new DeviceExitState();
                        }
                        else if (!LeBuddy._settings["Looting"].AsBool())
                        {
                            return new DeviceExitState();
                        }
                    }
                }
            }
            return null;
        }

        public void OnStateEnter()
        {
            teamNull = true;
            coonAttacked = false;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }

            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack();
            }

            if (DynelManager.LocalPlayer.Identity == LeBuddy.Leader)
            {
                LeBuddy.IPCChannel.Broadcast(new POSMessage
                {
                    Position = DynelManager.LocalPlayer.Position,
                    Floor = DynelManager.LocalPlayer.Room.Floor,
                });
            }

            Chat.WriteLine("BossRoomState");
        }

        public void OnStateExit()
        {
            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack();
            }

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }

            Chat.WriteLine("Exit BossRoomState");
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning || !Team.IsInTeam) { return; }

                if (DynelManager.LocalPlayer.Identity == LeBuddy.Leader)
                {
                    if (!Team.Members.Any(m => m.Character == null))
                    {
                        teamNull = false;
                    }
                }

                _masterofBiologicalMetamorphoses = DynelManager.NPCs.Where(c => c.Health > 0
                && c.Name == "Master of Biological Metamorphoses")
                    .FirstOrDefault();

                _regenerationConduit = DynelManager.NPCs
                .Where(c => c.Health > 0 && c.Name.Contains("Regeneration Conduit"))
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();

                _masterofTimeandSpace = DynelManager.NPCs.Where(c => c.Health > 0
                && c.Name == "Master of Time and Space")
                    .FirstOrDefault();

                _masterofPsyMod = DynelManager.NPCs.Where(c => c.Health > 0
                && c.Name == "Master of PsyMod")
                    .FirstOrDefault();

                _masterofSilence = DynelManager.NPCs.Where(c => c.Health > 0
                && (c.Name == "Master of Silence" || c.Name == "Master of Nanovoid"))
                    .FirstOrDefault();

                _nanovoider = DynelManager.NPCs.FirstOrDefault(c => c.Name == "Nanovoider");

                _coccoonAttendant = DynelManager.NPCs.Where(c => c.Health > 0
                && c.Name.Contains("Coccoon Attendant - Cha'Heru"))
                    .FirstOrDefault();

                _alienCoccoon = DynelManager.NPCs.Where(c => c.Health > 0
                && c.Name.Contains("Alien Coccoon"))
                    .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                    .FirstOrDefault();

                _chaHeru = DynelManager.NPCs.Where(c => c.Health > 0
                && c.Name.Contains("Cha'Heru"))
                    .FirstOrDefault();

                _exitDevice = DynelManager.AllDynels.FirstOrDefault(c => c.Name == "Exit Device");

                _attackMobs = DynelManager.NPCs
                .Where(c => c.Health > 0
                            && !(c.Name.Contains("Master of") || c.Name.Contains("Defense System")
                            || c.Name.Contains("Regeneration Conduit") || c.Name.Contains("Nanovoider") || c.Name.Contains("Alien Coccoon"))
                            && c.IsInLineOfSight)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .ThenBy(c => c.HealthPercent)
                .FirstOrDefault();

                _allMobs = DynelManager.NPCs
                .Where(c => c.Health > 0
                            && !c.Name.Contains("Nanovoider")
                            && c.IsInLineOfSight
                            && c.Room.Floor == LeBuddy.level
                            && !c.IsPet)
                .FirstOrDefault();

                _corpse = DynelManager.Corpses.Where (c => c.Room.Floor == LeBuddy.level)
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();

                if (_corpse != null && _allMobs == null && LeBuddy._settings["Looting"].AsBool())
                {
                        PathState.HandleCorpses(_corpse);
                }

                if (DynelManager.LocalPlayer.Identity != LeBuddy.Leader)
                {
                    LeBuddy._leader = Team.Members
                        .Where(c => c.Character?.Health > 0 && c.Character?.IsValid == true && c.Identity == LeBuddy.Leader)
                        .FirstOrDefault()?.Character;

                    if (LeBuddy._leader != null)
                    {
                        LeBuddy._leaderPos = LeBuddy._leader.Position;

                        if (LeBuddy._leader?.FightingTarget != null)
                        {
                            targetMob = DynelManager.NPCs
                               .Where(c => c.Health > 0
                                   && c.Identity == (Identity)LeBuddy._leader?.FightingTarget?.Identity)
                               .FirstOrDefault();
                        }

                        if (LeBuddy._leader?.IsAttacking == true)
                        {
                            if (targetMob != null)
                            {
                                PathState.HandleMobs(targetMob);
                            }
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.Position.Distance2DFrom(LeBuddy._leaderPos) > 2)
                            {
                                if (!SMovementController.IsNavigating())
                                {
                                    SMovementController.SetNavDestination(LeBuddy._leaderPos);
                                }
                            }
                            else
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                            }
                        }
                    }
                }
                if (DynelManager.LocalPlayer.Identity == LeBuddy.Leader && !teamNull)
                {
                    switch (GetVoidSelection())
                    {
                        case VoidType.BiologicalMetamorphoses:
                            HandleBiologicalMetamorphosesVoid();
                            break;
                        case VoidType.TimeandSpace:
                            HandleTimeandSpaceVoid();
                            break;
                        case VoidType.PsyMod:
                            HandlePsyModVoid();
                            break;
                        case VoidType.Silence:
                            HandleSilenceVoid();
                            break;
                        case VoidType.Others:
                            HandleOtherVoid();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + LeBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != LeBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    LeBuddy.previousErrorMessage = errorMessage;
                }
            }
        }

        private enum VoidType
        {
            BiologicalMetamorphoses,
            TimeandSpace,
            PsyMod,
            Silence,
            Others
        }

        private VoidType GetVoidSelection()
        {
            if (_masterofBiologicalMetamorphoses != null)
            {
                return VoidType.BiologicalMetamorphoses;
            }

            if (_masterofTimeandSpace != null)
            {
                return VoidType.TimeandSpace;
            }

            if (_masterofPsyMod != null)
            {
                return VoidType.PsyMod;
            }

            if (_masterofSilence != null)
            {
                return VoidType.Silence;
            }

            return VoidType.Others;
        }

        public static void HandleBasicBoss(SimpleChar mob, SimpleChar anotherMob, SimpleChar boss)
        {
            if (mob != null)
            {
                PathState.HandleMobs(mob);
            }
            else if (anotherMob != null)
            {
                PathState.HandleMobs(anotherMob);
            }
            else
            {
                PathState.HandleMobs(boss);
            }
        }

        private void HandleBiologicalMetamorphosesVoid()
        {
            HandleBasicBoss(_attackMobs, _regenerationConduit, _masterofBiologicalMetamorphoses);

            //if (_attackMobs != null)
            //{
            //    PathState.HandleMobs(_attackMobs);
            //}
            //else if (_regenerationConduit != null)
            //{
            //    PathState.HandleMobs(_regenerationConduit);
            //}
            //else
            //{
            //    PathState.HandleMobs(_masterofBiologicalMetamorphoses);
            //}
        }

        private void HandleTimeandSpaceVoid()
        {
            if (Mission.List.Exists(x => x.DisplayName.Contains("Infiltrate the alien ships!")))
            {
                foreach (Mission mission in Mission.List)
                    if (mission.DisplayName.Contains("Infiltrate the alien ships!"))
                    {
                        mission.Delete();
                    }
            }
            else
            {
                LeBuddy._stateMachine.SetState(new ButtonExitState());
            }
        }

        private void HandlePsyModVoid()
        {
            HandleBasicBoss(null, _attackMobs, _masterofPsyMod);

            //if (_attackMobs != null)
            //{
            //    PathState.HandleMobs(_attackMobs);
            //}
            //else
            //{
            //    PathState.HandleMobs(_masterofPsyMod);
            //}
        }

        private void HandleSilenceVoid()
        {
            HandleBasicBoss(null, _attackMobs, _masterofSilence);

            //if (_attackMobs != null)
            //{
            //    PathState.HandleMobs(_attackMobs);
            //}
            //else
            //{
            //    PathState.HandleMobs(_masterofSilence);
            //}
        }

        private void HandleOtherVoid()
        {
            if (_coccoonAttendant != null || _alienCoccoon != null || _chaHeru != null)
            {
                if (_attackMobs != null)
                {
                    if (!(_attackMobs.IsInAttackRange(true) && _attackMobs.IsInLineOfSight))
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(_attackMobs.Position);
                        }
                    }
                    else
                    {
                        if (SMovementController.IsNavigating())
                        {
                            SMovementController.Halt();
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.FightingTarget == null
                                                && !DynelManager.LocalPlayer.IsAttacking
                                                && !DynelManager.LocalPlayer.IsAttackPending)
                            {
                                DynelManager.LocalPlayer.Attack(_attackMobs);
                                coonAttacked = false;
                            }
                        }
                    }
                }
                else if (_coccoonAttendant != null)
                {
                    if (!(_coccoonAttendant.IsInAttackRange(true) && _coccoonAttendant.IsInLineOfSight))
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(_coccoonAttendant.Position);
                        }
                    }
                    else
                    {
                        if (SMovementController.IsNavigating())
                        {
                            SMovementController.Halt();
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.FightingTarget == null
                                    && !DynelManager.LocalPlayer.IsAttacking
                                        && !DynelManager.LocalPlayer.IsAttackPending)
                            {
                                DynelManager.LocalPlayer.Attack(_coccoonAttendant);
                            }
                        }
                    }
                }
                else if (_alienCoccoon != null)
                {
                    if (!(_alienCoccoon.IsInAttackRange(true) && _alienCoccoon.IsInLineOfSight))
                    {
                        if (!coonAttacked)
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                SMovementController.SetNavDestination(_alienCoccoon.Position);
                            }
                        }
                    }
                    else
                    {
                        if (!coonAttacked)
                        {
                            if (SMovementController.IsNavigating())
                            {
                                SMovementController.Halt();
                            }
                            else
                            {
                                if (DynelManager.LocalPlayer.FightingTarget == null
                                        && !DynelManager.LocalPlayer.IsAttacking
                                            && !DynelManager.LocalPlayer.IsAttackPending)
                                {
                                    DynelManager.LocalPlayer.Attack(_alienCoccoon);
                                    coonAttacked = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
