﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Pathfinding;

namespace LeBuddy
{
    public class GrabMissionState : IState
    {
        private static bool _init = false;
        private double _chatDelay;
        static SimpleChar _recruiter;
        private bool isRecruiterWindowOpen;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (!LeBuddy._settings["Enable"].AsBool())
            {
                return new IdleState();
            }
            else
            {
                if (Mission.List.Exists(x => x.DisplayName.Contains("Infiltrate the alien ships!")))
                {
                    if (Playfield.ModelIdentity.Instance == Constants.UnicornOutpost)
                    {
                        if (DynelManager.LocalPlayer.Position.Distance2DFrom(Constants._reformArea) < 10)
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                return new IdleState();
                            }
                        }
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            _init = false;

            if (Playfield.ModelIdentity.Instance != SharpNavGenState.Instance)
            {
                SharpNavGenState.DeleteNavMesh();
            }

            NpcDialog.ChatWindowOpened += OnChatWindowOpened;
            NpcDialog.AnswerListChanged += NpcDialog_AnswerListChanged;

            Chat.WriteLine("GrabMissionState");
        }

        void OnChatWindowOpened(Identity identity)
        {
            if (identity == _recruiter.Identity)
            {
                isRecruiterWindowOpen = true;
            }
        }

        public void OnStateExit()
        {
            NpcDialog.AnswerListChanged -= NpcDialog_AnswerListChanged;

            NpcDialog.ChatWindowOpened -= OnChatWindowOpened;

            isRecruiterWindowOpen = false;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }

            Chat.WriteLine("Exit GrabMissionState");
        }

        public void Tick()
        {
            if (Game.IsZoning || !Team.IsInTeam || LeBuddy._settings["Stop"].AsBool()) { return; }

            _recruiter = DynelManager.NPCs
                .Where(c => c.Name == Constants.UnicornRecruiter)
                .FirstOrDefault();


            if (!Mission.List.Exists(x => x.DisplayName.Contains("Infiltrate the alien ships!")))
            {
                if (!Extensions.IsAtUnicornRecruiter())
                {
                    if (!SMovementController.IsNavigating())
                    {
                        SMovementController.SetNavDestination(Constants._unicornRecruiter);
                    }
                }
                else
                {
                    if (SMovementController.IsNavigating())
                    {
                        SMovementController.Halt();
                    }
                    else
                    {
                        if (_recruiter != null)
                        {
                            if (!isRecruiterWindowOpen)
                            {
                                NpcDialog.Open(_recruiter.Identity);
                            }
                        }
                    }
                }
            }
            else
            {
                if (DynelManager.LocalPlayer.Position.Distance2DFrom(Constants._reformArea) > 5
                    && !SMovementController.IsNavigating())
                {
                    SMovementController.SetNavDestination(Constants._reformArea);
                }
            }
        }

        private void NpcDialog_AnswerListChanged(object s, Dictionary<int, string> options)
        {
            SimpleChar dialogNpc = DynelManager.GetDynel((Identity)s).Cast<SimpleChar>();

            if (dialogNpc.Name != Constants.UnicornRecruiter) { return; }

            var difficulty = (int)(LeBuddy.DifficultySelection)LeBuddy._settings["DifficultySelection"].AsInt32();

            foreach (KeyValuePair<int, string> option in options)
            {
                if (option.Value == "I want to visit the alien mothership!"
                   || (difficulty == 0 && option.Value == "Take it easy on us.  We want to come back in one piece.")
                   || (difficulty == 1 && option.Value == "We can handle our business.")
                   || (difficulty == 2 && option.Value == "I want the mission Unicorns are too chicken to take."))
                {
                    NpcDialog.SelectAnswer(dialogNpc.Identity, option.Key);
                }
            }
        }
    }
}
