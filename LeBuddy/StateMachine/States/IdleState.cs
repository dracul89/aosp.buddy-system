﻿using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;
using AOSharp.Pathfinding;
using System;
using LeBuddy.IPCMessages;
using AOSharp.Common.GameData;
using AOSharp.Core.Inventory;

namespace LeBuddy
{
    public class IdleState : IState
    {
        public static Identity selectedMember = Identity.None;
        private static Random rand = new Random();

        public IState GetNextState()
        {
            if (LeBuddy._settings["Enable"].AsBool())
            {
                bool missionExists = Mission.List.Exists(x => x.DisplayName.Contains("Infiltrate the alien ships!"));

                if (Playfield.ModelIdentity.Instance == Constants.UnicornOutpost)
                {
                    if (DynelManager.LocalPlayer.Identity == LeBuddy.Leader)
                    {
                        if (!Team.IsInTeam)
                        {
                            return new ReformState();
                        }
                        else
                        {
                            if (Extensions.CanProceed())
                            {
                                if (!missionExists)
                                {
                                    DeleteKey();

                                    return new GrabMissionState();
                                }
                                else
                                {
                                    if (selectedMember == Identity.None)
                                    {
                                        var keys = LeBuddy.teamReadiness.Keys.ToList();
                                        var member = keys[rand.Next(keys.Count)];
                                        selectedMember = member;
                                        LeBuddy.IPCChannel.Broadcast(new SelectedMemberUpdateMessage()
                                        { SelectedMemberIdentity = selectedMember });
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!missionExists)
                        {
                            DeleteKey();
                        }
                    }

                    if (Team.IsInTeam)
                    {
                        if (selectedMember != Identity.None)
                        {
                            if (DynelManager.LocalPlayer.Identity == selectedMember)
                            {
                                Chat.WriteLine("Selected member");
                                return new EnterState();
                            }
                        }

                        if (LeBuddy._settings["Merge"].AsBool())
                        {
                            if (missionExists)
                            {
                                if (Team.Members.Any(m => m.Character == null))
                                {
                                    return new EnterState();
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (Playfield.IsDungeon)
                    {
                        if (Team.IsInTeam && LeBuddy.MissionExist())
                        {
                            if (DynelManager.LocalPlayer.Room.Name == "Mothership_bossroom")
                            {
                                return new BossRoomState();
                            }
                            else
                            {
                                if (DynelManager.LocalPlayer.Identity != LeBuddy.Leader)
                                {
                                    return new FollowerState();
                                }
                                else
                                {
                                    return new PathState();
                                }
                            }
                        }
                        else
                        {
                            return new ButtonExitState();
                        }
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Idle");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }

            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack();
            };
        }

        public void OnStateExit() { }

        public void Tick()
        {
            if (LeBuddy._settings["Enable"].AsBool())
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._reclaim) < 5)
                {
                    if (!SMovementController.IsNavigating())
                    {
                        SMovementController.SetNavDestination(Constants._reformArea);
                    }
                }
            }
        }

        static void DeleteKey()
        {
            foreach (Item key in Inventory.Items)
            {
                if (key.Name == "Mission key to Alien Mothership")
                {
                    key.Delete();
                }
            }
        }
    }
}
