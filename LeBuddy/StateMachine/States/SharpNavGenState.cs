﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using LeBuddy.IPCMessages;
using System.IO;
using AOSharp.Common.GameData;
using AOSharp.Pathfinding;
using SharpNav;
using System.Linq;

namespace LeBuddy
{
    public class SharpNavGenState : IState
    {
        private static string filePath => $"{LeBuddy.PluginDir}\\NavMeshes\\{Playfield.ModelIdentity.Instance}.nav";

        public static int Instance;

        private static bool NavmeshFileExists => File.Exists(filePath);

        public static bool Saved = false;
        public static bool Loaded = false;

        public IState GetNextState()
        {
            if (LeBuddy._settings["Enable"].AsBool())
            {
                if (Playfield.IsDungeon)
                {
                    if (Loaded && Saved)
                    {
                        if (Playfield.IsDungeon)
                        {
                            LeBuddy.IPCChannel.Broadcast(new EnterMessage());

                            if (!Team.Members.Any(c => c.Character == null))
                            {
                                if (DynelManager.LocalPlayer.Identity != LeBuddy.Leader)
                                {
                                    return new FollowerState();
                                }
                                else
                                {
                                    return new PathState();
                                }
                            }
                        }
                    }
                }
                else
                {
                    return new IdleState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("NavGenState");
            Chat.WriteLine($"{Playfield.ModelIdentity.Instance}");
            Loaded = false;
            Saved = false;

            NavGen();
        }

        public void OnStateExit()
        {
            Chat.WriteLine("Exit NavGenState");

            MovementController.Instance.Halt();
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }
        }

        public static void NavGen()
        {
            SNavMeshGenerator.GenerateAsync(NavSettings).ContinueWith(navMesh =>
            {
                if (navMesh.Result == null)
                {
                    //Chat.WriteLine("NavGen Failed");
                    LeBuddy.IPCChannel.Broadcast(new ClearSelectedMemberMessage());
                    IdleState.selectedMember = Identity.None;
                    MovementController.Instance.SetMovement(MovementAction.BackwardStart);

                }
                if (navMesh.Result != null)
                {
                    //Chat.WriteLine(filePath);
                    SNavMeshSerializer.SaveToFile(navMesh.Result, filePath);
                    Saved = true;
                }
                if (Saved)
                {
                    Load();
                }
            });
        }

        public static void Load()
        {
            if (SNavMeshSerializer.LoadFromFile(filePath, out NavMesh navMesh))
            {
                //Chat.WriteLine(filePath);
                SMovementController.LoadNavmesh(navMesh);

                Loaded = true;
            }
        }

        public static NavMeshGenerationSettings NavSettings
        {
            get
            {
                var settings = new NavMeshGenerationSettings();

                settings.CellSize = 0.26f; // Size of a cell in the XZ plane. Smaller values increase detail.
                settings.CellHeight = 0.1f; // Height of a cell. Smaller values increase vertical detail.
                settings.MaxClimb = 0.3f; // Maximum height an agent can climb. Affects obstacle navigation.
                settings.AgentHeight = 1.7f; // Height of the agent. Determines the clearance needed.
                settings.AgentRadius = 0.2f; // Radius of the agent. Affects how close the agent can get to walls or obstacles.
                settings.MinRegionSize = 8; // Minimum number of cells allowed to form an isolated area (region).
                settings.MergedRegionSize = 20; // Minimum number of cells required to merge small regions into larger ones.
                settings.MaxEdgeLength = 12; // Maximum allowed length for contour edges. Smaller values can improve mesh detail.
                settings.MaxEdgeError = 1.8f; // Maximum allowed deviation for contour simplification. Higher values can simplify geometry.
                settings.VertsPerPoly = 6; // Maximum number of vertices per polygon. Higher values can create more complex shapes.
                settings.SampleDistance = 4; // Sample distance for detail mesh. Lower values create more detailed mesh surfaces.
                settings.MaxSampleError = 1; // Maximum sample error for detail mesh. Controls the accuracy of the detail mesh.
                settings.Bounds = Rect.Default; // Area bounds for the navmesh. Defines the navigation area.
                settings.BuildBoundingVolumeTree = true; // Determines if a bounding volume tree is created for faster queries.
                settings.FilterLargestSection = false;

                //settings.Links = new List<OffMeshConnection>(); // Define connections for paths that go off the mesh (like jump links).

                return settings;
            }
        }

        public static void DeleteNavMesh()
        {
            string navMeshFolderPath = $"{LeBuddy.PluginDir}\\NavMeshes\\";

            DirectoryInfo di = new DirectoryInfo(navMeshFolderPath);

            foreach (FileInfo file in di.GetFiles())
            {
                if (file.Name == $"{Instance}.nav")
                {
                    file.Delete();
                }
            }
        }
    }
}
