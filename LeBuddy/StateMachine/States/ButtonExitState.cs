﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LeBuddy
{
    public class ButtonExitState : IState
    {
        private Dynel _downButton;
        private Door _exitDoor;

        public IState GetNextState()
        {
            if (!LeBuddy._settings["Enable"].AsBool() || !Playfield.IsDungeon
                || DynelManager.LocalPlayer.Room.Floor != LeBuddy.level)
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            EnterState.NavGenSuccessful = false;

            Chat.WriteLine("Enter ButtonExitState");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }

            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack();
            }

            Chat.WriteLine("Exit ButtonExitState");
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                PathState.LockedDoors();

                _downButton = PathState.GetButton("Button (down)", null);

                _exitDoor = Playfield.Doors.FirstOrDefault(d =>
                            (d.RoomLink1 != null && d.RoomLink2 == null) || (d.RoomLink1 == null && d.RoomLink2 != null) && d.Room.Floor == 0); ;

                if (DynelManager.LocalPlayer.Room.Floor == 0)
                {
                    var mothershipEntranceRoom = Playfield.Rooms.FirstOrDefault(room => room.Name.Contains("Mothership_entrance"));

                    var _exitDoor = Playfield.Doors.FirstOrDefault(d =>
                       (d.RoomLink1 != null && d.RoomLink2 == null) || (d.RoomLink1 == null && d.RoomLink2 != null));

                    if (DynelManager.LocalPlayer.Identity == LeBuddy.Leader)
                    {
                        Team.Disband();
                    }

                    if (_exitDoor != null && DynelManager.LocalPlayer.Room.Name == "Mothership_entrance")
                    {
                        if (LeBuddy._exitDoorLocation == Vector3.Zero)
                        {
                            LeBuddy._exitDoorLocation = _exitDoor.Position;
                        }
                    }

                    if (DynelManager.LocalPlayer.Room.Name == "Mothership_entrance")
                    {
                        MoveToExit();
                    }
                    else
                    {
                        if (mothershipEntranceRoom != null)
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                Chat.WriteLine($"Pathing to mothership entrance {mothershipEntranceRoom.Position}");
                                SMovementController.SetNavDestination(mothershipEntranceRoom.Position);
                            }
                        }
                    }
                }
                else
                {
                    if (_downButton != null)
                    {
                        if (_downButton.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 2)
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                SMovementController.SetNavDestination(_downButton.Position);
                            }
                        }
                        else
                        {
                            if (SMovementController.IsNavigating())
                            {
                                SMovementController.Halt();
                            }
                            else
                            {
                                PathState.UseButton(_downButton);
                            }
                        }
                    }
                    else if (LeBuddy._downButtonLocation.ContainsValue(LeBuddy.level))
                    {
                        foreach (var buttonEntry in LeBuddy._downButtonLocation)
                        {
                            if (buttonEntry.Value == LeBuddy.level)
                            {
                                if (DynelManager.LocalPlayer.Position.DistanceFrom(buttonEntry.Key) > 5)
                                {
                                    if (!SMovementController.IsNavigating())
                                    {
                                        Chat.WriteLine($"Pathing to saved down button at {buttonEntry.Key}");
                                        SMovementController.SetNavDestination(buttonEntry.Key);
                                    }

                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Identity != LeBuddy.Leader)
                        {
                            LeBuddy._leader = Team.Members.Where(c => c.Character?.Health > 0
                                    && c.Character?.IsValid == true && c.Identity == LeBuddy.Leader).FirstOrDefault()?.Character;

                            if (DynelManager.LocalPlayer.Position.DistanceFrom(LeBuddy._leader.Position) > 2.0f)
                            {
                                if (DynelManager.LocalPlayer.MovementState != MovementState.Sit)
                                {
                                    SMovementController.SetNavDestination(LeBuddy._leader.Position);
                                }
                            }
                            else
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + LeBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != LeBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    LeBuddy.previousErrorMessage = errorMessage;
                }
            }
        }

        private void MoveToExit()
        {
            if (_exitDoor != null)
            {
                float distanceFromExit = DynelManager.LocalPlayer.Position.DistanceFrom(_exitDoor.Position);

                if (distanceFromExit > 2)
                {
                    if (!SMovementController.IsNavigating())
                    {
                        MovementController.Instance.SetDestination(_exitDoor.Position);
                    }
                }
                else
                {
                    if (SMovementController.IsNavigating())
                    {
                        SMovementController.Halt();
                    }
                    else
                    {
                        MovementController.Instance.SetMovement(MovementAction.ForwardStart);
                    }
                }
            }
        }
    }
}
