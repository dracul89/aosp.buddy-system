﻿using AOSharp.Core;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.Messages;
using System.Linq;
using LeBuddy.IPCMessages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using AOSharp.Pathfinding;
using AOSharp.Common.GameData;

namespace LeBuddy
{
    public class EnterState : IState
    {
        public static bool NavGenSuccessful;

        public IState GetNextState()
        {
            if (!LeBuddy._settings["Enable"].AsBool())
            {
                return new IdleState();
            }

            if (Playfield.IsDungeon || Playfield.ModelIdentity.Instance != Constants.UnicornOutpost)
            {
                if (IdleState.selectedMember != Identity.None || LeBuddy._settings["Merge"].AsBool())
                {
                    if (DynelManager.LocalPlayer.Identity == IdleState.selectedMember || LeBuddy._settings["Merge"].AsBool())
                    {
                        if (!NavGenSuccessful)
                        {
                            return new SharpNavGenState();
                        }
                    }
                }
                if (Team.IsInTeam)
                {
                    if (!Team.Members.Any(c => c.Character == null))
                    {
                        if (DynelManager.LocalPlayer.Identity != LeBuddy.Leader)
                        {
                            return new FollowerState();
                        }
                        else
                        {
                            return new PathState();
                        }
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Entering");

            LeBuddy._downButtonLocation.Clear();
            LeBuddy._mobLocations.Clear();
            LeBuddy._upButtonLocations.Clear();
            LeBuddy._exitDoorLocation = Vector3.Zero;

            if (IdleState.selectedMember != Identity.None)
            {
                if (DynelManager.LocalPlayer.Identity == IdleState.selectedMember)
                {
                    Network.N3MessageReceived += PlayshiftingFailed;
                }
            }
        }

        public void OnStateExit()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
            if (IdleState.selectedMember != Identity.None)
            {
                if (DynelManager.LocalPlayer.Identity == IdleState.selectedMember)
                {
                    Network.N3MessageReceived -= PlayshiftingFailed;
                }
            }

            IdleState.selectedMember = Identity.None;
            LeBuddy.missionTimer = Time.AONormalTime;
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            Dynel entrance = DynelManager.AllDynels.Where(e => e.Name.Contains("Alien Mothership")).FirstOrDefault();

            if (Playfield.ModelIdentity.Instance == Constants.UnicornOutpost)
            {
                if (Mission.List.Exists(x => x.DisplayName.Contains("Infiltrate the alien ships!")) && !SMovementController.IsNavigating())
                {
                    if (entrance != null)
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(entrance.Position) > 1.0f)
                        {
                            SMovementController.SetNavDestination(entrance.Position);
                        }
                    }
                    else
                    {
                        SMovementController.SetNavDestination(Constants._entranceStart);
                    }
                }
            }
        }

        public static void PlayshiftingFailed(object s, N3Message msg)
        {
            if (msg is FormatFeedbackMessage formatFeedbackMessage)
            {
                if (formatFeedbackMessage.Message.Contains("51103"))
                {
                    LeBuddy.IPCChannel.Broadcast(new DeleteMissionMessage());

                    if (Mission.List.Exists(x => x.DisplayName.Contains("Infiltrate the alien ships!")))
                    {
                        foreach (Mission mission in Mission.List)
                            if (mission.DisplayName.Contains("Infiltrate the alien ships!"))
                            {
                                mission.Delete();
                            }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._reformArea) > 1.0f)
                        {
                            if (!SMovementController.IsNavigating()) 
                            {
                                SMovementController.SetNavDestination(Constants._reformArea); 
                            }
                        }
                    }
                }
            }
        }
    }
}