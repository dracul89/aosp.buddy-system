﻿using AOSharp.Core;
using AOSharp.Pathfinding;
using System.Linq;

namespace DB2Buddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            DB2Buddy._leader = Team.Members
                        .Where(c => c.Character?.Health > 0
                            && c.Character?.IsValid == true
                            && c.Identity == DB2Buddy.Leader)
                        .FirstOrDefault()?.Character;

            if (DB2Buddy._settings["Enable"].AsBool())
            {
                if (Playfield.ModelIdentity.Instance == Constants.PWId)
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._entrance) < 30f)
                    {
                        if (DynelManager.LocalPlayer.Identity == DB2Buddy.Leader)
                        {
                            if (!Team.IsInTeam)
                            {
                                return new ReformState();
                            }
                            else
                            {
                                if (Extensions.CanProceed())
                                {
                                    return new EnterState();
                                }
                            }
                        }
                        else
                        {
                            if (Team.IsInTeam)
                            {
                                if (DB2Buddy._leader == null)
                                {
                                    if (Extensions.CanProceed())
                                    {
                                        return new EnterState();
                                    }
                                }
                            }
                        }
                    }
                }

                if (Playfield.ModelIdentity.Instance == Constants.DB2Id)
                {
                    DB2Buddy.GetDynels();

                    if (Team.IsInTeam)
                    {
                        if (DB2Buddy._taggedNotum || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.MachineShockwave))
                        {
                            return new NotumState();
                        }

                        if (DB2Buddy._aune != null)
                        {
                            if (DB2Buddy._redTower != null || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                            {
                                return new FightTowerState();
                            }

                            if (DB2Buddy._blueTower != null || DB2Buddy._aune.Buffs.Contains(DB2Buddy.Nanos.StrengthOfTheAncients))
                            {
                                if (!DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                                {
                                    return new FightTowerState();
                                }
                            }
                        }

                        if (DynelManager.LocalPlayer.IsFalling && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.first) < 75)
                        {
                            return new FellState();
                        }

                        if ((DB2Buddy.AuneCorpse || DB2Buddy._exitBeacon != null)
                           && DB2Buddy._settings["Farming"].AsBool())
                        {
                            return new FarmingState();
                        }

                        if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._centerPosition) > 50f)
                        {
                            return new PathToBossState();
                        }

                        if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._centerPosition) < 50f)
                        {
                            return new FightState();
                        }
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            SimpleChar MaskedCommando = DynelManager.NPCs.Where(c => c.Name.Contains("Masked Commando")).FirstOrDefault();

            if (MaskedCommando != null)
            {
                if (MaskedCommando.IsAttacking)
                {
                    if (DynelManager.LocalPlayer.FightingTarget == null
                              && !DynelManager.LocalPlayer.IsAttackPending)
                    {
                        DynelManager.LocalPlayer.Attack(MaskedCommando);
                    }
                }
            }
        }

        public void OnStateExit()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }
    }
}
