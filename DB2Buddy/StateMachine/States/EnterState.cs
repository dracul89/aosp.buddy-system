﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Linq;

namespace DB2Buddy
{
    public class EnterState : IState
    {
        private static double _time;

        public IState GetNextState()
        {
            DB2Buddy._leader = Team.Members
                        .Where(c => c.Character?.Health > 0
                            && c.Character?.IsValid == true
                            && c.Identity == DB2Buddy.Leader)
                        .FirstOrDefault()?.Character;

            if (Playfield.ModelIdentity.Instance != Constants.DB2Id
                && !Extensions.CanProceed())
            {
                return new IdleState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.DB2Id && Team.IsInTeam)
            {
                DB2Buddy.GetDynels();

                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._atDoor) < 10)
                {
                    if (!SMovementController.IsNavigating())
                    {
                        SMovementController.SetNavDestination(Constants._warpPos);
                    }
                }

                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._warpPos) < 5f)
                {
                    if (!Team.Members.Any(c => c.Character == null))
                    {
                        return new PathToBossState();
                    }
                }

                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._centerPosition) < 30f)
                {
                    return new FightState();
                }

                if (DB2Buddy._taggedNotum || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.MachineShockwave))
                {
                    return new NotumState();
                }

                if (DB2Buddy._aune != null)
                {
                    if (DB2Buddy._redTower != null || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                    {
                        return new FightTowerState();
                    }

                    if (DB2Buddy._blueTower != null || DB2Buddy._aune.Buffs.Contains(DB2Buddy.Nanos.StrengthOfTheAncients))
                    {
                        if (!DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                        {
                            return new FightTowerState();
                        }
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Entering");
            _time = Time.AONormalTime;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            DB2Buddy.AuneCorpse = false;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            try
            {
                Dynel entrance = DynelManager.AllDynels.
                    Where(c => c.Name.Contains("Dust Brigade 1 Outpost")).FirstOrDefault();

                if (Game.IsZoning) { return; }

                if (Playfield.ModelIdentity.Instance == Constants.PWId)
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._entrance) < 5)
                    {
                        DynelManager.LocalPlayer.Position = Constants._centerofentrance;
                        MovementController.Instance.SetMovement(MovementAction.Update);
                    }

                    if (Time.AONormalTime > _time + 2f)
                    {
                        _time = Time.AONormalTime;

                        MovementController.Instance.SetDestination(Constants._entrance);
                        MovementController.Instance.AppendDestination(Constants._append);
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + DB2Buddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != DB2Buddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    DB2Buddy.previousErrorMessage = errorMessage;
                }
            }
        }
    }
}