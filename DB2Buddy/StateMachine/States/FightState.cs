using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Linq;
using System.Runtime.Remoting.Messaging;

namespace DB2Buddy
{
    public class FightState : IState
    {
        public IState GetNextState()
        {
            if (DB2Buddy._settings["Enable"].AsBool())
            {
                if (Playfield.ModelIdentity.Instance != Constants.DB2Id)
                {
                    return new IdleState();
                }
                else
                {
                    DB2Buddy.GetDynels();

                    if (DB2Buddy._taggedNotum || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.MachineShockwave))
                    {
                        return new NotumState();
                    }

                    if (DB2Buddy._aune != null && !DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.SeismicActivity))
                    {
                        if (DB2Buddy._redTowerBool || DB2Buddy._redTower != null || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                        {
                            if (DynelManager.LocalPlayer.IsAttacking)
                            {
                                DynelManager.LocalPlayer.StopAttack();
                            }
                            else { return new FightTowerState(); }
                        }

                        if (DB2Buddy._blueTowerBool || DB2Buddy._blueTower != null || DB2Buddy._aune.Buffs.Contains(DB2Buddy.Nanos.StrengthOfTheAncients))
                        {
                            if (!DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                            {
                                return new FightTowerState();
                            }
                        }
                    }

                    if (DynelManager.LocalPlayer.IsFalling && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.first) < 75)
                    {
                        return new FellState();
                    }

                    if ((DB2Buddy.AuneCorpse || DB2Buddy._exitBeacon != null) && DB2Buddy._settings["Farming"].AsBool())
                    {
                        return new FarmingState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine($"Smash Aune!!");

            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack();
            }

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                DB2Buddy.GetDynels();

                if (DB2Buddy._auneCorpse != null)
                {
                    DB2Buddy.AuneCorpse = true;
                }
                else
                {
                    if (Extensions.Debuffed() || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                    {
                        if (DynelManager.LocalPlayer.IsAttacking)
                        {
                            DynelManager.LocalPlayer.StopAttack();
                        }

                        if (SMovementController.IsNavigating())
                        {
                            SMovementController.Halt();
                        }
                    }
                    else
                    {
                        if (DB2Buddy._aune != null)
                        {
                            if (DB2Buddy._aune.IsInLineOfSight && DB2Buddy._aune.IsInAttackRange(true)
                                && DynelManager.LocalPlayer.Position.DistanceFrom(DB2Buddy._aune.Position) < 8)
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                                else
                                {
                                    if (DynelManager.LocalPlayer.FightingTarget == DB2Buddy._aune)
                                    {
                                        DynelManager.LocalPlayer.StopAttack();
                                    }

                                    if (DynelManager.LocalPlayer.FightingTarget == null
                                      && !DynelManager.LocalPlayer.IsAttackPending
                                      && !DB2Buddy._aune.Buffs.Contains(DB2Buddy.Nanos.StrengthOfTheAncients))
                                    {
                                        DynelManager.LocalPlayer.Attack(DB2Buddy._aune);
                                    }
                                }
                            }
                            else
                            {
                                if (!SMovementController.IsNavigating())
                                {
                                    SMovementController.SetNavDestination(DB2Buddy._aune.Position);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + DB2Buddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != DB2Buddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    DB2Buddy.previousErrorMessage = errorMessage;
                }
            }
        }
    }
}
