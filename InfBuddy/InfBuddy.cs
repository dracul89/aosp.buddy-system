﻿using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Core;
using AOSharp.Core.IPC;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using InfBuddy.IPCMessages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace InfBuddy
{
    public class InfBuddy : AOPluginEntry
    {
        private StateMachine _stateMachine;

        public static SMovementController SMovementController { get; set; }

        public static IPCChannel IPCChannel { get; set; }

        public static Config Config { get; private set; }

        public static bool Enable;

        public static bool Ready = true;
        public static double missionTimer;
        public static double missionTimeOut;

        private Dictionary<Identity, bool> teamReadiness = new Dictionary<Identity, bool>();
        private bool? lastSentIsReadyState;

        ModeSelection currentMode;
        FactionSelection currentFaction;
        DifficultySelection currentDifficulty;

        public static SimpleChar _leader;
        public static Identity Leader = Identity.None;

        public static bool DoubleReward;

        public static double _stateTimeOut;
        private static double _uiDelay;

        public static string previousErrorMessage = string.Empty;

        public static List<string> _namesToIgnore = new List<string>
        {
                    "One Who Obeys Precepts",
                    "Buckethead Technodealer",
                    "The Retainer Of Ergo",
                    "Guardian Spirit of Purification"
        };

        private static Window infoWindow;

        private static string PluginDir;

        public static Settings _settings;

        public static float distance = -1f;

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("InfBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{CommonParameters.BasePath}\\{CommonParameters.AppPath}\\InfBuddy\\{DynelManager.LocalPlayer.Name}\\Config.json");

                SMovementControllerSettings mSettings = new SMovementControllerSettings
                {
                    NavMeshSettings = new SNavMeshSettings { DrawNavMesh = false, DrawDistance = 30 },

                    PathSettings = new SPathSettings
                    {
                        DrawPath = true,
                        MinRotSpeed = 10,
                        MaxRotSpeed = 30,
                        UnstuckUpdate = 5000,
                        UnstuckThreshold = 2f,
                        RotUpdate = 10,
                        MovementUpdate = 200,
                        PathRadius = 0.29f,
                        Extents = new Vector3(3f, 3f, 3f)
                    }
                };

                SMovementController.Set(mSettings);

                SMovementController.AutoLoadNavmeshes($"{PluginDir}\\NavMeshes");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.StartStop, OnStartStopMessage);
                IPCChannel.RegisterCallback((short)IPCOpcode.ModeSelections, OnModeSelectionsMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.LeaderInfo, OnLeaderInfoMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.WaitAndReady, OnWaitAndReadyMessage);

                Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannelChangedEvent += IPCChannel_Changed;

                Chat.RegisterCommand("enable", BuddyCommand);

                SettingsController.RegisterSettingsWindow("InfBuddy", pluginDir + "\\UI\\InfBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                NpcDialog.AnswerListChanged += NpcDialog_AnswerListChanged;
                Game.OnUpdate += OnUpdate;
                Team.TeamRequest += OnTeamRequest;

                _settings.AddVariable("ModeSelection", (int)ModeSelection.Normal);
                _settings.AddVariable("FactionSelection", (int)FactionSelection.Clan);
                _settings.AddVariable("DifficultySelection", (int)DifficultySelection.Hard);

                _settings.AddVariable("Enable", false);
                _settings["Enable"] = false;

                _settings.AddVariable("Stop", false);
                _settings["Stop"] = false;

                _settings.AddVariable("DoubleReward", false);
                _settings.AddVariable("Merge", false);
                _settings.AddVariable("Looting", false);
                _settings.AddVariable("Leech", false);

                if (Game.IsNewEngine)
                {
                    Chat.WriteLine("Does not work on this engine!");
                }
                else
                {
                    Chat.WriteLine("InfBuddy Loaded!");
                    Chat.WriteLine("/buddy for settings. /enable to start and stop.");
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

        private void InfoView(object s, ButtonBase button)
        {
            infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\InfBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            infoWindow.Show(true);
        }
        private void Start()
        {
            Enable = true;

            Chat.WriteLine("InfBuddy enabled.");

            if (Playfield.ModelIdentity.Instance != Constants.Mission)
            {
                foreach (var mission in Mission.List.Where(mission => mission.DisplayName.Contains("The Purification Ritual")))
                {
                    Chat.WriteLine("Deleting mission");
                    mission.Delete();
                }
            }

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }
        }

        private void Stop()
        {
            Enable = false;

            Chat.WriteLine("InfBuddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        private void OnStartStopMessage(int sender, IPCMessage msg)
        {
            if (!(msg is StartStopIPCMessage startStopMessage)) return;
            if (startStopMessage.IsStarting)
            {
                _settings["Enable"] = true;
                Start();
            }
            else
            {
                _settings["Enable"] = false;
                Stop();
            }
        }

        private void OnModeSelectionsMessage(int sender, IPCMessage msg)
        {
            if (!(msg is ModeSelectionsIPCMessage modeSelectionsMessage)) return;
            currentMode = modeSelectionsMessage.Mode;
            currentFaction = modeSelectionsMessage.Faction;
            currentDifficulty = modeSelectionsMessage.Difficulty;

            _settings["ModeSelection"] = (int)currentMode;
            _settings["FactionSelection"] = (int)currentFaction;
            _settings["DifficultySelection"] = (int)currentDifficulty;
        }
        private void OnLeaderInfoMessage(int sender, IPCMessage msg)
        {
            if (!(msg is LeaderInfoIPCMessage leaderInfoMessage)) return;
            if (leaderInfoMessage.IsRequest)
            {
                if (DynelManager.LocalPlayer.Identity == Leader)
                {
                    IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = Leader, IsRequest = false });
                }
            }
            else
            {
                Leader = leaderInfoMessage.LeaderIdentity;
            }
        }
        private void OnWaitAndReadyMessage(int sender, IPCMessage msg)
        {
            if (!(msg is WaitAndReadyIPCMessage waitAndReadyMessage)) return;
            var senderIdentity = waitAndReadyMessage.PlayerIdentity;

            teamReadiness[senderIdentity] = waitAndReadyMessage.IsReady;

            if (Leader == DynelManager.LocalPlayer.Identity)
            {
                Ready = Team.Members.All(teamMember => !teamReadiness.ContainsKey(teamMember.Identity) || teamReadiness[teamMember.Identity]);
            }
        }

        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning) { return; }

            if (_settings["Enable"].AsBool())
            {
                _stateMachine.Tick();
            }

            var kitsInstance = new Shared.Kits();

            kitsInstance.SitAndUseKit(66, 66);

            if (Team.IsInTeam)
            {
                if (_settings["Merge"].AsBool())
                {
                    if (Leader == Identity.None)
                    {
                        var teamLeader = Team.Members.FirstOrDefault(member => member.IsLeader)?.Character;

                        Leader = teamLeader?.Identity ?? Identity.None;
                    }
                }
                else
                {
                    if (Leader == Identity.None)
                    {
                        if (Team.IsLeader)
                        {
                            Leader = DynelManager.LocalPlayer.Identity;

                            IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = Leader });
                        }
                        else
                        {
                            IPCChannel.Broadcast(new LeaderInfoIPCMessage() { IsRequest = true });
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            foreach (var member in Team.Members.Where(member => member.Identity != Leader).Where(member => !ReformState._teamCache.Contains(member.Identity)))
                        {
                            ReformState._teamCache.Add(member.Identity);
                        }
                    }
                    else
                    {
                        var localPlayer = DynelManager.LocalPlayer;
                        var currentIsReadyState = !Shared.Kits.InCombat() && !(Spell.HasPendingCast
                                                                               || localPlayer.NanoPercent < 60
                                                                               || localPlayer.HealthPercent < 60
                                                                               || !Spell.List.Any(spell => spell.IsReady));
                        
                        if (currentIsReadyState != lastSentIsReadyState)
                        {
                            var localPlayerIdentity = DynelManager.LocalPlayer.Identity;

                                IPCChannel.Broadcast(new WaitAndReadyIPCMessage
                                {
                                    IsReady = currentIsReadyState,
                                    PlayerIdentity = localPlayerIdentity
                                });

                                lastSentIsReadyState = currentIsReadyState;
                            }
                        }
                    }
                }
            }

            #region UI

            if (!(Time.NormalTime > _uiDelay + 1.0)) return;
            if (SettingsController.settingsWindow == null || !SettingsController.settingsWindow.IsValid) return;
            SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);

            if (channelInput != null)
            {
                if (int.TryParse(channelInput.Text, out int channelValue)
                    && Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel != channelValue)
                {
                    Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel = channelValue;
                }
            }

            if (SettingsController.settingsWindow.FindView("InfBuddyInfoView", out Button infoView))
            {
                infoView.Tag = SettingsController.settingsWindow;
                infoView.Clicked = InfoView;
            }

            if (!_settings["Enable"].AsBool() && Enable)
            {
                IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                Stop();
            }
            if (_settings["Enable"].AsBool() && !Enable)
            {
                if (!_settings["Merge"].AsBool())
                    Leader = DynelManager.LocalPlayer.Identity;

                IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                Start();
            }

            var newMode = (ModeSelection)_settings["ModeSelection"].AsInt32();
            var newFaction = (FactionSelection)_settings["FactionSelection"].AsInt32();
            var newDifficulty = (DifficultySelection)_settings["DifficultySelection"].AsInt32();

            var modeChanged = newMode != currentMode;
            var factionChanged = newFaction != currentFaction;
            var difficultyChanged = newDifficulty != currentDifficulty;

            if (modeChanged || factionChanged || difficultyChanged)
            {
                var modeSelectionsMessage = new ModeSelectionsIPCMessage
                {
                    Mode = newMode,
                    Faction = newFaction,
                    Difficulty = newDifficulty
                };

                IPCChannel.Broadcast(modeSelectionsMessage);

                if (modeChanged)
                {
                    currentMode = newMode;
                }

                if (factionChanged)
                {
                    currentFaction = newFaction;
                }

                if (difficultyChanged)
                {
                    currentDifficulty = newDifficulty;
                }
            }
            _uiDelay = Time.NormalTime;

            #endregion
        }

        private void NpcDialog_AnswerListChanged(object s, Dictionary<int, string> options)
        {
            SimpleChar dialogNpc = DynelManager.GetDynel((Identity)s).Cast<SimpleChar>();

            if (dialogNpc.Name == "The Retainer Of Ergo")
            {
                foreach (var option in options.Where(option => option.Value == "Is there anything I can help you with?" ||
                                                               (FactionSelection.Clan == (FactionSelection)_settings["FactionSelection"].AsInt32() && option.Value == "I will defend against the Unredeemed!") ||
                                                               (FactionSelection.Omni == (FactionSelection)_settings["FactionSelection"].AsInt32() && option.Value == "I will defend against the Redeemed!") ||
                                                               (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32() && option.Value == "I will defend against the creatures of the brink!") ||
                                                               (DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && option.Value == "I will deal with only the weakest aversaries") || //Brink missions have a typo
                                                               (DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && option.Value == "I will deal with only the weakest adversaries") ||
                                                               (DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && option.Value == "I will challenge these invaders, as long as there aren't too many") ||
                                                               (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && !_settings["DoubleReward"].AsBool() && option.Value == "I will purge the temple of any and all assailants") ||
                                                               (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && _settings["DoubleReward"].AsBool() && !DoubleReward && option.Value == "I will challenge these invaders, as long as there aren't too many") ||
                                                               (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && _settings["DoubleReward"].AsBool() && DoubleReward && option.Value == "I will purge the temple of any and all assailants")))
                {
                    NpcDialog.SelectAnswer(dialogNpc.Identity, option.Key);
                }
            }
            else if (dialogNpc.Name == "One Who Obeys Precepts")
            {
                foreach (var option in options.Where(option => option.Value == "Yes, I am ready."))
                {
                    NpcDialog.SelectAnswer(dialogNpc.Identity, option.Key);
                }
            }
        }

        public static bool MissionExist()
        {
            return !(Time.AONormalTime > missionTimer + 5) || Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri"));
        }

        public static bool IsInRange(SimpleChar target)
        {
            var playerPos = DynelManager.LocalPlayer.Position;
            var targetPos = target.Position;

            if (!target.IsInAttackRange(true)) return false;
            if (distance < 0)
            {
                distance = Vector3.Distance(playerPos, targetPos);
            }

            var attackRange = distance - 1f;

            return Vector3.Distance(playerPos, targetPos) <= attackRange;

        }

        private void OnTeamRequest(object sender, TeamRequestEventArgs e)
        {
            if (!_settings["Merge"].AsBool()) return;
            if (e.Requester == Leader)
            {
                e.Accept();
            }
        }

        private void BuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Enable"].AsBool())
                    {
                        _settings["Enable"] = true;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                        Start();
                    }
                    else
                    {
                        _settings["Enable"] = false;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                        Stop();
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public enum ModeSelection
        {
            Normal,
            Roam
        }
        public enum FactionSelection
        {
            Neutral,
            Clan,
            Omni
        }
        public enum DifficultySelection
        {
            Easy,
            Medium,
            Hard
        }
       
        public static int GetLineNumber(Exception ex)
        {
            var lineMatch = Regex.Match(ex.StackTrace ?? "", @":line (\d+)$", RegexOptions.Multiline);
            return lineMatch.Success ? int.Parse(lineMatch.Groups[1].Value) : 0;

        }
    }
}
