﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace InfBuddy
{
    public static class Extensions
    {
        public static void AddRandomness(this ref Vector3 pos, int entropy)
        {
            pos.X += Next(-entropy, entropy);
            pos.Z += Next(-entropy, entropy);
        }
        public static int Next(int min, int max)
        {
            if (min >= max)
            {
                throw new ArgumentException("Min value is greater or equals than Max value.");
            }

            byte[] intBytes = new byte[4];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(intBytes);
            }

            return min + Math.Abs(BitConverter.ToInt32(intBytes, 0)) % (max - min + 1);
        }

        public static bool HasDied()
        {
            return Playfield.ModelIdentity.Instance == Constants.OmniPandeGarden || Playfield.ModelIdentity.Instance == Constants.ClanPandeGarden;
        }
        public static bool TimedOut(double _time, float _timeOut)
        {
            return Team.IsInTeam && Time.NormalTime > _time + _timeOut;
        }

        public static bool IsClear()
        {
            return !DynelManager.NPCs.Any(c => c.FightingTarget?.Name != "Guardian Spirit of Purification");
        }

        public static bool IsNull(SimpleChar _target)
        {
            return _target == null
                || _target?.IsPet == true
                || _target?.IsValid == false
                || _target?.Health == 0
                || _target?.Name == "Guardian Spirit of Purification";
        }

        public static bool CanProceed()
        {
            return DynelManager.LocalPlayer.HealthPercent > 65
                && DynelManager.LocalPlayer.NanoPercent > 65
                && DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1
                && DynelManager.LocalPlayer.MovementState != MovementState.Sit
                && Spell.List.Any(c => c.IsReady)
                && !Spell.HasPendingCast;
        }

        public static bool Rooted()
        {
            return DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOERoot);
        }
    }
}
