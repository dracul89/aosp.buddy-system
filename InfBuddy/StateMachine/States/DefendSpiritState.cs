using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Pathfinding;

namespace InfBuddy
{
    public class DefendSpiritState : PositionHolder, IState
    {
        private static Corpse _corpse;

        public static Vector3 _corpsePos = Vector3.Zero;

        public static double IsMovingDelay;

        public DefendSpiritState() : base(Constants.DefendPos, 3f, 1)
        { }

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            _corpse = DynelManager.Corpses
                .Where(c => c.Name.Contains("Remains of "))
                .FirstOrDefault();

            var _target = DynelManager.NPCs
                  .Where(c => c.Health > 0 && !c.Name.Contains("Guardian Spirit of Purification")
                  && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                  .OrderBy(c => c.HealthPercent)
                  .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                  .FirstOrDefault();

            if (Extensions.HasDied())
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.Mission)
            {
                if (InfBuddy._settings["Looting"].AsBool() && _corpse != null
                   && Extensions.IsNull(_target))
                {
                    return new LootingState();
                }

                if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                {
                    return new ExitMissionState();
                }

                if (InfBuddy.ModeSelection.Roam == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                {
                    return new RoamState();
                }

                if (Time.AONormalTime > InfBuddy.missionTimeOut + 1200)
                {
                    foreach (Mission mission in Mission.List)
                    {
                        if (mission.DisplayName.Contains("The Purification Ritual"))
                        {
                            Chat.WriteLine("Mission timed out, deleting.");
                            mission.Delete();
                        }
                    }   
                }
            }

            if (Playfield.ModelIdentity.Instance != Constants.Mission)
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Defending");

            Vector3 randoPos = Constants.DefendPos;
            randoPos.AddRandomness((int)1.34f);

            if (!SMovementController.IsNavigating())
            {
                SMovementController.SetNavDestination(randoPos);
            }

            InfBuddy.missionTimeOut = Time.AONormalTime;
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
            {
                InfBuddy._leader = Team.Members
                        .Where(c => c.Character?.Health > 0
                            && c.Character?.IsValid == true
                            && c.Identity == InfBuddy.Leader)
                        .FirstOrDefault()?.Character;

                if (InfBuddy._leader != null)
                {
                    if (InfBuddy._leader?.FightingTarget != null || InfBuddy._leader?.IsAttacking == true)
                    {
                        SimpleChar targetMob = DynelManager.NPCs
                            .Where(c => c.Health > 0
                                && c.Identity == (Identity)InfBuddy._leader?.FightingTarget?.Identity)
                            .FirstOrDefault();

                        if (targetMob != null)
                        {
                            if (targetMob.IsInLineOfSight && targetMob.IsInAttackRange())
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                                else
                                {
                                    if (DynelManager.LocalPlayer.FightingTarget == null
                                         && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                                    {
                                        InfBuddy.missionTimeOut = Time.AONormalTime;

                                        DynelManager.LocalPlayer.Attack(targetMob);
                                    }
                                }
                            }
                            if (!targetMob.IsInLineOfSight || !targetMob.IsInAttackRange())
                            {
                                if (!SMovementController.IsNavigating())
                                {
                                    SMovementController.SetNavDestination(targetMob.Position);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPos) > 5)
                        {
                            SMovementController.SetNavDestination(Constants.DefendPos, true);
                        }
                        else
                        {
                            HoldPosition();
                        }
                    }
                }
                else
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPos) > 5)
                    {
                        SMovementController.SetNavDestination(Constants.DefendPos, true);
                    }
                    else
                    {
                        HoldPosition();
                    }
                }
            }
            else
            {
                SimpleChar mob = DynelManager.NPCs
                   .Where(c => c.Health > 0 && !c.Name.Contains("Guardian Spirit of Purification")
                   && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                   .OrderBy(c => c.HealthPercent)
                   .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                   .FirstOrDefault();

                if (mob != null)
                {

                    float distanceToTarget = mob.Position.DistanceFrom(DynelManager.LocalPlayer.Position);

                    if (mob.IsInAttackRange() && mob.IsInLineOfSight)
                    {
                        if (SMovementController.IsNavigating())
                        {
                            SMovementController.Halt();
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.FightingTarget == null
                               && !DynelManager.LocalPlayer.IsAttacking
                               && !DynelManager.LocalPlayer.IsAttackPending)
                            {
                                InfBuddy.missionTimeOut = Time.AONormalTime;
                                DynelManager.LocalPlayer.Attack(mob);
                            }
                        }
                    }

                    if (!mob.IsInAttackRange() && distanceToTarget < 40)
                    {
                        if (!mob.IsInAttackRange() || !mob.IsInLineOfSight)
                        {
                            SMovementController.SetNavDestination(mob.Position);
                        }
                    }

                    else if (distanceToTarget > 20 && mob.IsInLineOfSight)
                    {
                        Shared.TauntingTools.HandleTaunting(mob);
                    }
                }
                else
                {
                    if (DynelManager.LocalPlayer.Position.Distance2DFrom(Constants.DefendPos) > 5)
                    {

                        SMovementController.SetNavDestination(Constants.DefendPos, true);

                    }
                    else
                    {
                        HoldPosition();
                    }
                }
            }
        }

        private static bool IsMoving(SimpleChar target)
        {
            if (!target.IsMoving)
            {
                IsMovingDelay = Time.AONormalTime;
            }
            else
            {
                return true;
            }

            return !target.IsMoving && Time.AONormalTime > IsMovingDelay + 6;
        }

        public void OnStateExit()
        {

        }
    }

    public class PositionHolder
    {
        private readonly Vector3 _holdPos;
        private readonly float _HoldDist;
        private readonly int _entropy;

        public PositionHolder(Vector3 holdPos, float holdDist, int entropy)
        {
            _holdPos = holdPos;
            _HoldDist = holdDist;
            _entropy = entropy;
        }

        public void HoldPosition()
        {
            if (!SMovementController.IsNavigating() && !IsNearDefenseSpot())
            {
                Vector3 randomHoldPos = _holdPos;
                randomHoldPos.AddRandomness(_entropy);

                SMovementController.SetNavDestination(randomHoldPos);
            }
        }

        private bool IsNearDefenseSpot()
        {
            return DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPos) < _HoldDist;
        }
    }
}
