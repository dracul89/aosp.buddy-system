﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Collections.Generic;
using System.Linq;

namespace InfBuddy
{
    public class ReformState : IState
    {
        public static double reformDelay;
        private static double disbandDelay;
        public static bool TeamCheck;

        public static HashSet<Identity> _teamCache = new HashSet<Identity>();
        HashSet<Identity> _invitedList = new HashSet<Identity>();

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Team.IsInTeam && !TeamCheck)
            {
                if (Time.AONormalTime > reformDelay + 8)
                {
                    if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
                    {
                        if (_teamCache.Count == (Team.Members.Count - 1))
                        {
                            return new IdleState();
                        }
                    }
                    else
                    {
                        if (!Team.Members.Any(t => t.Character == null))
                        {
                            return new IdleState();
                        }
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            if (Game.IsZoning) { return; }

            TeamCheck = true;
            disbandDelay = 0;

            Chat.WriteLine("Reforming");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
            {
                _invitedList.Clear();
            }

            disbandDelay = 0;
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (!Team.IsInTeam)
            {
                reformDelay = Time.AONormalTime;

                TeamCheck = false;
            }

            if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
            {
                if (Team.IsInTeam && TeamCheck)
                {
                    if (!Team.Members.Any(t => t.Character == null))
                    {
                        if (disbandDelay == 0)
                        {
                            disbandDelay = Time.AONormalTime;
                        }

                        if (disbandDelay > 0.0 && Time.AONormalTime > disbandDelay + 6)
                        {
                            Team.Disband();
                        }
                    }
                }
                if (!Team.IsInTeam)
                {
                    InvitePlayers();
                }
            }
        }

        private void InvitePlayers()
        {
            if (Time.AONormalTime > disbandDelay + 10)
            {
                foreach (SimpleChar player in DynelManager.Players.Where(c => _teamCache.Contains(c.Identity)
                            && !_invitedList.Contains(c.Identity)))
                {
                    _invitedList.Add(player.Identity);
                    Team.Invite(player.Identity);
                }
            }
        }
    }
}


