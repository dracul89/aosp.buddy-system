﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System;
using System.Linq;
using AOSharp.Pathfinding;

namespace InfBuddy
{
    public class LootingState : IState
    {
        private Corpse _corpse;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.Mission)
            {
                SimpleChar mob = DynelManager.NPCs
                   .Where(c => c.Health > 0 && !c.Name.Contains("Guardian Spirit of Purification")
                   && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                   .OrderBy(c => c.HealthPercent)
                   .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                   .FirstOrDefault();

                _corpse = DynelManager.Corpses
                    .Where(c => c.Name.Contains("Remains of "))
                    .FirstOrDefault();

                if (_corpse == null || !Extensions.IsNull(mob))
                {
                    if (InfBuddy.ModeSelection.Normal == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                    {
                        return new DefendSpiritState();
                    }
                    else
                    {
                        return new RoamState();
                    }
                }
            }

            if (Playfield.ModelIdentity.Instance != Constants.Mission)
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Moving to corpse");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                _corpse = DynelManager.Corpses
                    .Where(c => c.Name.Contains("Remains of "))
                    .FirstOrDefault();

                if (Game.IsZoning || _corpse == null) { return; }

                if (_corpse != null)
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(_corpse.Position) > 5f)
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(_corpse.Position);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + InfBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != InfBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    InfBuddy.previousErrorMessage = errorMessage;
                }
            }
        }
    }
}