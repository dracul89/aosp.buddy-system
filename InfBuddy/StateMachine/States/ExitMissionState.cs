﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Linq;

namespace InfBuddy
{
    public class ExitMissionState : IState
    {
        private double delayForCorpse;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            SimpleChar _target = DynelManager.NPCs
                 .Where(c => c.Health > 0 && !c.Name.Contains("Guardian Spirit of Purification")
                 && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                 .OrderBy(c => c.HealthPercent)
                 .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                 .FirstOrDefault();

            Corpse _corpse = DynelManager.Corpses
                .Where(c => c.Name.Contains("Remains of "))
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();

            if (Extensions.HasDied())
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.Mission)
            {
                if (InfBuddy._settings["Looting"].AsBool() && _corpse != null
                   && Extensions.IsNull(_target))
                {
                    return new LootingState();
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.Inferno)
            {
                if (SMovementController.IsNavigating())
                {
                    SMovementController.Halt();
                }
                else
                {
                    if (InfBuddy._settings["DoubleReward"].AsBool())
                    {
                        if (!InfBuddy.DoubleReward)
                        {
                            InfBuddy.DoubleReward = true;

                            return new GrabMissionState();
                        }
                        else
                        {
                            InfBuddy.DoubleReward = false;

                            return new ReformState();
                        }
                    }
                    else
                    {
                        return new ReformState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Exit Mission");

            if (InfBuddy._settings["Leech"].AsBool())
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.LeechMissionExit) > 2)
                {
                    DynelManager.LocalPlayer.Position = Constants.LeechMissionExit;
                }
            }

            delayForCorpse = Time.AONormalTime;
        }

        public void OnStateExit()
        {
            Chat.WriteLine("Exit Mission");
        }

        public void Tick()
        {
            if (Game.IsZoning || !Team.IsInTeam) { return; }

            if (InfBuddy._settings["DoubleReward"].AsBool() && !InfBuddy.DoubleReward)
            {
                if (!SMovementController.IsNavigating())
                {
                    SMovementController.SetNavDestination(Constants.MissionExitToInferno);
                }
            }

            if (!InfBuddy._settings["Stop"].AsBool())
            {
                if (Time.AONormalTime > delayForCorpse + 30)
                {
                    if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(Constants.MissionExitToInferno);
                        }
                    }
                    else
                    {
                        InfBuddy._leader = Team.Members
                            .Where(c => c.Character?.Health > 0
                                && c.Character?.IsValid == true
                                && c.Identity == InfBuddy.Leader)
                            .FirstOrDefault()?.Character;

                        if (InfBuddy._leader != null)
                        {
                            if (DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)InfBuddy._leader?.Position) > 2f)
                            {
                                SMovementController.SetNavDestination((Vector3)InfBuddy._leader?.Position);
                            }
                            else
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                            }
                        }
                        else
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                SMovementController.SetNavDestination(Constants.MissionExitToInferno);
                            }
                        }
                    }
                }
            }
        }
    }
}
