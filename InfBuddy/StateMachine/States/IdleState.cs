﻿using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Linq;

namespace InfBuddy
{
    public class IdleState : IState
    {
        private static Corpse _corpse;

        public static bool _init = false;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            _corpse = DynelManager.Corpses
                .Where(c => c.Name.Contains("Remains of "))
                .FirstOrDefault();

            if (!InfBuddy.Enable)
            {
                return null;
            }

            if (Extensions.HasDied())
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.Inferno)
            {
                if (!Team.IsInTeam)
                {
                    return new ReformState();
                }
                else
                {
                    if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                    {
                        return new GrabMissionState();
                    }
                    else
                    {
                        return new EnterMissionState();
                    }
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.Mission)
            {
                if (InfBuddy.MissionExist())
                {
                    if (InfBuddy._settings["Leech"].AsBool())
                    {
                        return new LeechState();
                    }
                    else
                    {
                        if (DynelManager.NPCs.Any(c => c.Name == "One Who Obeys Precepts"))
                        {
                            if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
                            {
                                return new StartMissionState();
                            }
                        }
                        else
                        {
                            if (InfBuddy.ModeSelection.Normal == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                            {
                                return new DefendSpiritState();
                            }
                            else
                            {
                                return new RoamState();
                            }
                        }
                    }
                }

                if (Extensions.IsClear() || !InfBuddy.MissionExist())
                {
                    if (InfBuddy._settings["Looting"].AsBool() && _corpse != null)
                    {
                        return new LootingState();
                    }
                    else
                    {
                        return new ExitMissionState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy.missionTimer = Time.AONormalTime;

            Chat.WriteLine("Idle");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
        }
    }
}
