﻿using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;
using AOSharp.Pathfinding;
using System.Security.Policy;

namespace InfBuddy
{
    public class StartMissionState : IState
    {
        private static bool _init = false;
        private double _timeToOpenDialog = 0;

        public IState GetNextState()
        {
            Corpse _corpse = DynelManager.Corpses
                 .Where(c => c.Name.Contains("Remains of "))
                 .FirstOrDefault();

            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied())
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == (Constants.Mission))
            {
                if (!InfBuddy.MissionExist())
                {
                    if (InfBuddy._settings["Looting"].AsBool() && _corpse != null)
                    {
                        return new LootingState();
                    }
                    else
                    {
                        return new ExitMissionState();
                    }
                }
                else
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.OneWhoObeysPrecepts) < 8f)
                    {
                        if (!DynelManager.NPCs.Any(c => c.Name == "One Who Obeys Precepts"))
                        {
                            if (InfBuddy.ModeSelection.Normal == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                            {
                                return new DefendSpiritState();
                            }
                            else
                            {
                                return new RoamState();
                            }
                        }
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy.missionTimer = Time.AONormalTime;
            InfBuddy._stateTimeOut = Time.NormalTime;
            _init = false;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }

            Chat.WriteLine("Starting Mission");
        }

        public void OnStateExit()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            Dynel OneWhoObeysPrecepts = DynelManager.NPCs
                .Where(c => c.Name == "One Who Obeys Precepts")
                .FirstOrDefault();


            if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.OneWhoObeysPrecepts) > 4f)
            {
                if (!SMovementController.IsNavigating())
                {
                    SMovementController.SetNavDestination(Constants.OneWhoObeysPrecepts);
                }
            }
            else
            {
                if (SMovementController.IsNavigating())
                {
                    SMovementController.Halt();
                }
                else
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.OneWhoObeysPrecepts) < 8f)
                    {
                        if (OneWhoObeysPrecepts != null)
                        {
                            if (!_init)
                            {
                                _init = true;
                                _timeToOpenDialog = Time.NormalTime + 1;
                            }

                            if (_timeToOpenDialog > 0 && Time.NormalTime >= _timeToOpenDialog)
                            {
                                NpcDialog.Open(OneWhoObeysPrecepts);
                                _timeToOpenDialog = 0;
                                _init = false;
                            }
                        }
                    } 
                }
            }
        }
    }
}