﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Linq;

namespace InfBuddy
{
    public class EnterMissionState : IState
    {

        private bool InTeam;
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied())
            {
                return new DiedState();
            }

            var startMobs = DynelManager.NPCs
                    .Where(c => c.Health > 0 && !(c.Name.Contains("One Who Obeys Precepts") || c.Name.Contains("Guardian Spirit of Purification"))
                    && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                    .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                    .ThenBy(c => c.HealthPercent)
                    .FirstOrDefault();


            if (Playfield.ModelIdentity.Instance == Constants.Inferno)
            {
                if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                {
                    return new GrabMissionState();
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.Mission)
            {
                if (InfBuddy._settings["Leech"].AsBool())
                {
                    return new LeechState();
                }
                else
                {
                    if (startMobs == null)
                    {
                        var leader = DynelManager.Players.Where(l => l.Identity == InfBuddy.Leader).FirstOrDefault();

                        if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
                        {
                            if (leader == null)
                            {
                                if (InfBuddy.ModeSelection.Normal == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                                {
                                    return new DefendSpiritState();
                                }
                                else
                                {
                                    return new RoamState();
                                }
                            }
                        }
                        else
                        {
                            if (InTeam)
                            {
                                return new StartMissionState();
                            }
                        }
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Entering");

            InTeam = false;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            if (Game.IsZoning || !Team.IsInTeam) { return; }

            if (Playfield.ModelIdentity.Instance == Constants.Inferno)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.EntrancetoXantemple) > 1)
                {
                    if (!SMovementController.IsNavigating())
                    {
                        SMovementController.SetNavDestination(Constants.EntrancetoXantemple);
                    }
                }
                else
                {
                    MovementController.Instance.SetMovement(MovementAction.ForwardStart);
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.Mission)
            {
                if (MovementController.Instance.IsNavigating)
                {
                    MovementController.Instance.Halt();
                }
                else
                {
                    if (!Team.Members.Any(t => t.Character == null))
                    {
                        InTeam = true;
                    }
                    if (InTeam)
                    {
                        KillEmALL();
                    }
                }
            }
        }

        public void OnStateExit()
        {
        }

        private static void KillEmALL()
        {
            if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
            {
                InfBuddy._leader = Team.Members
                        .Where(c => c.Character?.Health > 0
                            && c.Character?.IsValid == true
                            && c.Identity == InfBuddy.Leader)
                        .FirstOrDefault()?.Character;

                if (InfBuddy._leader != null)
                {
                    if (InfBuddy._leader?.FightingTarget != null || InfBuddy._leader?.IsAttacking == true)
                    {
                        var targetMob = DynelManager.NPCs
                            .Where(c => c.Health > 0
                                && c.Identity == (Identity)InfBuddy._leader?.FightingTarget?.Identity)
                            .FirstOrDefault();

                        if (targetMob != null)
                        {
                            if (targetMob.IsInLineOfSight && targetMob.IsInAttackRange())
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                                else
                                {
                                    if (DynelManager.LocalPlayer.FightingTarget == null
                                         && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                                    {
                                        DynelManager.LocalPlayer.Attack(targetMob);
                                    }
                                }
                            }
                            if (!targetMob.IsInLineOfSight || !targetMob.IsInAttackRange())
                            {
                                if (!SMovementController.IsNavigating())
                                {
                                    SMovementController.SetNavDestination(targetMob.Position);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)InfBuddy._leader?.Position) > 2f)
                        {
                            SMovementController.SetNavDestination((Vector3)InfBuddy._leader?.Position);
                        }
                        else
                        {
                            if (SMovementController.IsNavigating())
                            {
                                SMovementController.Halt();
                            }
                        }
                    }
                }
                else { return; }
            }
            else
            {
                var mob = DynelManager.NPCs
                    .Where(c => c.Health > 0 && !c.Name.Contains("One Who Obeys Precepts")
                    && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                    .OrderBy(c => c.HealthPercent)
                    .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                    .FirstOrDefault();

                if (mob != null)
                {
                    if (mob.IsInAttackRange() && mob.IsInLineOfSight)
                    {
                        if (SMovementController.IsNavigating())
                        {
                            SMovementController.Halt();
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.FightingTarget == null
                               && !DynelManager.LocalPlayer.IsAttacking
                               && !DynelManager.LocalPlayer.IsAttackPending)
                            {
                                DynelManager.LocalPlayer.Attack(mob);
                            }
                        }
                    }

                    if (!mob.IsInAttackRange() || !mob.IsInLineOfSight)
                    {
                        if (DynelManager.LocalPlayer.IsAttacking)
                        {
                            DynelManager.LocalPlayer.StopAttack();
                        }
                        else
                        {
                            SMovementController.SetNavDestination(mob.Position);
                        }
                    }
                }
                else
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.OneWhoObeysPrecepts) > 5)
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(Constants.OneWhoObeysPrecepts);
                        }
                    }
                }
            }
        }
    }
}
