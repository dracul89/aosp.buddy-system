﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;

namespace InfBuddy
{
    public class LeechState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied())
            {
                return new DiedState();
            }
                
            if (Playfield.ModelIdentity.Instance == Constants.Mission)
            {
                if (!InfBuddy.MissionExist())
                {
                    return new ExitMissionState();
                }

                if (!InfBuddy._settings["Leech"].AsBool())
                {
                    return new IdleState();
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.Inferno)
            {
                return new IdleState();
            }
            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy.missionTimer = Time.AONormalTime;

            Chat.WriteLine("Leecher");

            DynelManager.LocalPlayer.Position = Constants.LeechSpot;
            MovementController.Instance.SetMovement(MovementAction.Update);
            MovementController.Instance.SetMovement(MovementAction.JumpStart);
            MovementController.Instance.SetMovement(MovementAction.Update);
        }

        public void OnStateExit()
        {
            DynelManager.LocalPlayer.Position = new Vector3(160.4f, 2.6f, 103.0f);
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

        }
    }
}
