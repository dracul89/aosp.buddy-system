﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;
using AOSharp.Pathfinding;

namespace InfBuddy
{
    public class RoamState : IState
    {
        private static Corpse _corpse;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            var _target = DynelManager.NPCs
                 .Where(c => c.Health > 0 && !c.Name.Contains("Guardian Spirit of Purification")
                 && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                 .OrderBy(c => c.HealthPercent)
                 .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                 .FirstOrDefault();

            _corpse = DynelManager.Corpses
                .Where(c => c.Name.Contains("Remains of "))
                .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                .FirstOrDefault();

            if (Extensions.HasDied())
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.Mission)
            {
                if (InfBuddy._settings["Looting"].AsBool() && _corpse != null
                   && Extensions.IsNull(_target))
                {
                    return new LootingState();
                }

                if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                {
                    return new ExitMissionState();
                }

                if (InfBuddy.ModeSelection.Normal == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                {
                    return new DefendSpiritState();
                }

                if (Time.AONormalTime > InfBuddy.missionTimeOut + 600)
                {
                    foreach (Mission mission in Mission.List)
                    {
                        if (mission.DisplayName.Contains("The Purification Ritual"))
                        {
                            Chat.WriteLine("Mission timed out, deleting.");
                            mission.Delete();
                        }
                    }
                }
            }

            if (Playfield.ModelIdentity.Instance != Constants.Mission)
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }

            InfBuddy.missionTimeOut = Time.AONormalTime;

            Chat.WriteLine("Roaming");
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
            {
                InfBuddy._leader = Team.Members
                        .Where(c => c.Character?.Health > 0
                            && c.Character?.IsValid == true
                            && c.Identity == InfBuddy.Leader)
                        .FirstOrDefault()?.Character;

                if (InfBuddy._leader != null)
                {
                    if (InfBuddy._leader?.FightingTarget != null || InfBuddy._leader?.IsAttacking == true)
                    {
                        SimpleChar targetMob = DynelManager.NPCs
                            .Where(c => c.Health > 0
                                && c.Identity == (Identity)InfBuddy._leader?.FightingTarget?.Identity)
                            .FirstOrDefault();

                        if (targetMob != null)
                        {
                            if (targetMob.IsInLineOfSight && targetMob.IsInAttackRange())
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                                else
                                {
                                    if (DynelManager.LocalPlayer.FightingTarget == null
                                         && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                                    {
                                        InfBuddy.missionTimeOut = Time.AONormalTime;
                                        DynelManager.LocalPlayer.Attack(targetMob);
                                    }
                                }
                            }
                            if (!targetMob.IsInLineOfSight || !targetMob.IsInAttackRange())
                            {
                                if (!SMovementController.IsNavigating())
                                {
                                    SMovementController.SetNavDestination(targetMob.Position);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)InfBuddy._leader?.Position) > 2f)
                        {
                            SMovementController.SetNavDestination((Vector3)InfBuddy._leader?.Position);
                        }
                        else
                        {
                            if (SMovementController.IsNavigating())
                            {
                                SMovementController.Halt();
                            }
                        }
                    }
                }
                else
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.RoamPos) > 5)
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(Constants.RoamPos);
                        }
                    }
                }
            }
            else
            {
                SimpleChar mob = DynelManager.NPCs
                    .Where(c => c.Health > 0 && !c.Name.Contains("Guardian Spirit of Purification")
                    && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                    .OrderBy(c => c.HealthPercent)
                    .ThenBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                    .FirstOrDefault();

                if (mob != null)
                {
                    if (mob.IsInAttackRange() && mob.IsInLineOfSight)
                    {
                        if (SMovementController.IsNavigating())
                        {
                            SMovementController.Halt();
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.FightingTarget == null
                               && !DynelManager.LocalPlayer.IsAttacking
                               && !DynelManager.LocalPlayer.IsAttackPending)
                            {
                                InfBuddy.missionTimeOut = Time.AONormalTime;
                                DynelManager.LocalPlayer.Attack(mob);
                            }
                        }
                    }

                    if (!mob.IsInAttackRange() || !mob.IsInLineOfSight)
                    {
                        SMovementController.SetNavDestination(mob.Position);
                    }
                }
                else
                {
                    if (mob != null)
                    {
                        if (SMovementController.IsNavigating())
                        {
                            SMovementController.Halt();
                        }
                    }
                    else if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.RoamPos) > 5)
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(Constants.RoamPos);
                        }
                    }
                }
            }
        }
        public void OnStateExit()
        {
        }
    }
}
