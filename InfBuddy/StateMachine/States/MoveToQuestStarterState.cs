﻿using AOSharp.Core;
using AOSharp.Pathfinding;

namespace InfBuddy
{
    public class MoveToQuestStarterState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Extensions.HasDied())
            {
                return new DiedState();
            }

            if (!SMovementController.IsNavigating() && Extensions.IsAtStarterPos())
            {
                return new StartMissionState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy._stateTimeOut = Time.NormalTime;

            if (!SMovementController.IsNavigating() && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.QuestStarterBeforePos) > 4f)
            {
                SMovementController.SetDestination(Constants.QuestStarterBeforePos);
                SMovementController.AppendDestination(Constants.QuestStarterPos);
            }
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
        }
    }
}
