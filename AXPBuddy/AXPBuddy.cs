﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Core.IPC;
using AOSharp.Pathfinding;
using AXPBuddy.IPCMessages;
using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using System.Text.RegularExpressions;
using AOSharp.Core.Inventory;

namespace AXPBuddy
{
    public class AXPBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static Identity Leader = Identity.None;
        public static string LeaderName = string.Empty;
        public static SimpleChar _leader;
        public static Vector3 _leaderPos = Vector3.Zero;
        public static Vector3 _ourPos = Vector3.Zero;

        public static bool IsNavigating;

        public static float Tick = 0;

        public static bool _initMerge = false;

        public static bool Enable;

        public static bool Ready = true;

        private Dictionary<Identity, bool> teamReadiness = new Dictionary<Identity, bool>();

        private bool? lastSentIsReadyState;

        public static double _stateTimeOut;

        public static double _mainUpdate;

        public static double Timeout;

        public static double _lastZonedTime = Time.NormalTime;

        public static Vector3 _pos = Vector3.Zero;

        public static Window _infoWindow;

        public static Settings _settings;

        public static string PluginDir;

        public static string previousErrorMessage = string.Empty;

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("AXPBuddy");

                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{CommonParameters.BasePath}\\{CommonParameters.AppPath}\\AXPBuddy\\{DynelManager.LocalPlayer.Name}\\Config.json");

                NavMeshMovementController = new NavMeshMovementController($"{pluginDir}\\NavMeshes", true);

                MovementController.Set(NavMeshMovementController);

                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.StartStop, OnStartStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.LeaderInfo, OnLeaderInfoMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.WaitAndReady, OnWaitAndReadyMessage);

                Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannelChangedEvent += IPCChannel_Changed;

                Chat.RegisterCommand("enable", BuddyCommand);

                SettingsController.RegisterSettingsWindow("AXPBuddy", pluginDir + "\\UI\\AXPBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                Game.OnUpdate += OnUpdate;
                Team.TeamRequest += OnTeamRequest;

                _settings.AddVariable("ModeSelection", (int)ModeSelection.Path);

                _settings.AddVariable("Enable", false);
                _settings.AddVariable("Merge", false);

                _settings["Enable"] = false;

                if (Game.IsNewEngine)
                {
                    Chat.WriteLine("Does not work on this engine!");
                }
                else
                {
                    Chat.WriteLine("AXPBuddy Loaded!");
                    Chat.WriteLine("/buddy for settings. /enable to start and stop.");
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

        private void Start()
        {
            Enable = true;

            Chat.WriteLine("Buddy enabled.");

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }
        }

        private void Stop()
        {
            Enable = false;

            Chat.WriteLine("Buddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }

            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack();
            }

            if (MovementController.Instance.IsNavigating)
            {
                MovementController.Instance.Halt();
            }
        }

        private void OnStartStopMessage(int sender, IPCMessage msg)
        {
            if (!(msg is StartStopIPCMessage startStopMessage)) return;

            if (startStopMessage.IsStarting)
            {
                _settings["Enable"] = true;
                Start();
            }
            else
            {
                _settings["Enable"] = false;
                Stop();
            }
        }

        private void OnLeaderInfoMessage(int sender, IPCMessage msg)
        {
            if (!(msg is LeaderInfoIPCMessage leaderInfoMessage)) return;

            if (leaderInfoMessage.IsRequest)
            {
                if (DynelManager.LocalPlayer.Identity == Leader)
                {
                    IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = Leader, IsRequest = false });
                }
            }
            else
            {
                Leader = leaderInfoMessage.LeaderIdentity;
            }
        }

        private void OnWaitAndReadyMessage(int sender, IPCMessage msg)
        {
            if (!(msg is WaitAndReadyIPCMessage waitAndReadyMessage)) return;

            var senderIdentity = waitAndReadyMessage.PlayerIdentity;

            teamReadiness[senderIdentity] = waitAndReadyMessage.IsReady;

            if (Leader == DynelManager.LocalPlayer.Identity)
            {
                Ready = Team.Members.All(teamMember => !teamReadiness.ContainsKey(teamMember.Identity)
                || teamReadiness[teamMember.Identity]);
            }
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\AXPBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        private void OnUpdate(object s, float deltaTime)
        {
            try
            {
                if (Game.IsZoning) { return; }

                Shared.Kits kitsInstance = new Shared.Kits();
                kitsInstance.SitAndUseKit(66, 66);
                _stateMachine.Tick();

                if (Team.IsInTeam)
                {
                    if (_settings["Merge"].AsBool())
                    {
                        if (Leader == Identity.None)
                        {
                            var teamLeader = Team.Members.FirstOrDefault(member => member.IsLeader)?.Character;

                            Leader = teamLeader?.Identity ?? Identity.None;
                        }
                    }
                    else
                    {
                        if (Leader != Identity.None)
                        {
                            if (DynelManager.LocalPlayer.Identity != Leader)
                            {
                                if (Playfield.ModelId == PlayfieldId.Sector13)
                                {
                                    var localPlayer = DynelManager.LocalPlayer;
                                    bool currentIsReadyState = true;

                                    if (!Shared.Kits.InCombat())
                                    {
                                        if (Spell.HasPendingCast || localPlayer.NanoPercent < 60 || localPlayer.HealthPercent < 60
                                            || !Spell.List.Any(spell => spell.IsReady))
                                        {
                                            currentIsReadyState = false;
                                        }

                                        if (!Spell.HasPendingCast && localPlayer.NanoPercent > 66
                                            && localPlayer.HealthPercent > 66 && Spell.List.Any(spell => spell.IsReady))
                                        {
                                            currentIsReadyState = true;
                                        }
                                    }
                                    else
                                    {
                                        currentIsReadyState = true;
                                    }

                                    if (currentIsReadyState != lastSentIsReadyState)
                                    {
                                        Identity localPlayerIdentity = DynelManager.LocalPlayer.Identity;

                                        IPCChannel.Broadcast(new WaitAndReadyIPCMessage
                                        {
                                            IsReady = currentIsReadyState,
                                            PlayerIdentity = localPlayerIdentity
                                        });

                                        lastSentIsReadyState = currentIsReadyState;
                                    }
                                }
                            }
                            else
                            {
                                foreach (var member in Team.Members.Where(member => !ReformState.TeamList.Contains(member.Identity)))
                                {
                                    ReformState.TeamList.Add(member.Identity);
                                }
                            }
                        }
                        else
                        {
                            if (Team.IsLeader)
                            {
                                Leader = DynelManager.LocalPlayer.Identity;

                                IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = Leader });
                            }
                            else
                            {
                                IPCChannel.Broadcast(new LeaderInfoIPCMessage() { IsRequest = true });
                            }
                        }
                    }
                }

                #region UI Update

                if (SettingsController.settingsWindow == null || !SettingsController.settingsWindow.IsValid) return;

                if (SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput))
                {
                    if (channelInput == null) return;

                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel != channelValue)
                    {
                        Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel = channelValue;
                    }
                }

                if (SettingsController.settingsWindow.FindView("AXPBuddyInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (!_settings["Enable"].AsBool() && Enable)
                {
                    IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                    Stop();
                }
                if (_settings["Enable"].AsBool() && !Enable)
                {
                    IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                    Start();
                }

                #endregion

            }

            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + AXPBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public static void HandleAttacking(SimpleChar target)
        {
            if (target != null)
            {
                var localPlayer = DynelManager.LocalPlayer;

                if (localPlayer.IsAttacking && localPlayer.FightingTarget?.Identity != target?.Identity)
                {
                    localPlayer.StopAttack();
                }

                if (target.IsInAttackRange(true) && target.IsInLineOfSight)
                {
                    if (NavMeshMovementController.IsNavigating)
                    {
                        NavMeshMovementController.Halt();
                        IsNavigating = false;
                    }
                    else
                    {
                        if (localPlayer.FightingTarget == null
                        && !localPlayer.IsAttackPending
                        && !localPlayer.IsAttacking)
                        {
                            localPlayer.Attack(target);
                            Timeout = Time.AONormalTime + 300;
                        }
                    }
                }
                else
                {
                    if (!NavMeshMovementController.IsNavigating)
                    {
                        NavMeshMovementController.SetNavMeshDestination(target.Position);
                        IsNavigating = true;
                    }
                }
            }
        }

        public static void Mines(Dynel Mine)
        {
            Item Tool = Inventory.Items
            .Where(c => c.Name.Contains("Bomb Disarmament Tools"))
            .FirstOrDefault();

            if (Mine != null && Tool != null)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(Mine.Position) < 8)
                {
                    Chat.WriteLine(Tool.Name);

                    Tool.UseOn(Mine.Identity);
                }
            }
        }

        private void OnTeamRequest(object sender, TeamRequestEventArgs e)
        {
            if (_settings["Merge"].AsBool())
            {
                if (Leader != Identity.None)
                {
                    if (e.Requester == Leader)
                    {
                        e.Accept();
                    }
                }
                else
                {
                    Leader = e.Requester;
                }
            }
        }

        private void BuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Enable"].AsBool())
                    {
                        _settings["Enable"] = true;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                        Start();
                    }
                    else
                    {
                        _settings["Enable"] = false;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                        Stop();
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public enum ModeSelection
        {
            Pull, Path, Leech
        }

        public static class RelevantItems { }

        public static int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;

            var lineMatch = Regex.Match(ex.StackTrace ?? "", @":line (\d+)$", RegexOptions.Multiline);

            if (lineMatch.Success)
            {
                lineNumber = int.Parse(lineMatch.Groups[1].Value);
            }

            return lineNumber;
        }
    }
}