﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace AXPBuddy
{
    public class ReformState : IState
    {
        public static List <Identity> TeamList = new List <Identity> ();
        List<Identity> invited = new List<Identity>();

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId
                 || (Playfield.ModelIdentity.Instance == Constants.APFHubId
                 && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.MarkerpPoll) < 5))
            {
                return new DiedState();
            }

            if (Team.IsInTeam)
            {
                if (!Team.IsRaid)
                {
                    Team.ConvertToRaid();
                }
                else
                {
                    if (TeamList.Count == Team.Members.Count)
                    {
                        return new EnterSectorState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Reforming");
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

                InvitePlayers();
        }

        public void OnStateExit()
        {
            invited.Clear();
        }

        private void InvitePlayers()
        {

            foreach (var player in DynelManager.Players.Where(c => c.IsValid && c.IsInPlay && TeamList.Contains(c.Identity)
            && !Team.Members.Contains(c.Identity) && !invited.Contains(c.Identity) && c.Identity != DynelManager.LocalPlayer.Identity))
            {
                invited.Add(player.Identity);
                Team.Invite(player.Identity);
            }
        }
    }
}
