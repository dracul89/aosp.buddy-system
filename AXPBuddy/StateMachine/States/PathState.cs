using AOSharp.Core;
using AOSharp.Core.UI;
using System;
using System.Linq;

namespace AXPBuddy
{
    public class PathState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId
                || (Playfield.ModelIdentity.Instance == Constants.APFHubId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.MarkerpPoll) < 5))
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId)
            {
                return new IdleState();
            }

            if (AXPBuddy.ModeSelection.Pull == (AXPBuddy.ModeSelection)AXPBuddy._settings["ModeSelection"].AsInt32())
            {
                return new PullState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Path");
            AXPBuddy.IsNavigating = false;
            AXPBuddy.Timeout = Time.AONormalTime + 300;
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                var Mine = DynelManager.AllDynels.Where(m => m.Name.Contains("Mine")).FirstOrDefault();

                AXPBuddy.Mines(Mine);

                if (Time.AONormalTime > AXPBuddy.Timeout)
                {
                    Chat.WriteLine("Timed out, disbanding.");
                    Team.Disband();
                }

                if (Team.IsInTeam)
                {
                    var switchToMob = DynelManager.NPCs.Where(c => c.IsAttacking
                    && c.FightingTarget?.Identity != DynelManager.LocalPlayer.Identity)
                        .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                    .FirstOrDefault();

                    var mob = DynelManager.NPCs
                    .Where(c => c.Health > 0
                                && !c.Name.Contains("Unicorn Recon Agent Chittick")
                                && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 30)
                    .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                    .FirstOrDefault();


                    if (switchToMob != null)
                    {
                        AXPBuddy.HandleAttacking(switchToMob);
                    }
                    else
                    {
                        if (AXPBuddy.Ready)
                        {
                            if (mob != null)
                            {
                                AXPBuddy.HandleAttacking(mob);
                            }
                            else
                            {
                                if (mob != null || Team.Members.Any(c => c.Character == null))
                                {
                                    if (AXPBuddy.IsNavigating)
                                    {
                                        AXPBuddy.NavMeshMovementController.Halt();
                                        AXPBuddy.IsNavigating = false;
                                        AXPBuddy.Timeout = Time.AONormalTime + 300;
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }

                                if (DynelManager.LocalPlayer.IsAttacking)
                                {
                                    DynelManager.LocalPlayer.StopAttack();
                                }

                                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13GoalPos) > 5f)
                                {
                                    if (!Team.Members.Any(c => c.Character == null) || AXPBuddy.Ready)
                                    {
                                        if (!AXPBuddy.IsNavigating)
                                        {
                                            AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13GoalPos);
                                            AXPBuddy.IsNavigating = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!DynelManager.NPCs.Any(c => c.Health > 0 && c.DistanceFrom(DynelManager.LocalPlayer) < 30f))
                                    {
                                        Team.Disband();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + AXPBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != AXPBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    AXPBuddy.previousErrorMessage = errorMessage;
                }
            }
        }

        public void OnStateExit()
        {
        }
    }
}
