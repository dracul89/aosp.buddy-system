﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using System;
using System.Linq;

namespace AXPBuddy
{
    public class PullState : IState
    {
        public static int _aggToolCounter = 0;
        public static int _attackTimeout = 0;
        public static bool endTaunting;

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < AXPBuddy._lastZonedTime + 2f) { return null; }

            if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId
                || (Playfield.ModelIdentity.Instance == Constants.APFHubId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.MarkerpPoll) < 5))
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId)
            {
                return new IdleState();
            }

            if (AXPBuddy.ModeSelection.Path == (AXPBuddy.ModeSelection)AXPBuddy._settings["ModeSelection"].AsInt32())
            {
                return new PathState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Pull");
            AXPBuddy.IsNavigating = false;
            AXPBuddy.Timeout = Time.AONormalTime + 300;
        }

        public void Tick()
        {
            try
            {
                var Mine = DynelManager.AllDynels.Where(m => m.Name.Contains("Mine")).FirstOrDefault();
                AXPBuddy.Mines(Mine);

                if (Game.IsZoning || Time.NormalTime < AXPBuddy._lastZonedTime + 2f) { return; }

                if (Time.AONormalTime > AXPBuddy.Timeout)
                {
                    Chat.WriteLine("Timed out, disbanding.");
                    Team.Disband();
                }

                if (Team.IsInTeam)
                {
                    var switchToMob = DynelManager.NPCs.Where(c => c.IsAttacking
                    && c.FightingTarget?.Identity != DynelManager.LocalPlayer.Identity)
                        .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position)).FirstOrDefault();

                    var mob = DynelManager.NPCs
                    .Where(c => c.Health > 0
                                && !c.Name.Contains("Unicorn Recon Agent Chittick")
                                && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 30)
                    .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position)).FirstOrDefault();

                    if (switchToMob != null)
                    {
                        AXPBuddy.HandleAttacking(switchToMob);
                    }
                    else
                    {
                        if (AXPBuddy.Ready)
                        {
                            if (mob != null)
                            {
                                if (mob.IsInLineOfSight)
                                {
                                    if (AXPBuddy.IsNavigating && !endTaunting)
                                    {
                                        AXPBuddy.NavMeshMovementController.Halt();
                                        AXPBuddy.IsNavigating = false;
                                    }

                                    if (!mob.IsInAttackRange())
                                    {
                                        if (!Team.Members.Any(t => t.Character == null))
                                        {
                                            HandleTaunting(mob);
                                        }
                                    }
                                    else
                                    {
                                        if (DynelManager.LocalPlayer.FightingTarget == null && !DynelManager.LocalPlayer.IsAttacking
                                               && !DynelManager.LocalPlayer.IsAttackPending)
                                        {
                                            DynelManager.LocalPlayer.Attack(mob);
                                            AXPBuddy.Timeout = Time.AONormalTime + 300;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!Team.Members.Any(t => t.Character == null))
                                    {
                                        if (!AXPBuddy.IsNavigating)
                                        {
                                            AXPBuddy.NavMeshMovementController.SetNavMeshDestination(mob.Position);
                                            AXPBuddy.IsNavigating = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (mob != null || Team.Members.Any(c => c.Character == null))
                                {
                                    if (AXPBuddy.IsNavigating)
                                    {
                                        Chat.WriteLine("Stopping for team or mob");

                                        AXPBuddy.NavMeshMovementController.Halt();
                                        AXPBuddy.IsNavigating = false;
                                        AXPBuddy.Timeout = Time.AONormalTime + 300;
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }

                                if (DynelManager.LocalPlayer.IsAttacking)
                                {
                                    DynelManager.LocalPlayer.StopAttack();
                                }

                                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13GoalPos) > 5f)
                                {
                                    if (!Team.Members.Any(c => c.Character == null) && !Spell.HasPendingCast && DynelManager.LocalPlayer.NanoPercent > 70
                                        && DynelManager.LocalPlayer.HealthPercent > 70 && AXPBuddy.Ready)
                                    {
                                        if (!AXPBuddy.IsNavigating)
                                        {
                                            AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13GoalPos);
                                            AXPBuddy.IsNavigating = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!DynelManager.NPCs.Any(c => c.Health > 0 && c.DistanceFrom(DynelManager.LocalPlayer) < 30f))
                                    {
                                        Team.Disband();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + AXPBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != AXPBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    AXPBuddy.previousErrorMessage = errorMessage;
                }
            }
        }

        public static void HandleTaunting(SimpleChar target)
        {
            if (_aggToolCounter >= 2)
            {
                if (_attackTimeout >= 1)
                {
                    if (target.IsAttacking && !target.IsInAttackRange())
                    {
                        endTaunting = true;

                        if (!AXPBuddy.NavMeshMovementController.IsNavigating)
                        {
                            AXPBuddy.NavMeshMovementController.SetNavMeshDestination(target.Position);
                        }
                    }
                    else
                    {
                        _attackTimeout = 0;
                        _aggToolCounter = 0;
                        endTaunting = false;
                        return;
                    }
                }

                _attackTimeout++;
                _aggToolCounter = 0;
            }

            if (Inventory.Find(83920, out Item item) ||  // Aggression Enhancer
                Inventory.Find(83919, out item) ||  // Aggression Multiplier
                Inventory.Find(151692, out item) ||  // Modified Aggression Enhancer (low)
                Inventory.Find(151693, out item) ||  // Modified Aggression Enhancer (High)
                Inventory.Find(152029, out item) || // Aggression Enhancer (Jealousy Augmented)
                Inventory.Find(152028, out item) || // Aggression Multiplier (Jealousy Augmented)
                Inventory.Find(253186, out item) || // Codex of the Insulting Emerto (Low)
                Inventory.Find(253187, out item))   // Codex of the Insulting Emerto (High)
            {
                if (!Item.HasPendingUse && !DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology))
                {
                    item.Use(target, true);
                    _aggToolCounter++;
                }
            }
        }

        public void OnStateExit()
        {

        }
    }
}
