﻿using AOSharp.Core;
using AOSharp.Core.UI;

namespace AXPBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (AXPBuddy._settings["Enable"].AsBool())
            {
                if (Team.IsRaid)
                {
                    if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId
                        || (Playfield.ModelIdentity.Instance == Constants.APFHubId
                        && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.MarkerpPoll) < 5))
                    {
                        return new DiedState();
                    }

                    if (Playfield.ModelIdentity.Instance == Constants.APFHubId)
                    {
                        return new EnterSectorState();
                    }

                    if (Playfield.ModelIdentity.Instance == Constants.S13Id)
                    {
                        int modeSelection = AXPBuddy._settings["ModeSelection"].AsInt32();

                        if (DynelManager.LocalPlayer.Identity != AXPBuddy.Leader)
                        {
                            if (modeSelection == (int)AXPBuddy.ModeSelection.Leech)
                            {
                                return new LeechState();
                            }
                            else
                            {
                                return new FollowerState();
                            }
                        }
                        else
                        {
                            if (modeSelection == (int)AXPBuddy.ModeSelection.Path)
                            {
                                return new PathState();
                            }
                            else
                            {
                                return new PullState();
                            }
                        }
                    }
                }
                else
                {
                    if (DynelManager.LocalPlayer.Identity == AXPBuddy.Leader)
                    {
                        return new ReformState();
                    }
                }
            }
            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Idle");
        }

        public void Tick()
        {
        }

        public void OnStateExit()
        {
        }
    }
}
