using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System;
using System.Linq;

namespace AXPBuddy
{
    public class FollowerState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId
                || (Playfield.ModelIdentity.Instance == Constants.APFHubId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.MarkerpPoll) < 5))
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId)
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Follower");
            AXPBuddy.IsNavigating = false;
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                var Mine = DynelManager.AllDynels.Where(m => m.Name.Contains("Mine")).FirstOrDefault();

                AXPBuddy.Mines(Mine);

                if (Team.IsInTeam)
                {
                    AXPBuddy._leader = Team.Members
                        .Where(c => c.Character?.Health > 0
                            && c.Character?.IsValid == true
                            && (c.Identity == AXPBuddy.Leader))
                        .FirstOrDefault()?.Character;

                    var mob = DynelManager.NPCs
                       .Where(c => c.Health > 0
                                   && !c.Name.Contains("Unicorn Recon Agent Chittick")
                                   && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 30)
                       .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                       .FirstOrDefault();


                    if (AXPBuddy._leader != null)
                    {
                        if (AXPBuddy._leader?.FightingTarget != null || AXPBuddy._leader?.IsAttacking == true)
                        {
                            var targetMob = DynelManager.NPCs
                                .Where(c => c.Health > 0
                                    && c.Identity == (Identity)AXPBuddy._leader?.FightingTarget?.Identity)
                                .FirstOrDefault();


                            if (targetMob != null)
                            {
                                AXPBuddy.HandleAttacking(targetMob);
                            }
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.Position.DistanceFrom(AXPBuddy._leader.Position) > 5f)
                            {
                                AXPBuddy.NavMeshMovementController.SetNavMeshDestination(AXPBuddy._leader.Position);
                                AXPBuddy.IsNavigating = true;
                            }
                        }
                    }
                    else
                    {
                        if (mob == null)
                        {
                            if (!AXPBuddy.IsNavigating)
                            {
                                AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13GoalPos);
                                AXPBuddy.IsNavigating = true;
                            }
                        }
                        else
                        {
                            if (AXPBuddy.IsNavigating)
                            {
                                AXPBuddy.NavMeshMovementController.Halt();
                                AXPBuddy.IsNavigating = false;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + AXPBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != AXPBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    AXPBuddy.previousErrorMessage = errorMessage;
                }
            }
        }

        public void OnStateExit()
        {
        }
    }
}
