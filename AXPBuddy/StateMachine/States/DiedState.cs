﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;

namespace AXPBuddy
{
    public class DiedState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.S13Id)
            {
                int modeSelection = AXPBuddy._settings["ModeSelection"].AsInt32();

                if (DynelManager.LocalPlayer.Identity != AXPBuddy.Leader)
                {
                    if (modeSelection == (int)AXPBuddy.ModeSelection.Leech)
                    {
                        return new LeechState();
                    }
                    else
                    {
                        return new FollowerState();
                    }
                }
                else
                {
                    if (modeSelection == (int)AXPBuddy.ModeSelection.Path)
                    {
                        return new PathState();
                    }
                    else
                    {
                        return new PullState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {

            Chat.WriteLine($"Died");
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (Game.IsZoning || Time.NormalTime < AXPBuddy._lastZonedTime + 2f) { return; }

            Dynel Lever = DynelManager.AllDynels
                .Where(c => c.Name == "A Lever"
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 6f)
                .FirstOrDefault();

            if (DynelManager.LocalPlayer.HealthPercent > 65 && DynelManager.LocalPlayer.NanoPercent > 65
                && DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1
                && DynelManager.LocalPlayer.MovementState != MovementState.Sit)
            {

                if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId)
                {
                    if (!AXPBuddy.NavMeshMovementController.IsNavigating)
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.UnicornLever) > 6)
                        {
                            AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.Bridge);
                            AXPBuddy.NavMeshMovementController.AppendNavMeshDestination(Constants.BridgeTurn);
                            AXPBuddy.NavMeshMovementController.AppendNavMeshDestination(Constants.UnicornLever);
                        }
                    }

                    if (Lever != null)
                    {
                        if (AXPBuddy.NavMeshMovementController.IsNavigating)
                        {
                            AXPBuddy.NavMeshMovementController.Halt();
                        }
                        else
                        {
                            Lever.Use();
                        }
                    }
                }
                else if (Playfield.ModelIdentity.Instance == Constants.APFHubId)
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13EntrancePos) > 6)
                    {
                        AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13EntrancePos);
                    }
                }
            }
        }
    }
}