﻿using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;

namespace AXPBuddy
{
    public class EnterSectorState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId
                 || (Playfield.ModelIdentity.Instance == Constants.APFHubId
                 && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.MarkerpPoll) < 5))
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.S13Id)
            {
                int modeSelection = AXPBuddy._settings["ModeSelection"].AsInt32();

                if (DynelManager.LocalPlayer.Identity != AXPBuddy.Leader)
                {
                    if (modeSelection == (int)AXPBuddy.ModeSelection.Leech)
                    {
                        return new LeechState();
                    }
                    else
                    {
                        return new FollowerState();
                    }
                }
                else
                {
                    if (modeSelection == (int)AXPBuddy.ModeSelection.Path)
                    {
                        return new PathState();
                    }
                    else
                    {
                        return new PullState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Enter S13");

        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (Team.IsRaid)
            {
                if (DynelManager.LocalPlayer.Identity == AXPBuddy.Leader)
                {
                    PathtoEntrance();
                }
                else
                {
                    AXPBuddy._leader = Team.Members
                           .Where(c => c.Character?.Health > 0
                               && c.Character?.IsValid == true
                               && (c.Identity == AXPBuddy.Leader))
                           .FirstOrDefault()?.Character;

                    if (AXPBuddy._leader == null)
                    {
                        PathtoEntrance();
                    }
                }
            }
        }

        void PathtoEntrance()
        {
            if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13EntrancePos) > 1)
            {
                if (!AXPBuddy.NavMeshMovementController.IsNavigating)
                {
                    AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13EntrancePos);
                }
            }
        }
    }
}