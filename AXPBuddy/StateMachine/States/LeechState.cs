﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;

namespace AXPBuddy
{
    public class LeechState : IState
    {
        private static double _timeOut;

        private static bool _init = false;
        private static bool _initTeam = false;

        public IState GetNextState()
        {
            if (Game.IsZoning || Time.NormalTime < AXPBuddy._lastZonedTime + 2f) { return null; }

            if (Playfield.ModelIdentity.Instance == Constants.UnicornHubId
                || (Playfield.ModelIdentity.Instance == Constants.APFHubId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.MarkerpPoll) < 5))
            {
                return new DiedState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.APFHubId
                && !Team.IsInTeam
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.S13ZoneOutPos) <= 10f)
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Leecher");

            DynelManager.LocalPlayer.Position = new Vector3(150.8, 67.7f, 42.0f);
        }

        public void OnStateExit()
        {

            _initTeam = false;
        }

        public void Tick()
        {
            if (!Team.IsInTeam || Game.IsZoning || Time.NormalTime < AXPBuddy._lastZonedTime + 2f) { return; }

            if (DynelManager.LocalPlayer.Position.Distance2DFrom(Constants.S13GoalPos) <= 30f
                && !_initTeam)
            {
                _initTeam = true;
            }

            if (Playfield.ModelIdentity.Instance == Constants.S13Id)
            {
                AXPBuddy._ourPos = DynelManager.LocalPlayer.Position;
            }

            if (!AXPBuddy._initMerge && AXPBuddy._settings["Merge"].AsBool())
            {
                if (!AXPBuddy._initMerge)
                {
                    AXPBuddy._initMerge = true;
                }

                AXPBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.S13GoalPos);
            }

            AXPBuddy._leader = Team.Members
                .Where(c => c.Character?.Health > 0
                    && c.Character?.IsValid == true
                    && (c.Identity == AXPBuddy.Leader))
                .FirstOrDefault()?.Character;

            if (AXPBuddy._leader != null)
            {
                AXPBuddy._leaderPos = (Vector3)AXPBuddy._leader?.Position;

                if (AXPBuddy.NavMeshMovementController.IsNavigating)
                {
                    if (DynelManager.LocalPlayer.Position.Distance2DFrom(new Vector3(169.5f, 36.0f, 164.3f)) > 15f)
                    {
                        _init = false;
                    }

                    if (Time.NormalTime > _timeOut + 10f && _init)
                    {
                        AXPBuddy.NavMeshMovementController.Halt();
                        DynelManager.LocalPlayer.Position = new Vector3(AXPBuddy._leaderPos.X, 67.7f, AXPBuddy._leaderPos.Z);
                    }

                    if (DynelManager.LocalPlayer.Position.Distance2DFrom(new Vector3(169.5f, 36.0f, 164.3f)) <= 15f)
                    {
                        if (!_init)
                        {
                            _init = true;
                            _timeOut = Time.NormalTime;
                        }
                    }
                }
                if (DynelManager.LocalPlayer.Position.Distance2DFrom(AXPBuddy._leaderPos) > 2f
                    && DynelManager.LocalPlayer.Position.Distance2DFrom(Constants.S13GoalPos) > 30f
                    && DynelManager.LocalPlayer.Position.Distance2DFrom(new Vector3(137.7f, 67.6f, 490.3f)) <= 5f)
                {
                    AXPBuddy.NavMeshMovementController.SetDestination(new Vector3(161.5f, 67.6f, 488.3f));
                }

                if (DynelManager.LocalPlayer.Position.Distance2DFrom(AXPBuddy._leaderPos) > 2f
                    && DynelManager.LocalPlayer.Position.Distance2DFrom(Constants.S13GoalPos) > 30f
                    && DynelManager.LocalPlayer.Position.Distance2DFrom(new Vector3(137.7f, 67.6f, 490.3f)) > 5f)
                {
                    AXPBuddy.NavMeshMovementController.SetNavMeshDestination(new Vector3(AXPBuddy._leaderPos.X, 67.7f, AXPBuddy._leaderPos.Z));
                }
            }
        }
    }
}