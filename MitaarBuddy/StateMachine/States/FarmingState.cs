﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Linq;

namespace MitaarBuddy
{
    public class FarmingState : IState
    {
        public static bool _initCorpse = false;
        public static bool _atCorpse = false;
        private static double _timeToLeave;

        private static Corpse _sinuhCorpse;

        public static Vector3 _sinuhCorpsePos = Vector3.Zero;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.XanHubId)
            {
                if (DynelManager.LocalPlayer.Identity == MitaarBuddy.Leader)
                {
                    if (!Team.Members.Any(c => c.Character == null))
                    {
                        return new ReformState();
                    }
                }
                else
                {
                    return new IdleState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            _atCorpse = false;
            MitaarBuddy.SinuhCorpse = false;
            Chat.WriteLine("Looting state");
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (Playfield.ModelIdentity.Instance == Constants.MitaarId)
            {
                Dynel Device = DynelManager.AllDynels
               .Where(c => c.Name == "Strange Alien Device")
               .FirstOrDefault();

                _sinuhCorpse = DynelManager.Corpses
                             .Where(c => c.Name == "Remains of Technomaster Sinuh")
                                 .FirstOrDefault();

                if (_sinuhCorpse != null)
                {
                    _sinuhCorpsePos = (Vector3)_sinuhCorpse?.Position;

                    if (!_atCorpse)
                    {
                        if (AtPosition(_sinuhCorpsePos, 2))
                        {
                            Chat.WriteLine("Pause for looting, 30 sec");
                            _timeToLeave = Time.AONormalTime + 30;
                            _atCorpse = true;
                        }
                    }
                    else
                    {
                        if (Time.AONormalTime > _timeToLeave)
                        {
                            HandleDeviceUse(Device);
                        }
                    }
                }
                else
                {
                    HandleDeviceUse(Device);
                }
            }
        }

        void HandleDeviceUse(Dynel Device)
        {
            if (Device != null)
            {
                if (Extensions.CanProceed())
                {
                   if (AtPosition(Constants._strangeAlienDevice, 3))
                    {
                        if (DynelManager.LocalPlayer.Identity == MitaarBuddy.Leader)
                        {
                            Device.Use();
                        }
                        else
                        {
                            MitaarBuddy._leader = Team.Members
                                   .Where(c => c.Character?.Health > 0
                                       && c.Character?.IsValid == true
                                       && (c.Identity == MitaarBuddy.Leader))
                                   .FirstOrDefault()?.Character;

                            if (MitaarBuddy._leader == null)
                            {
                                Device.Use();
                            }
                        }
                    }
                }
            }
        }

        bool AtPosition(Vector3 position, int distance)
        {
            HandlePathing(position, distance);
            return MovementController.Instance.IsNavigating == false;
        }

        void HandlePathing(Vector3 position, int distance)
        {
            if (DynelManager.LocalPlayer.Position.DistanceFrom(position) > distance)
            {
                if (!MovementController.Instance.IsNavigating)
                {
                    MovementController.Instance.SetDestination(position);
                }
            }
            else
            {
                if (MovementController.Instance.IsNavigating)
                {
                    MovementController.Instance.Halt();
                }
            }
        }

        enum LootingPhase
        {
            Moving,
            Looting,
            Leaving,
        }
    }
}