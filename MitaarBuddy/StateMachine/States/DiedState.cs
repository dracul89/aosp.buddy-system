﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Linq;

namespace MitaarBuddy
{
    public class DiedState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance != Constants.XanHubId)
            {
                return null;
            }
            else
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._entrance) < 10.0f)
                {
                    return new IdleState();
                }
            }
            return null;
        }

        public void OnStateEnter()
        {
            MitaarBuddy._died = true;

            Chat.WriteLine($"Died");
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._entrance) > 10.0f)
            {
                if (DynelManager.LocalPlayer.HealthPercent > 65 && DynelManager.LocalPlayer.NanoPercent > 65)
                {
                    if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                    {
                        MovementController.Instance.SetMovement(MovementAction.LeaveSit);
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1)
                        {
                            if (!MitaarBuddy.NavMeshMovementController.IsNavigating)
                            {
                                MitaarBuddy.NavMeshMovementController.SetNavMeshDestination(Constants._reneterPos);
                            }
                        }
                    }
                }
            }
        }
    }
}