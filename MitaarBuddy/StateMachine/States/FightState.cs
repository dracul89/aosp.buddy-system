﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Linq;

namespace MitaarBuddy
{
    public class FightState : IState
    {
        private static SimpleChar _sinuh;
        private static SimpleChar _alienCoccoon;
        private static SimpleChar _xanSpirits;
        private static Corpse _sinuhCorpse;

        Dynel AltaroftheTrueBlood;
        Dynel AltaroftheOutsider;
        Dynel AltaroftheLight;
        Dynel AltaroftheSource;

        bool TeamReady;

        public IState GetNextState()
        {
            GetDynels();

            if (Playfield.ModelIdentity.Instance == Constants.XanHubId)
            {
                return new IdleState();
            }
            else
            {
                if (MitaarBuddy._settings["Farming"].AsBool())
                {
                    if (Extensions.CanProceed())
                    {
                        if (MitaarBuddy.SinuhCorpse && _xanSpirits == null && _alienCoccoon == null)
                        {
                            return new FarmingState();
                        }
                    }
                }
            }
            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Starting Hardcore Parkour!");
            TeamReady = false;
            MovementController.Instance.SetDestination(Constants._redPodium);
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (!Team.IsInTeam || Game.IsZoning) { return; }

            if (Playfield.ModelIdentity.Instance == Constants.MitaarId)
            {
                GetDynels();

                if (!Team.Members.Any(t => t.Character == null))
                {
                    TeamReady = true;
                }

                if (TeamReady)
                {
                    if (_sinuhCorpse != null)
                    {
                        MitaarBuddy.SinuhCorpse = true;
                    }

                    if (MitaarBuddy._settings["StopAttack"].AsBool())
                    {
                        if (_xanSpirits != null)
                        {
                            HandleStopAttack();
                        }
                        else
                        {
                            if (_alienCoccoon != null)
                            {
                                HandleAlienCoccoonAttack();
                            }
                            else
                            {
                                if (_sinuh != null)
                                {
                                    HandleSinuhAttack();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_alienCoccoon != null)
                        {
                            HandleAlienCoccoonAttack();
                        }
                        else
                        {
                            if (_sinuh != null)
                            {
                                HandleSinuhAttack();
                            }
                        }
                    }

                    //Pathing to spirits
                    if (_xanSpirits != null)
                    {
                        if (MitaarBuddy._settings["Red"].AsBool())
                        {
                            HandleSpirits(AltaroftheTrueBlood, MitaarBuddy.SpiritNanos.BlessingofTheBlood);
                        }

                        if (MitaarBuddy._settings["Blue"].AsBool())
                        {
                            HandleSpirits(AltaroftheSource, MitaarBuddy.SpiritNanos.BlessingofTheSource);
                        }

                        if (MitaarBuddy._settings["Green"].AsBool())
                        {
                            HandleSpirits(AltaroftheOutsider, MitaarBuddy.SpiritNanos.BlessingofTheOutsider);
                        }

                        if (MitaarBuddy._settings["Yellow"].AsBool())
                        {
                            HandleSpirits(AltaroftheLight, MitaarBuddy.SpiritNanos.BlessingofTheLight);
                        }
                    }
                }
            }
        }
        void HandleSpirits(Dynel spirit, int spell)
        {
            var localplayer = DynelManager.LocalPlayer;

            if (spirit != null)
            {
                if (!localplayer.Buffs.Contains(spell))
                {
                    HandleSpiritPathing(spirit);
                }
                else
                {
                    if (localplayer.Buffs.Find(spell, out Buff buff) && buff.RemainingTime < 3)
                    {
                        HandleSpiritPathing(spirit);
                    }
                }
            }
        }

        void HandleSpiritPathing(Dynel spirit)
        {
            if (DynelManager.LocalPlayer.Position.DistanceFrom(spirit.Position) > 0.9f)
            {
                if (!MovementController.Instance.IsNavigating)
                {
                    MovementController.Instance.SetDestination(spirit.Position);
                }
            }
        }

        void HandleStopAttack()
        {
            if (DynelManager.LocalPlayer.IsAttacking == true && DynelManager.LocalPlayer.FightingTarget?.Name == _sinuh.Name)
            {
                DynelManager.LocalPlayer.StopAttack();
            }
        }

        void HandleSinuhAttack()
        {
            if (DynelManager.LocalPlayer.IsAttacking == false && !DynelManager.LocalPlayer.IsAttackPending)
            {
                DynelManager.LocalPlayer.Attack(_sinuh);
            }
        }

        void HandleAlienCoccoonAttack()
        {
            if (DynelManager.LocalPlayer.IsAttacking == true)
            {
                if (DynelManager.LocalPlayer.FightingTarget?.Name == _sinuh.Name)
                {
                    DynelManager.LocalPlayer.StopAttack();
                }
            }
            else
            {
                if (!DynelManager.LocalPlayer.IsAttackPending)
                {
                    DynelManager.LocalPlayer.Attack(_alienCoccoon);
                }
            }
        }

        void GetDynels()
        {
            _sinuh = DynelManager.NPCs
               .Where(c => c.Health > 0
                && c.Name == "Technomaster Sinuh")
                .FirstOrDefault();

            _alienCoccoon = DynelManager.NPCs
               .Where(c => c.Health > 0
                       && c.Name == "Alien Coccoon")
                   .FirstOrDefault();

            _xanSpirits = DynelManager.NPCs
                .Where(c => c.Health > 0
                    && c.Name == "Xan Spirit")
                    .FirstOrDefault();

            _sinuhCorpse = DynelManager.Corpses.Where(c => c.Name == "Remains of Technomaster Sinuh").FirstOrDefault();

            AltaroftheLight = DynelManager.AllDynels.Where(a => a.Name == "Altar of the Light").FirstOrDefault();
            AltaroftheOutsider = DynelManager.AllDynels.Where(a => a.Name == "Altar of the Outsider").FirstOrDefault();
            AltaroftheSource = DynelManager.AllDynels.Where(a => a.Name == "Altar of the Source").FirstOrDefault();
            AltaroftheTrueBlood = DynelManager.AllDynels.Where(a => a.Name == "Altar of the True Blood").FirstOrDefault();
        }
    }
}