﻿using AOSharp.Core;
using System.Linq;

namespace VortexxBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (!VortexxBuddy._settings["Enable"].AsBool()) { return null; }
            else
            {
                if (Playfield.ModelIdentity.Instance == Constants.XanHubId)
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._entrance) < 20f)
                    {
                        if (Team.IsInTeam)
                        {
                            if (Extensions.CanProceed())
                            {
                                if (DynelManager.LocalPlayer.Identity == VortexxBuddy.Leader)
                                {
                                    if (!Team.Members.Any(t => t.Character == null))
                                    {
                                        return new EnterState();
                                    }   
                                }
                                else
                                {
                                    VortexxBuddy._leader = Team.Members
                                   .Where(c => c.Character?.Health > 0
                                       && c.Character?.IsValid == true
                                       && (c.Identity == VortexxBuddy.Leader))
                                   .FirstOrDefault()?.Character;

                                    if (!VortexxBuddy._settings["Clear"].AsBool())
                                    {
                                        if (VortexxBuddy._leader == null)
                                        {
                                            return new EnterState();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Extensions.CanProceed())
                        {
                            return new DiedState();
                        }
                    }
                }

                if (Playfield.ModelIdentity.Instance == Constants.VortexxId)
                {
                    return new FightState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
        }
    }
}
