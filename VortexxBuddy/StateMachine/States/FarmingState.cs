﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Linq;

namespace VortexxBuddy
{
    public class FarmingState : IState
    {
        public static bool _atCorpse = false;
        private static double _timeToLeave;

        private static Corpse _vortexxCorpse;

        public static Vector3 _vortexxCorpsePos = Vector3.Zero;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.XanHubId
                 && !Team.Members.Any(c => c.Character == null))
            {
                if (DynelManager.LocalPlayer.Identity == VortexxBuddy.Leader)
                {
                    if (!Team.Members.Any(c => c.Character == null))
                    {
                        return new ReformState();
                    }
                }
                else
                {
                    return new IdleState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Looting");

            if (VortexxBuddy.NavMeshMovementController.IsNavigating)
            {
                VortexxBuddy.NavMeshMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            _atCorpse = false;

            VortexxBuddy.VortexxCorpse = false;

            if (VortexxBuddy.NavMeshMovementController.IsNavigating)
            {
                VortexxBuddy.NavMeshMovementController.Halt();
            }
        }

        public void Tick()
        {

            Dynel Beacon = DynelManager.AllDynels
               .Where(c => c.Name == "Dust Brigade Beacon")
               .FirstOrDefault();

            _vortexxCorpse = DynelManager.Corpses
                   .Where(c => c.Name.Contains("Remains of Ground Chief Vortexx"))
                       .FirstOrDefault();

            if (_vortexxCorpse != null)
            {
                _vortexxCorpsePos = (Vector3)_vortexxCorpse?.Position;

                if (!_atCorpse)
                {
                    if (AtPosition(_vortexxCorpsePos, 2))
                    {
                        Chat.WriteLine("Pause for looting, 30 sec");
                        _timeToLeave = Time.AONormalTime + 30;
                        _atCorpse = true;
                    }
                }
                else
                {
                    if (Time.AONormalTime > _timeToLeave)
                    {
                        HandleDeviceUse(Beacon);
                    }
                }
            }
            else
            {
                HandleDeviceUse(Beacon);
            }
        }

        void HandleDeviceUse(Dynel Device)
        {
            if (Device != null)
            {
                if (Extensions.CanProceed())
                {
                    if (AtPosition(Device.Position, 3))
                    {
                        if (DynelManager.LocalPlayer.Identity == VortexxBuddy.Leader)
                        {
                            Device.Use();
                        }
                        else
                        {
                            VortexxBuddy._leader = Team.Members
                                   .Where(c => c.Character?.Health > 0
                                       && c.Character?.IsValid == true
                                       && (c.Identity == VortexxBuddy.Leader))
                                   .FirstOrDefault()?.Character;

                            if (VortexxBuddy._leader == null)
                            {
                                Device.Use();
                            }
                        }
                    }
                }
            }
        }

        bool AtPosition(Vector3 position, int distance)
        {
            HandlePathing(position, distance);
            return VortexxBuddy.NavMeshMovementController.IsNavigating == false;
        }

        void HandlePathing(Vector3 position, int distance)
        {
            if (DynelManager.LocalPlayer.Position.DistanceFrom(position) > distance)
            {
                if (!VortexxBuddy.NavMeshMovementController.IsNavigating)
                {
                    VortexxBuddy.NavMeshMovementController.SetNavMeshDestination(position);
                }
            }
            else
            {
                if (VortexxBuddy.NavMeshMovementController.IsNavigating)
                {
                    VortexxBuddy.NavMeshMovementController.Halt();
                }
            }
        }
    }
}
